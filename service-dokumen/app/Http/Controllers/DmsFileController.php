<?php

namespace Modules\DMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\API\BaseController as BaseController;

use Modules\DMS\Entities\Folder;
use Modules\DMS\Entities\File;
use Modules\DMS\Entities\FilePasis;
use Modules\DMS\Entities\FileHasRole;
use Modules\DMS\Entities\FolderHasRole;
use App\Models\Auth\User;
use Validator;
use App\Models\Akademik\Siswa;
use App\Models\Akademik\SiswaDataKelompok;
use App\Models\Akademik\SiswaKelompok;

class DmsFileController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        return false;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dms::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'nama.required'    => 'File harus diisi.',
            'deskripsi.required'    => 'Deskripsi File harus diisi.',
            'id_user.required'    => 'Id user tidak ditemukan.',
            'id_folder.required'    => 'Folder tidak ditemukan.',
        ];
        $validator = Validator::make($input, [
            'id_user' => 'required',
            'id_folder' => 'required',
            'file' => 'required'
        ]);        

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $extension = strtolower($input['file']->getClientOriginalExtension());

        $input['size'] = $input['file']->getSize() / 1024;
        $input['nama'] = str_replace(' ', '%20', $input['file']->getClientOriginalName());
        $input['type'] = $extension;
        $user = User::find($input['id_user']);
        
        // if($user->roles[0]->name == 'pasis' && $input['tipe'] == 'public') {
        //     $data = new FilePasis($input);
        // }else{
            $data = new File($input);
        // }

        if(isa_convert_bytes_to_specified($input['file']->getSize(), 'M') >= $user->kapasitas){
            return $this->sendResponse($data, 'Ukuran File Melebihi Kapasitas');
        }else if(((double) $user->sisa_kapasitas + isa_convert_bytes_to_specified($input['file']->getSize(), 'M')) >= $user->kapasitas) {
            return $this->sendResponse($data, 'Ukuran File Melebihi Kapasitas');
        }else if((int) $user->sisa_kapasitas >= $user->kapasitas) {
            return $this->sendResponse($data, 'Kapasitas Sudah Melebihi Batas Maksimum');
        }else{
            $data->save();
            $input['file']->move(public_path().'/modules/dms/file/',$input['file']->getClientOriginalName());
            return $this->sendResponse($data, 'File berhasil terupload');
        }
    }

    public function storeTtd(Request $request)
    {
        $input = $request->all();
        $messages = [
            'file.required'    => 'File harus diisi.',
        ];
        $validator = Validator::make($input, [
            'file' => 'required'
        ]);        

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $extension = strtolower($input['file']->getClientOriginalExtension());

        $input['size'] = $input['file']->getSize() / 1024;
        $input['nama'] = str_replace(' ', '%20', $input['file']->getClientOriginalName());
        $input['type'] = $extension;
        $data = User::findOrFail($request->id_user);
        $data->ttd = $input['file']->getClientOriginalName();
        $data->save();

        $input['file']->move(public_path().'/modules/dms/ttd/',$input['file']->getClientOriginalName());

        return $this->sendResponse($data, 'File berhasil terupload');
    }

    public function setPermission(Request $request) {
        $pasisMancanegara = $request->input('pasisMancanegara');
        $user = User::find($request->id_user);

        if($user->roles[0]->name == 'bintara') {
            $siswamn = Siswa::where('matra', 'MN')->get()->pluck('nrp');

            $idUserPasisAktif = collect(User::pasis())->pluck('id')->toArray();
            $dataUser = User::with('roles')->whereHas('roles', function($query) {
                $query->whereNotIn('name', ['pasis', 'caraka']);
            })
            ->orWhereIn('id', $idUserPasisAktif); 

            if($pasisMancanegara) {
                $dataUser = $dataUser->whereNotIn('nrp', $siswamn)->get()->pluck('id');
            }else{
                $dataUser = $dataUser->get()->pluck('id');
            }

            foreach($dataUser as $id) {
                $hasRole = FileHasRole::where('id_file', $request->id_file)->where('id_role', $id)->first();

                if(!$hasRole) {
                    $file = File::find($request->id_file);
                    $folderHasRole = FolderHasRole::where('id_folder', $file->id_folder)->first();

                    if(!$folderHasRole) {
                        $permissionFolder = new FolderHasRole;
                        $permissionFolder->id_role = $id;
                        $permissionFolder->id_folder = $file->id_folder;
                        $permissionFolder->save();
                    }

                    $permission = new FileHasRole;
                    $permission->id_role = $id;
                    $permission->id_file = $request->id_file;
                    try{
                        $permission->save();
                    }catch(\Exception $e){
                        return $this->sendError($e, 'File gagal dibagikan');
                    };
                    
                }            
            }
        }else{
            $lainnya = $request->lainnya;
            $file = File::find($request->id_file);
            
            //yang melakukan share file
            $hasRoleUser = FolderHasRole::where('id_folder', $file->id_folder)->where('id_role', $request->id_user)->first();
            if(!$hasRoleUser) {
                $permission = new FolderHasRole;
                $permission->id_role = $request->id_user;
                $permission->id_folder = $file->id_folder;
                try{
                    $permission->save();
                }catch(\Exception $e){
                    return $this->sendError($e, 'Folder gagal dibagikan');
                };       
            }

            $request->id_role = collect($request->id_role)->pluck('id');

            if($lainnya['dosen']) {
                $dosen_id = collect(User::dosen())->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($dosen_id);
            }

            if($lainnya['patun']) {
                $patun_id = collect(User::patun())->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($patun_id);
            }

            if($lainnya['seluruh_pasis']) {
                $pasis_id = collect(User::pasis())->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($pasis_id);
            }else if($lainnya['kelompok_pasis'] && $lainnya['id_kelompok_pasis']) {
                $pasis_kelompok = SiswaKelompok::with('DataSiswa')
                    ->select('siswa_kelompok.id', 'siswa_id', 'siswa_kelompok_id', 'sdk.nama')
                    ->join('siswa_data_kelompok as sdk', 'sdk.id', 'siswa_kelompok.siswa_kelompok_id')
                    ->where('sekolah_angkatan_id', angkatan_aktif()['angkatan_id'])
                    ->where('tahap', strtolower(angkatan_aktif()['nama']) == 'seskoau' ? bulan_tahap() : 1)
                    ->where('siswa_kelompok_id', $lainnya['id_kelompok_pasis'])
                    ->get()
                    ->pluck('DataSiswa.nrp')
                    ->toArray();

                $kelompok_pasis = user::whereIn('nrp', $pasis_kelompok)->get()->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($kelompok_pasis);
            }

            foreach($request->id_role as $role_id) {
                $hasRole = FileHasRole::where('id_file', $request->id_file)->where('id_role', $role_id)->first();

                $folderHasRole = FolderHasRole::where('id_folder', $file->id_folder)
                                ->where('id_role', $role_id)
                                ->first();

                if(!$folderHasRole) {
                    $permissionFolder = new FolderHasRole;
                    $permissionFolder->id_role = $role_id;
                    $permissionFolder->id_folder = $file->id_folder;
                    $permissionFolder->save();
                }

                if(!$hasRole) {
                    $permission = new FileHasRole;
                    $permission->id_role = $role_id;
                    $permission->id_file = $request->id_file;
                    try{
                        $permission->save();
                    }catch(\Exception $e){
                        return $this->sendError($e, 'File gagal dibagikan');
                    };
                    
                }            
            }
        }

        return $this->sendResponse([], 'File berhasil dibagikan');
    }

    public function deleteSetPermission(Request $request) {
        $permission = FileHasRole::find($request->id);
        try{
            $permission->delete();
        }catch(\Exception $e){
            return $this->sendError($e, 'File Role gagal dihapus');
        };

        return $this->sendResponse($permission, 'File Role berhasil dihapus');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('dms::show');
    }

    public function detail($id) {
        $id_user = request()->input('id_user');
        $type = request()->input('type');

        $user = User::find($id_user);
        // if($user->roles[0]->name == 'pasis' && $type == 'public') {
        //     $data = FilePasis::with(['folder'])->find($id);
        // }else{
            if($user->roles[0]->name == 'bintara') {
                $data = File::with(['folder', 'user' => function($query) {
                    $query->select(['id', 'first_name', 'last_name']);
                }])->find($id);
                $data->has_role = [];
            }else{
                $data = File::with(['folder', 'has_role.roleLite', 'user' => function($query) {
                    $query->select(['id', 'first_name', 'last_name']);
                }])->find($id);
            }
        // }

        return $this->sendResponse($data, 'Detail File retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('dms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'id_user' => 'required',
            'nama' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find($input['id_user']);
        // if($user->roles[0]->name == 'pasis' && $input['tipe'] == 'public') {
        //     $data = FilePasis::findOrFail($id);
        // }else{
            $data = File::findOrFail($id);
        // }

        $oldFile = public_path().'/modules/dms/file/'.str_replace('%20', ' ', $data->nama);
        $newFile = public_path().'/modules/dms/file/'.str_replace('%20', ' ', $request->nama);
        if(!rename($oldFile, $newFile))
        { 
            return $this->sendError('Nama file sudah digunakan', $e);
        }

        $data->nama = str_replace(' ', '%20', $request->nama);
        $data->deskripsi = $request->deskripsi ? $request->deskripsi : null;
        $data->type = explode('.', $data->nama)[1];

        try{
            $data->save();
        }catch(\Exception $e){
            return $this->sendError('Nama file gagal diubah', $e);
        };

        return $this->sendResponse($data, 'Nama file berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $type = request()->input('type');

        $user = User::find(request()->id_user);
        // if($user->roles[0]->name == 'pasis' && $type == 'public') {
        //     $data = FilePasis::findOrFail($id);
        // }else{
            $data = File::findOrFail($id);
        // }
        $data->name = str_replace('%20', ' ', $data->name);

        //unlink foto
        $destinationPath = '/modules/dms/file/';
        $path = public_path().$destinationPath;
        $file = $path."/".$data->nama;
        if (file_exists($file) && $data->nama!='' && $data->nama!=null)
        {
            unlink($file);
        }

        $data->delete();

        return $this->sendResponse($data, 'File berhasil dihapus');
    }

    public function storeNotaDinas(Request $request) {
        return $request->all();
    }
}
