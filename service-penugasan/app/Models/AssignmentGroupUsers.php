<?php

namespace Modules\Ams\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentGroupUsers extends Model
{
    use HasFactory;

    public $label = 'Assignment Groups';

    protected $connection = 'ams';
    protected $table = 'assignment_group_users';
    protected $fillable = [
        'group_id',
        'assignment_id',
        'sbs_id',
        'description',
        'file',
        'deadline',
    ];
    protected $appends = ['start_date','deadline_date','status','status_value','url_penugasan'];


    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentGroupUsersFactory::new();
    }
    public function getUrlPenugasanAttribute() {
        if($this->file){
            $url = config('app.url').'/assets/admin/ams/groups/'.$this->file;
            $cekExist = public_path().'/assets/admin/ams/groups/'.$this->file;
            $url = config('app.url').'/assets/admin/ams/groups/'.$this->file;
            return $url;
        }else{
            return null;
        }
    }

    public function getStartDateAttribute(){
        return get_date(\Carbon\Carbon::parse($this->created_at), true);
    }

    public function tugasPasis()
    {
        return $this->hasOne('\Modules\Ams\Entities\AssignmentGroupCollect','id', 'assignment_id');
    }
    public function getStatusAttribute(){
        $cekTugas = AssignmentGroupCollect::where('assignment_id',$this->id)->first();
        $now = \Carbon\Carbon::parse(date('y-m-d'));
        $deadline = \Carbon\Carbon::parse($this->deadline);
        if($cekTugas && $now > $deadline){
            return 'selesai';
        }else if($cekTugas && $now <= $deadline){
            return 'selesai';
        }else if(!$cekTugas && $now > $deadline){
            return 'Terlewat';
        }else{
            return 'Belum dikumpulkan';
        }

    //    if(\Carbon\Carbon::parse($this->deadline) < \Carbon\Carbon::parse(date('y-m-d')) && !$cekTugas){
    //         return 'Belum Mengumpulkan';
    //    }else if(\Carbon\Carbon::parse($this->deadline) < \Carbon\Carbon::parse(date('y-m-d')) && $cekTugas){
    //         return 'selesai';
    //    }else if(\Carbon\Carbon::parse($this->deadline) > \Carbon\Carbon::parse(date('y-m-d')) && !$cekTugas){
    //       return 'Terlewat';
    //    }elseif (\Carbon\Carbon::parse($this->deadline) > \Carbon\Carbon::parse(date('y-m-d')) && $cekTugas) {
    //        return 'Selesai';
       
    }
    public function getStatusValueAttribute(){
        $cekTugas = AssignmentGroupCollect::where('assignment_id',$this->id)->first();
        $now = \Carbon\Carbon::parse(date('y-m-d'));
        $deadline = \Carbon\Carbon::parse($this->deadline);
        if($cekTugas && $now > $deadline){
            return 'finish_selesai';
        }else if($cekTugas && $now <= $deadline){
            return 'selesai';
        }else if(!$cekTugas && $now > $deadline){
            return 'Terlewat';
        }else{
            return 'Belum dikumpulkan';
        }
    }
    public function getDeadlineDateAttribute(){
        return get_date(\Carbon\Carbon::parse($this->deadline), true);
    }
}
