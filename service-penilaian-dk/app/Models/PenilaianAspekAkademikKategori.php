<?php

namespace App\Models\Penilaian;

use Illuminate\Database\Eloquent\Model;

class PenilaianAspekAkademikKategori extends Model {
    protected $connection = 'penilaian';
    protected $table   = 'penilaian_aspek_akademik_kategori';

    public function Form(){
        return $this->belongsTo(PenilaianFormAkademik::class,'penilaian_form_akademik_id');
    }

}
