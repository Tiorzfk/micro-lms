<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttachmentNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_attachment_nota_dinas';
    protected $fillable = ['id_nota_dinas' , 'tipe' , 'file'];
}
