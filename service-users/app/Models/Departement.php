<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Departement extends Model
{
    use HasFactory;

    public $label = 'Departemen';
    protected $connection = 'admin';

    protected $table ='departement';

    protected $fillable = ['id','kode','name','color','description','created_at','updated_at'];
    
    protected static function newFactory()
    {
        return \Modules\Admin\Database\factories\DepartementFactory::new();
    }
}
