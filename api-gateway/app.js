require('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const materiRouter = require('./routes/materi');
const bankSoalRouter = require('./routes/bankSoal');
const bimbinganRouter = require('./routes/bimbingan');
const chattingRouter = require('./routes/chatting');
const dokumenRouter = require('./routes/dokumen');
const penilaianDKRouter = require('./routes/penilaian_dk');
const penugasanRouter = require('./routes/penugasan');
const suratRouter = require('./routes/surat');
const timelineRouter = require('./routes/timeline');
const ujianRouter = require('./routes/ujian');

const verifyToken = require('./middlewares/verifyToken');
const can = require('./middlewares/permission');

const app = express();

app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false, limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/materi', materiRouter);
app.use('/bank-soal', bankSoalRouter);
app.use('/bimbingan', bimbinganRouter);
app.use('/chatting', chattingRouter);
app.use('/dokumen', dokumenRouter);
app.use('/penilaian-dk', penilaianDKRouter);
app.use('/penugasan', penugasanRouter);
app.use('/surat', suratRouter);
app.use('/timeline', timelineRouter);
app.use('/ujian', ujianRouter);
// app.use('/chapters', verifyToken, can('admin'), chaptersRouter);

module.exports = app;
