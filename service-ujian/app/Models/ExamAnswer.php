<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamAnswer extends Model
{
    use HasFactory;

    public $label = 'Exam Answer';

    protected $connection = 'elearning';
    protected $table = 'exam_answer';
    protected $fillable = ['exam_id', 'question_id', 'answer', 'isMarked', 'user_id'];
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\ExamAnswerFactory::new();
    }
}
