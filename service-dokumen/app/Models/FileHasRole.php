<?php

namespace Modules\DMS\Entities;

use Illuminate\Database\Eloquent\Model;

class FileHasRole extends Model
{
    protected $table = 'dms_file_has_role';

    protected $fillable = ['id_file', 'id_role'];

    public function role()
    {
        return $this->hasOne('\App\Models\Auth\User', 'id', 'id_role');
    }

    public function roleLite()
    {
        return $this->hasOne('\App\Models\Auth\UserLite', 'id', 'id_role');
    }
}
