const apiAdapter = require('../../apiAdapter');
const {
  BIMBINGAN_APPS_SERVICE_HOST,
  BIMBINGAN_APPS_SERVICE_PORT,
} = process.env;

const api = apiAdapter(BIMBINGAN_APPS_SERVICE_HOST, BIMBINGAN_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });
    
    const id = req.params.id;

    const bimbingan = await api.get(`/bimbingan/${id}/upload-naskah`, req.body);
    return res.json(bimbingan.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}