<?php

namespace Modules\DMS\Entities;

use Illuminate\Database\Eloquent\Model;

class FilePasis extends Model
{
    protected $table = 'dms_file_pasis';

    protected $fillable = ['id_user', 'id_folder', 'nama', 'deskripsi', 'type', 'size'];

    public function folder()
    {
        return $this->hasOne('\Modules\DMS\Entities\Folder', 'id', 'id_folder');
    }
}
