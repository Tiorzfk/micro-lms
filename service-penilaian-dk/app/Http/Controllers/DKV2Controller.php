<?php

namespace Modules\PenilaianAkademik\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseApiController;
use App\Models\Kepribadian\PenilaianKksDetail;

use App\Models\Akademik\Siswa;
use App\Models\Penilaian\PenilaianAkademik;
use App\Models\Penilaian\PenilaianFormAkademik;
use App\Models\Akademik\SiswaKelompok;
use App\Models\Akademik\Departemen;
use App\Models\Akademik\Sekolah;
use App\Models\Akademik\SekolahAngkatan;
use App\Models\Akademik\DataPatun;
use App\Models\Penilaian\PenilaianAspekAkademikKategoriSesau;
use App\Models\Penilaian\PenilaianAspekAkademikKategori;
use App\Models\Penilaian\PenilaianDetailAkademik;
use App\Models\Penilaian\PenilaianAspekAkademik;
use App\Models\Akademik\NilaiAkhirAkademik;
use App\Models\Akademik\KurikulumRpp;
use App\Models\Akademik\PatunKelompok;

class DKV2Controller extends BaseApiController {

    public function getPasis(Request $request) {
        $tahap = $request->tahap ? $request->tahap : bulan_tahap();

        $angkatan = SekolahAngkatan::findOrfail($request->angkatan);
        $penilaianAkademik = PenilaianAkademik::where('id',$request->id)->firstOrFail();

        $dataSiswa = SiswaKelompok::with('DataSiswa')
            ->where('sekolah_angkatan_id',$angkatan->id)
            ->where('siswa_kelompok_id',$penilaianAkademik->siswa_data_kelompok_id)
            ->where('tahap',$tahap)
            ->has('DataSiswa')
            ->get();

        $dataSiswa->transform(function($item){
            $app = app();
            $object = $app->make('stdClass');
            $object->id = $item->DataSiswa->id;
            $object->no_pasis = $item->DataSiswa->no_pasis;
            $object->nama = $item->DataSiswa->nama;

            return $object;
        });
        $collection = collect($dataSiswa);
        $sortedNopasis = $collection->sortBy('no_pasis');
        $siswa = $sortedNopasis->values()->all();

        return $this->sendResponse(200, 'Request data berhasil', $siswa);
    }

    public function getPreview(Request $request) {
        $nrp = $request->nrp;
        $tahap = $request->tahap ? $request->tahap : bulan_tahap();

        $angkatan = SekolahAngkatan::findOrfail($request->angkatan);
        $penilaianAkademik = PenilaianAkademik::where('id',$request->id)->firstOrFail();
        $dataSiswa = SiswaKelompok::with('DataSiswa')
            ->where('sekolah_angkatan_id',$angkatan->id)
            ->where('siswa_kelompok_id',$penilaianAkademik->siswa_data_kelompok_id)
            ->where('tahap',$tahap)
            ->has('DataSiswa')
            ->get();
        
        $dataSiswa->transform(function($query) use ($nrp, $request, $angkatan) {
            $total_nilai = PenilaianDetailAkademik::query()
            ->whereHas('AspekPenilaian',function($query)use($request){
                return $query->where('penilaian_form_akademik_id',$request->form);
            })
            ->where('penilaian_akademik_id',$request->id)
            ->where('data_patun_id', $this->getDataPatunKelompok($nrp))
            ->where('siswa_id',$query->DataSiswa->id);

            $d['id'] = $query->DataSiswa->id;
            $d['no_pasis'] = $query->DataSiswa->no_pasis;
            $d['nama'] = $query->DataSiswa->nama; 
            $d['status'] = $total_nilai->first() ? $total_nilai->first()->status : '-'; 
        
            // 4 = Seskoau
            // 5 = Sesau
            if($angkatan->sekolah_id == 4){
                $aspekPenilaian = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form)
                ->where('nilai','1')
                ->count();

                $d['nilai'] = number_format(($total_nilai->sum('nilai'))/$aspekPenilaian,2);
            }else{
                $d['nilai'] = number_format(($total_nilai->sum('nilai')),2);
            };

            return $d;
        });  
        
        $collection = collect($dataSiswa);
        $sortedNopasis = $collection->sortBy('no_pasis');
        $siswa = $sortedNopasis->values()->all();

        return $this->sendResponse(200, 'Request data berhasil', $siswa);
    }

    public function getDataPatunKelompok($nrp = null){
        $angkatan_aktif = angkatan_aktif();
        $angkatan_aktif_sesau_sesko = angkatan_aktif_sesau_sesko();

        $getPatun = DataPatun::with('patunkelompok.kelompok')
                        ->where(function($query) use ($angkatan_aktif_sesau_sesko) {
                            $query->where('sekolah_angkatan_id', $angkatan_aktif_sesau_sesko[0]['angkatan_id'])
                                ->orWhere('sekolah_angkatan_id', $angkatan_aktif_sesau_sesko[1]['angkatan_id']);
                        })
                        ->where('nrp',$nrp ? $nrp : \Auth::user()->nrp)->firstOrFail();
        if($getPatun){
            return $getPatun->id;
        }else{
            return null;
        }
    }

    public function getAspek(Request $request) {
        $angkatan = SekolahAngkatan::find($request->angkatan);

        $aspeks = PenilaianAspekAkademik::select('id', 'penilaian_form_akademik_id', 'parent_id', 'nomor', 'nama', 'nilai')
        ->where('penilaian_form_akademik_id',$request->form)
        ->where('parent_id', 0)
        ->orderBy('nomor', 'ASC')
        ->get();

        $aspeks->transform(function($aspek) use ($angkatan, $request) {
            $kategorNilai_0 = 0;
            $kategorNilai_1 = 0;
            $kategorNilai_2 = 0;
            $kategorNilai_3 = 0;
            $kategorNilai_4 = 0;

            $a['id'] = $aspek->id;
            $a['nilai'] = $aspek->nilai;
            $a['aspek'] = '<span style="font-weight: bold;">'.$aspek->nomornama.'</span>';
            //Kategori Nilai
            if($a['nilai'] == 1){
                $kategorNilai = $this->getKategoriNilai($angkatan,$aspek->id,$request);
                if(count($kategorNilai) > 0) {
                    foreach ($kategorNilai as $key_  => $kat) {
                        //Get Total Kategori
                        switch ($key_ ) {
                            case 0:
                                $kategorNilai_0+=$kat->nilai;
                                break;
                            case 1:
                                $kategorNilai_1+=$kat->nilai;
                                break;
                            case 2:
                                $kategorNilai_2+=$kat->nilai;
                                break;
                            case 3:
                                $kategorNilai_3+=$kat->nilai;
                                break;
                            case 4:
                                $kategorNilai_4+=$kat->nilai;
                                break;
                        }
                        $a['kategori'][$key_] = $kat->nilai;
                        $a['kategori_nama'][$key_] = $kat->nama;
                    }
                }else{
                    $a['kategori'] = [];
                    $a['kategori_nama'] = [];
                }

                $a['nilai_pasis'] = $this->getNilaiAspkePasis($aspek->id,$request);
            }

            //Child
            $childs = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form)
                ->where('parent_id', $aspek->id)
                ->get();

            $childs->transform(function($child) use ($angkatan, $request, $a) {
                $kategorNilai_0 = 0;
                $kategorNilai_1 = 0;
                $kategorNilai_2 = 0;
                $kategorNilai_3 = 0;
                $kategorNilai_4 = 0;

                $c['id'] = $child->id;
                $c['aspek'] = '<span style="padding-left: 20px;">'.$child->nomornama.'</span>';
                $c['nilai'] = $child->nilai;

                //Child Kategori Nilai
                if($child->nilai == 1){
                    $kategorNilai = $this->getKategoriNilai($angkatan,$child->id,$request);
                    
                    if(count($kategorNilai) > 0) {
                        foreach ($kategorNilai as $key_ => $kat) {
                            //Get Total Kategori
                            switch ($key_ ) {
                                case 0:
                                    $kategorNilai_0+=$kat->nilai;
                                    break;
                                case 1:
                                    $kategorNilai_1+=$kat->nilai;
                                    break;
                                case 2:
                                    $kategorNilai_2+=$kat->nilai;
                                    break;
                                case 3:
                                    $kategorNilai_3+=$kat->nilai;
                                    break;
                                case 4:
                                    $kategorNilai_4+=$kat->nilai;
                                    break;
                            }
                            $c['kategori'][$key_] = $kat->nilai;
                            $c['kategori_nama'][$key_] = $kat->nama;
                        }
                    }else{
                        $c['kategori'] = [];
                        $c['kategori_nama'] = [];
                    }

                    $c['nilai_pasis'] = $this->getNilaiAspkePasis($child->id,$request);
                }

                //Child 2
                $childs_2 = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form)
                    ->where('parent_id', $child->id)
                    ->get();
                
                $childs_2->transform(function($child_2) use ($angkatan, $request, $a) {
                    $kategorNilai_0 = 0;
                    $kategorNilai_1 = 0;
                    $kategorNilai_2 = 0;
                    $kategorNilai_3 = 0;
                    $kategorNilai_4 = 0;

                    $d['id'] = $child_2->id;
                    $d['aspek'] = '<span style="padding-left: 50px;">'.$child_2->nomornama.'</span>';
                    $d['nilai'] = $child_2->nilai;

                    //Kategori Nilai
                    if($child_2->nilai == 1){
                        $kategorNilai = $this->getKategoriNilai($angkatan,$child_2->id,$request);
                        if(count($kategorNilai) > 0) {
                            foreach ($kategorNilai as $key_ => $kat) {
                                //Get Total Kategori
                                switch ($key_ ) {
                                    case 0:
                                        $kategorNilai_0+=$kat->nilai;
                                        break;
                                    case 1:
                                        $kategorNilai_1+=$kat->nilai;
                                        break;
                                    case 2:
                                        $kategorNilai_2+=$kat->nilai;
                                        break;
                                    case 3:
                                        $kategorNilai_3+=$kat->nilai;
                                        break;
                                    case 4:
                                        $kategorNilai_4+=$kat->nilai;
                                        break;
                                }
                                $d['kategori'][$key_] = $kat->nilai;
                            }
                        }else{
                            $d['kategori'] = [];
                            $d['kategori_nama'] = [];
                        }

                        $d['nilai_pasis'] = $this->getNilaiAspkePasis($child_2->id,$request);
                    }

                    return $d;
                });

                $c['child_2'] = $childs_2;

                return $c;
            });

            $a['child'] = $childs;

            return $a;
        });

        $footer = [[
            'id' => 0,
            'aspek' => '<span style="font-weight: bold;padding-left:50%;">Total</span>',
            'nilai' => '0',
            'kategori' => [],
            'nilai_pasis' => 0,
            'child' => []
        ]];
        $aspeks = collect($aspeks)->merge($footer);

        return $this->sendResponse(200, 'Request data berhasil', $aspeks);
    }

    /**
     * Get Kategori Nilai
     */
    public function getKategoriNilai($angkatan,$aspek,$request){
        // 4 = Seskoau
        // 5 = Sesau
        if($angkatan->sekolah_id == 4){
            $kategorNilai  = PenilaianAspekAkademikKategori::query()
            ->where('penilaian_form_akademik_id',$request->form)
            ->get();
            //Kategori Nilai
            return $kategorNilai;
        }else{
            //Kategori Nilai
            $kategorNilaiSesau = PenilaianAspekAkademikKategoriSesau::query()
                ->select('id',\DB::raw('kategori_nilai as nilai'))
                ->where('penilaian_aspek_akademik_id',$aspek)
                ->OrderBy('kategori_nilai','desc')
                ->get();

            return $kategorNilaiSesau;
        }
    }

    /**
     * Get Data Nilai Pasis Per Asepk
     */
    public function getNilaiAspkePasis($aspek,$request){
        $dataNilai = PenilaianDetailAkademik::where('penilaian_akademik_id',$request->id)
            ->where('penilaian_aspek_akademik_id',$aspek)
            ->where('data_patun_id', $this->getDataPatunKelompok($request->nrp))        
            ->where('siswa_id',$request->siswa_id)
            ->first();

        if($dataNilai){
            return $dataNilai->nilai;
        }else{
            return '';
        }
    }

    public function storeNilaiDraft(Request $request) {
        $insertDetailNilai = [];
        $angkatan = SekolahAngkatan::findOrfail($request->angkatan);

        $total_nilai_before = PenilaianDetailAkademik::query()
            ->whereHas('AspekPenilaian',function($query)use($request){
                return $query->where('penilaian_form_akademik_id',$request->form_id);
            })
            ->where('penilaian_akademik_id',$request->penilaian_akademik_id)
            ->where('data_patun_id', $this->getDataPatunKelompok($request->nrp))
            ->where('siswa_id',$request->siswa_id);
        
        if($angkatan->sekolah_id == 4){
            $aspekPenilaian = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form_id)
            ->where('nilai','1')
            ->count();

            $nilai_before = number_format(($total_nilai_before->sum('nilai'))/$aspekPenilaian,2);
        }else{
            $nilai_before = number_format(($total_nilai_before->sum('nilai')),2);
        };

        foreach($request->penilaian_aspek as $aspek) {
            $aspek = (Object) $aspek;
            if($aspek->nilai) {
                $nilai = [
                    'penilaian_akademik_id' => $request->penilaian_akademik_id,
                    'penilaian_aspek_akademik_id' => $aspek->penilaian_aspek_akademik_id,
                    'data_patun_id' => $this->getDataPatunKelompok($request->nrp),
                    'siswa_id' => $request->siswa_id,
                    'nilai' => $aspek->nilai,
                    'keterangan' => '',
                    'status' => 'draft',
                ];

                $CekNilai =  PenilaianDetailAkademik::where('penilaian_akademik_id', $request->penilaian_akademik_id)
                            ->where('penilaian_aspek_akademik_id', $aspek->penilaian_aspek_akademik_id)
                            ->where('siswa_id', $request->siswa_id)
                            ->where('data_patun_id', $this->getDataPatunKelompok($request->nrp))
                            ->update([
                                'nilai' => $aspek->nilai,
                                'status' => 'draft',
                            ]);

                if($CekNilai > 1) {
                    $CekNilai =  PenilaianDetailAkademik::where('penilaian_akademik_id', $request->penilaian_akademik_id)
                    ->where('penilaian_aspek_akademik_id', $aspek->penilaian_aspek_akademik_id)
                    ->where('siswa_id', $request->siswa_id)
                    ->where('data_patun_id', $this->getDataPatunKelompok($aspek->nrp))
                    ->delete();

                    // dd($CekNilai);

                    $insertDetailNilai = PenilaianDetailAkademik::insert($nilai);
                }else if(!$CekNilai){
                    $insertDetailNilai = PenilaianDetailAkademik::insert($nilai);
                }
            }
        }

        $total_nilai_after = PenilaianDetailAkademik::query()
            ->whereHas('AspekPenilaian',function($query)use($request){
                return $query->where('penilaian_form_akademik_id',$request->form_id);
            })
            ->where('penilaian_akademik_id',$request->penilaian_akademik_id)
            ->where('data_patun_id', $this->getDataPatunKelompok($request->nrp))
            ->where('siswa_id',$request->siswa_id);

        if($total_nilai_after->count() > 0) {
            // 4 = Seskoau
            // 5 = Sesau

            if($angkatan->sekolah_id == 4){
                $aspekPenilaian = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form_id)
                ->where('nilai','1')
                ->count();

                $nilai_after = number_format(($total_nilai_after->sum('nilai'))/$aspekPenilaian,2);
            }else{
                $nilai_after = number_format(($total_nilai_after->sum('nilai')),2);
            };

            $siswa = Siswa::find($request->siswa_id);
            $pa = PenilaianAkademik::find($request->penilaian_akademik_id);

            if($siswa && $pa && $nilai_after != $nilai_before) insert_log('penilaian-dk-simpan' , 'Memberikan nilai '.$nilai_after.' kepada pasis '.$siswa->nama.' pada kegiatan penilaian DK yang berjudul '.$pa->judul_latihan , Auth()->user()->id);
        }

        return $this->sendResponse(200, 'Request simpan data berhasil', $insertDetailNilai);
    }

    public function storeNilaiDepartemen(Request $request) {
        $kirim = [];
        
        foreach($request->penilaian_aspek as $aspek) {
            $aspek = (Object) $aspek;

            $kirim =  PenilaianDetailAkademik::where('penilaian_akademik_id', $aspek->penilaian_akademik)
                ->where('siswa_id', $aspek->siswa_id)
                ->where('data_patun_id', $this->getDataPatunKelompok($aspek->nrp))
                ->whereIn('penilaian_aspek_akademik_id', $request->aspek)
                ->update([
                    'status' => 'kirim',
                ]);
        }

        $this->calculate_nilaiaka_dk($request);
        return $this->sendResponse(200, 'Request simpan data berhasil', $kirim);
    }

    public function rekapNilai(Request $request) {
        $tahap = $request->tahap ? $request->tahap : bulan_tahap();

        $angkatan_aktif = angkatan_aktif();

        $penilaianAkademik = PenilaianAkademik::where('id',$request->nilai_id )->firstOrFail();

        $dataSiswa = SiswaKelompok::with('DataSiswa')
            ->where('sekolah_angkatan_id',$request->angkatan)
            ->where('siswa_kelompok_id',$penilaianAkademik->siswa_data_kelompok_id)
            ->where('tahap',$tahap)
            ->has('DataSiswa')
            ->get();
            
        $dataSiswa->transform(function($item) use ($angkatan_aktif, $request){
            //Get Rata2 Nilai
            $rata2Kbk = $this->getRata2Kbk($item->DataSiswa->id, $request, $angkatan_aktif);
            $rata2Kme = $this->getRata2Kme($item->DataSiswa->id, $request, $angkatan_aktif);
            $rata2Kbe = $this->getRata2Kbe($item->DataSiswa->id, $request, $angkatan_aktif);
            $rata2Kks = $this->getRata2Kks($item->DataSiswa->id, $request, $angkatan_aktif);

            // $rumus_nilai= (($rata2Kbk)+($rata2Kme)+($rata2Kbe)+($rata2Kks));
            // $jml_nilai_akhir = $rumus_nilai/4;

            // //5 = Sesau
            // //4 = Seskoau
            // if($angkatan_aktif['sekolah_id'] == 5){
               
            // }else{
            //     //Get Jumlah Nilai Akhir
            //     /*
            //     ** RUMUS
            //     ** (5*KBK+3*KME+2KBE)/10
            //     */
            //     $rumus_nilai= (($rata2Kbk*5)+($rata2Kme*3)+($rata2Kbe*2));
            //     $jml_nilai_akhir = $rumus_nilai/10;
            // }
            $rumus_nilai= (($rata2Kbk*5)+($rata2Kme*3)+($rata2Kbe*2));
            $jml_nilai_akhir = $rumus_nilai/10;

            $d['no_pasis']      = $item->DataSiswa->no_pasis;
            $d['nama']          = $item->DataSiswa->nama;
            $d['jml_nilai_kbk'] = $rata2Kbk;
            $d['jml_nilai_kme'] = $rata2Kme;
            $d['jml_nilai_kbe'] = $rata2Kbe;
            $d['jml_nilai_kks'] = $rata2Kks;
            $d['jml_akhir']     = number_format($jml_nilai_akhir, 3);
            $d['keterangan']    = '';

            return $d;
        });

        $collection = collect($dataSiswa);
        $sortedNopasis = $collection->sortBy('no_pasis');
        $siswas = $sortedNopasis->values()->all();

        $return['total'] = count($siswas);
        $return['rows']  = $siswas;

        return $this->sendResponse(200, 'Request data berhasil', $return);
    }


    //Get DATA RATA2 KKS
    public function getRata2Kks($siswa_id , $request , $angkatan_aktif)
    {
        $data = PenilaianKksDetail::where('siswa_id' , $siswa_id)
                                    ->where('penilaian_akademik_id' , $request->id)
                                    ->where('data_patun_id' , $this->getDataPatunKelompok($request->nrp))
                                    ->sum('nilai');
      
        return $data/2;
    }

    //Get Data Rata2 KBK/KKA
    public function getRata2Kbk($siswa_id, $request, $angkatan_aktif){
        $data = PenilaianDetailAkademik::select('penilaian_detail_akademik.nilai')
            ->leftJoin('penilaian_aspek_akademik', 'penilaian_detail_akademik.penilaian_aspek_akademik_id', '=', 'penilaian_aspek_akademik.id')
            ->where('penilaian_form_akademik_id',$request->form_kbk)
            ->where('penilaian_akademik_id',$request->nilai_id)
            ->where('siswa_id',$siswa_id)
            ->where('data_patun_id',$this->getDataPatunKelompok($request->nrp))
            ->sum('penilaian_detail_akademik.nilai');

        //5 = Sesau
        //4 = Seskoau
        if($angkatan_aktif['sekolah_id'] == 5){
            $format = number_format($data, 2);
        }else{
            $aspekPenilaian = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form_kbk)
                ->where('nilai','1')
                ->count();
            
            $rata2  = ($data/$aspekPenilaian);
            //Format 3 digit
            $format = number_format($rata2, 2);
            
        }
    
        return $format;
    }

    //Get Data Rata2 KME
    public function getRata2Kme($siswa_id, $request, $angkatan_aktif){
        $data = PenilaianDetailAkademik::select('penilaian_detail_akademik.nilai')
            ->leftJoin('penilaian_aspek_akademik', 'penilaian_detail_akademik.penilaian_aspek_akademik_id', '=', 'penilaian_aspek_akademik.id')
            ->where('penilaian_form_akademik_id',$request->form_kme)
            ->where('penilaian_akademik_id',$request->nilai_id)
            ->where('siswa_id',$siswa_id)
            ->where('data_patun_id',$this->getDataPatunKelompok($request->nrp))
            ->sum('penilaian_detail_akademik.nilai');

        
        //5 = Sesau
        //4 = Seskoau
        if($angkatan_aktif['sekolah_id'] == 5){
            $format = number_format($data, 2);
        }else{
            $aspekPenilaian = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form_kme)
            ->where('nilai','1')
            ->count();
        
            $rata2  = ($data/$aspekPenilaian);
            //Format 2 digit
            $format = number_format($rata2, 2);
            
        }
        
        return $format;
    }

    //Get Data Rata2 KBE
    public function getRata2Kbe($siswa_id, $request, $angkatan_aktif){
        $data = PenilaianDetailAkademik::select('penilaian_detail_akademik.nilai')
            ->leftJoin('penilaian_aspek_akademik', 'penilaian_detail_akademik.penilaian_aspek_akademik_id', '=', 'penilaian_aspek_akademik.id')
            ->where('penilaian_form_akademik_id',$request->form_kbe)
            ->where('penilaian_akademik_id',$request->nilai_id)
            ->where('siswa_id',$siswa_id)
            ->where('data_patun_id',$this->getDataPatunKelompok($request->nrp))
            ->sum('penilaian_detail_akademik.nilai');
        
        //5 = Sesau
        //4 = Seskoau
        if($angkatan_aktif['sekolah_id'] == 5){
            $format = number_format($data, 2);
        }else{
            $aspekPenilaian = PenilaianAspekAkademik::where('penilaian_form_akademik_id',$request->form_kbe)
            ->where('nilai','1')
            ->count();
        
            $rata2  = ($data/$aspekPenilaian);
            //Format 3 digit
            $format = number_format($rata2, 3);
        }
    
        return $format;
    }

    public function calculate_nilaiaka_dk($request) {
        $penilaianAkademik = PenilaianAkademik::where('id',$request->nilai_id)->firstOrFail();
        $angkatan_aktif = angkatan_aktif();

        $dataSiswa = SiswaKelompok::with('DataSiswa')
            ->where('sekolah_angkatan_id',$request->angkatan_id)
            ->where('siswa_kelompok_id',$penilaianAkademik->siswa_data_kelompok_id)
            ->where('tahap',$request->tahap)
            ->has('DataSiswa')
            ->get();

        $patunKelompok = PatunKelompok::has('patun')
            ->with('patun')
            ->where('sekolah_angkatan_id',$request->angkatan_id)
            ->where('tahap',$request->tahap)
            ->where('siswa_data_kelompok_id',$penilaianAkademik->siswa_data_kelompok_id)
            ->get();

        foreach($dataSiswa as $item) {
            $jml_nilai_akhir_patun = 0;
            $jml_patun = 0;

            foreach($patunKelompok as $ip => $p) {
                //Get Rata2 Nilai
                $request->nrp = $p->patun->nrp;
                $rata2Kbk = $this->getRata2Kbk($item->DataSiswa->id, $request, $angkatan_aktif);
                $rata2Kme = $this->getRata2Kme($item->DataSiswa->id, $request, $angkatan_aktif);
                $rata2Kbe = $this->getRata2Kbe($item->DataSiswa->id, $request, $angkatan_aktif);
                // $rata2Kks = $this->getRata2Kks($item->DataSiswa->id, $request, $angkatan_aktif);
                $rata2Kks = 0;

                $rumus_nilai= (($rata2Kbk*5)+($rata2Kme*3)+($rata2Kbe*2));
                $jml_nilai_akhir = $rumus_nilai/10;
                if($jml_nilai_akhir > 0)
                {
                    $jml_patun += 1;
                }
                $jml_nilai_akhir_patun += $jml_nilai_akhir;

                // save nilai per patun
                $rataNilai = ['kbk', 'kbe', 'kme'];
                if((string)\Auth::user()->nrp == $p->patun->nrp)
                {
                    foreach ($rataNilai as $tipe) {
                        $param = [
                            'siswa_id' => $item->DataSiswa->id,
                            'tahap' => $request->tahap,
                            'tipe' => 'dk-'.$tipe,
                            'kurikulum_rpp_id' => $penilaianAkademik->sbs_id,
                            'penilai_id' => $p->patun->id,
                            'kegiatan_id' => $penilaianAkademik->id,
                        ];
                        $cekNilaiPatun = NilaiAkhirAkademik::where($param);
                        if($tipe == 'kbk')
                        {
                            $nilaiForm = number_format($rata2Kbk, 3);
                        }else if($tipe == 'kbe')
                        {
                            $nilaiForm = number_format($rata2Kbe, 3);
                        }else
                        {
                            $nilaiForm = number_format($rata2Kme, 3);
                        }
                        if($cekNilaiPatun->count() > 0) {
                            $cekNilaiPatun->update([
                                'nilai' => $nilaiForm
                            ]);
                        }else{
                            $kur = KurikulumRpp::with('parent')->where('id', $penilaianAkademik->sbs_id)->first();
                            $param['departemen_id'] = $kur ? $kur->parent->kode : 0;
                            $param['nilai'] = $nilaiForm;
                            $cekNilaiPatun->create($param);
                        }
                    }
                }
            }

            $param = [
                'siswa_id' => $item->DataSiswa->id,
                'tahap' => $request->tahap,
                'tipe' => 'dk',
                'kurikulum_rpp_id' => $penilaianAkademik->sbs_id,
                'kegiatan_id' => $penilaianAkademik->id,
            ];
            
            $cek = NilaiAkhirAkademik::where($param);
            if($cek->count() > 0) {
                $cek->update([
                    'nilai' => $jml_patun > 0 ? number_format($jml_nilai_akhir_patun, 3) / $jml_patun : 0
                ]);
            }else{
                $p = KurikulumRpp::with('parent')->where('id', $penilaianAkademik->sbs_id)->first();
                $param['departemen_id'] = $p ? $p->parent->kode : 0;
                $param['nilai'] = $jml_patun > 0 ? number_format($jml_nilai_akhir_patun, 3) / $jml_patun : 0;
                $cek->create($param);
            }
        }
    }
}

?>