<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ChatPeople extends Model
{
    use HasFactory;

    public $label = 'Chat Friend';

    protected $connection = 'ams';
    protected $table = 'chat_people';
    protected $fillable = [
        'user_id',
        'user_id_friend',
    ];

    public function chat()
    {
        return $this->hasOne('\Modules\Ams\Entities\ChatPersonal','id', 'user_id');
    }
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\ChatPeopleFactory::new();
    }
}
