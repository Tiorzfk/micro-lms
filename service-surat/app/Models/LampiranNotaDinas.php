<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LampiranNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_lampiran_nota_dinas';
    protected $fillable = ['id_nota_dinas', 'lampiran'];
}
