<?php

namespace App\Models\Penilaian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Penilaian\PenilaianAspekAkademikKategoriSesau;

class PenilaianAspekAkademik extends Model {
    protected $connection = 'penilaian';
    protected $table   = 'penilaian_aspek_akademik';
    protected $appends = ['nomornama','action'];
    
    //Kategor Nilai Sesau
    public function KategoriNilaiSesau(){
        return $this->hasMany(PenilaianAspekAkademikKategoriSesau::class,'penilaian_aspek_akademik_id','id');
    }

    //recursive parent id
    public function parent() {
        return $this->belongsTo(PenilaianAspekAkademik::class, 'parent_id', 'id');
    }
    
    public function children() {
        return $this->hasMany(PenilaianAspekAkademik::class, 'parent_id', 'id');
    }

    //Append Field Nomor - Nama
    function getNomornamaAttribute(){
        return $this->nomor.'. '.$this->nama;
    }
    
    //Append Field Action
    function getActionAttribute() {
        if($this->parent_id != 0){
            return '
            <button class="btn btn-xs btn-primary" id="tambah" data-parent_id="'.$this->id.'"><i class="fa fa-plus"></i></button>
            <button class="btn btn-xs btn-warning" data-id="'.$this->id.'" data-nomor="'.$this->nomor.'" data-nama="'.$this->nama.'" data-nilai="'.$this->nilai.'" ><i class="fa fa-edit"> </i></button>
            <button class="btn btn-xs btn-danger hapus" data-id="'.$this->id.'" data-url="'.route('akademik.penilaian.aspek.delete').'" style="cursor:pointer;" ><i class="fa fa-times"></i></button>
            ';
        }else{
            return '
            <button class="btn btn-xs btn-primary" id="tambah" data-parent_id="'.$this->id. '"><i class="fa fa-plus"></i></button>
            <button class="btn btn-xs btn-warning" data-id="'.$this->id.'" data-nomor="'.$this->nomor.'" data-nama="'.$this->nama.'" data-nilai="'.$this->nilai.'" ><i class="fa fa-edit"> </i></button>
            <button class="btn btn-xs btn-danger hapus" data-id="'.$this->id.'" data-url="'.route('akademik.penilaian.aspek.delete').'" style="cursor:pointer;" ><i class="fa fa-times"></i></button>
            ';
        }
    }
    
}
