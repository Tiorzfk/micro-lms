const apiAdapter = require('../../apiAdapter');
const {
  USERS_APPS_SERVICE_HOST,
  USERS_APPS_SERVICE_PORT,
} = process.env;

const api = apiAdapter(USERS_APPS_SERVICE_HOST, USERS_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    const id = req.user.data.id;
    const user = await api.post(`/logout`, { user_id: id });
    return res.json(user.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}