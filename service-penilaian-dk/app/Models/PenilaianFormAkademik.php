<?php

namespace App\Models\Penilaian;
use App\Models\Akademik\KurikulumSkepJuknis;

use Illuminate\Database\Eloquent\Model;

class PenilaianFormAkademik extends Model {
    protected $connection = 'penilaian';
    protected $table = 'penilaian_form_akademik';

    protected $append = ['skep'];

    public function getSkepAttribute() {
        return KurikulumSkepJuknis::find($this->kurikulum_skep_juknis_id);
    }

    public function SkepJuknis(){
        return $this->belongsTo(KurikulumSkepJuknis::class,'kurikulum_skep_juknis_id');
    }

    public function KategoriNilai(){
        return $this->hasMany(PenilaianAspekAkademikKategori::class,'penilaian_form_akademik_id');
    }

}
