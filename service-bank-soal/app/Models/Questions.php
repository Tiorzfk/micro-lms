<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\QuestionAnswers;
class Questions extends Model
{
    use HasFactory;
    protected $table = 'questions';
    protected $fillable = ['name','created_by','mp_id','category_id','name','file'];
    protected $appends = ['index_answer','first_answer'];
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\QuestionsFactory::new();
    } 

    public function kategori()
    {
        return $this->hasOne('App\Models\QuestionCategory', 'id', 'category_id');
    }

    public function rightAnswer()
    {
        return $this->hasMany('App\Models\QuestionAnswers', 'question_id', 'id')->orderBy('id','ASC');
    }
    
    public function getIndexAnswerAttribute()
    {
        $data = QuestionAnswers::select('id','istrue','question_id')->where('question_id', $this->id)->orderBy('id','ASC')->get();
        $idx = [];
        if(count($data) > 0){
            foreach ($data as $key => $value) {
                if($value->istrue == '1'){
                    array_push($idx,$key);
                }
            }
            if(count($idx) > 0){
                return $idx[0];
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    public function getFirstAnswerAttribute()
    {
        $data = QuestionAnswers::where('question_id', $this->id)->where('istrue', '1')->orderBy('id','ASC')->get();
        return $data;
    }
}
