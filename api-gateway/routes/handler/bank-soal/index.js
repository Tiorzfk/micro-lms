const getBankSoal = require('./getBankSoal');
const getDetailBankSoal = require('./getDetailBankSoal');
const getCategory = require('./getCategory');
const addBankSoal = require('./addBankSoal');
const editBankSoal = require('./editBankSoal');
const deleteBankSoal = require('./deleteBankSoal');

module.exports = {
    getBankSoal,
    getDetailBankSoal,
    getCategory,
    addBankSoal,
    editBankSoal,
    deleteBankSoal,
};