const apiAdapter = require('../../apiAdapter');
const {
  PENUGASAN_APPS_SERVICE_HOST,
  PENUGASAN_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(PENUGASAN_APPS_SERVICE_HOST, PENUGASAN_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });

    const id = req.params.id;
    
    const materi = await api.post(`/personal/${id}/upload-tugas`, req.body);
    const data = materi.data.data;

    return res.json({
      status: 'success',
      data: data
    });

  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}