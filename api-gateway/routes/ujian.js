
const express = require('express');
const router = express.Router();

const ujianHandler = require('./handler/ujian');
const verifyToken = require('../middlewares/verifyToken');

router.get('/', verifyToken, ujianHandler.getUjian);
router.get('/show/:id', verifyToken, ujianHandler.getDetailUjian);
router.get('/mulai/:slug', verifyToken, ujianHandler.mulaiUjian);
router.get('/review/:id', verifyToken, ujianHandler.reviewUjian);

router.post('/answer', verifyToken, ujianHandler.answerUjian);
router.post('/', verifyToken, ujianHandler.addUjian);
router.put('/:id', verifyToken, ujianHandler.editUjian);
router.delete('/:id', verifyToken, ujianHandler.deleteUjian);

module.exports = router;
