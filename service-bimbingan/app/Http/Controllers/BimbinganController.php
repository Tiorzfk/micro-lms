<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Carbon\Carbon;
use Validator;
use DB;
use App\Models\AssignmentPersonalMentor;
use App\Models\AssignmentSubmissionGuidance;
use App\Models\AssignmentGuidance;
use Illuminate\Support\Facades\Log as LogSystem;
use App\Models\User;

class BimbinganController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');
            
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://localhost:8092/api/'.$request->header('auth').'/my-roles');
            $role = json_decode($response->getBody())->data;

            $sortBy = $request->input('sortBy');

            $query = AssignmentPersonalMentor::select('assignment_personal_mentor.id', 'ap.description as name', 'mentor.name as mentor_name',
                        'dep.name as dep_name', 'dep.color as dep_color', 'asg.status', 'asg.type as tipe', 'asg.start_time', 'asg.end_time',
                        'asg.status')
                        ->join('micro_penugasan.assignment_personal as ap', 'ap.id', 'assignment_personal_mentor.assignment_id')
                        ->join('seskoad_admin.kurikulum_rpp as sbs', 'sbs.id', 'ap.sbs_id')
                        ->join('seskoad_admin.departement as dep', 'sbs.kode_departemen', 'dep.kode')
                        ->join('seskoad_admin.users as mentor', 'mentor.id', 'assignment_personal_mentor.mentor_id')
                        ->leftJoin('assignment_submission_guidance as asg', function($leftJoin) {
                            $leftJoin->on('asg.personal_mentor_id', 'assignment_personal_mentor.id')
                                        ->where('asg.id', AssignmentSubmissionGuidance::max('id'));
                        })
                        ->leftJoin('assignment_guidance as ag', function($leftJoin) {
                            $leftJoin->on('asg.id', 'ag.submission_id')
                                        ->where('ag.id', AssignmentGuidance::max('id'));
                        });

            if(in_array('dosen', $role))
            {
                $query->where('mentor_id', $request->header('auth'));
            }else if(in_array('pasis', $role)){
                $query->where('user_id', $request->header('auth'));
            }

            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                      $query->where('ap.description', 'like', '%' . $searchValue . '%');
                      $query->orWhere('mentor.name', 'like', '%' . $searchValue . '%');
                });
            }

            if($sortBy)
            {
                if(strtolower($sortBy) == 'terbaru')
                {
                    $query->orderBy('id', 'DESC');

                }else if(strtolower($sortBy) == 'terlama')
                {
                    $query->orderBy('id', 'ASC');
                }else if(strtolower($sortBy) == '7 hari sebelumnya')
                {
                    $last7Day = Carbon::parse(Carbon::now()->subDays(7)->format('Y-m-d'));
                    $query->where(DB::raw('assignment_submission_guidance.created_at', '%Y-%m-%d'), '>=', $last7Day);
                }else if(strtolower($sortBy) == '30 hari sebelumnya')
                {
                    $last30Day = Carbon::parse(Carbon::now()->subDays(30)->format('Y-m-d'));
                    $query->where(DB::raw('assignment_submission_guidance.created_at', '%Y-%m-%d'), '>=', $last30Day);
                }else{
                    $query->orderBy('id', 'DESC');
                }
            }else{
                $query->orderBy('id', 'DESC');
            }

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) {
                $q->tempat = '';

                $jml = AssignmentSubmissionGuidance::where('personal_mentor_id', $q->id)->where('status', 'approved')->get()->count();
                $q->jumlah_bimbingan = $jml > 0 ? 'N-'.$jml : '-';
                $q->waktu = '';
                $q->status = $q->status == null ? '' : $q->status;
                return $q;
            });

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[BimbinganController index] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function setPembimbing(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'dosen.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'dosen' => 'Dosen',
            ]; 

            $validator = Validator::make($input, [
                'dosen'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $cek = AssignmentPersonalMentor::where('assignment_id', $id)
                        ->where('user_id', $request->header('auth'))
                        ->first();

            $mentor = AssignmentPersonalMentor::where('assignment_id', $id)
                        ->where('user_id', $request->header('auth'))
                        ->first();

            if(!$mentor) {
                $mentor = new AssignmentPersonalMentor;
                $mentor->assignment_id = $id;
                $mentor->user_id = $input['user_id'];
            }

            $mentor->mentor_id = $input['dosen']['id'];
            $mentor->save();

            if($cek)
            {
                $msg = trans('validation.updated');
                insert_log([
                    'model' => $cek,
                    'action' => 'modified',
                    'parameter' => $mentor,
                    'data' => 'Pembimbing '.User::find($cek->mentor_id)->name.' menjadi '.User::find($mentor->mentor_id)->name
                ]);
            }else{
                $msg = trans('validation.created');
                insert_log([
                    'model' => $mentor,
                    'action' => 'created',
                    'data' => 'Pembimbing '.User::find($input['dosen']['id'])->name.' => Pasis '.User::find($mentor->user_id)->name
                ]);
            }

            return $this->sendResponse(200, $msg, $mentor);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment setPembimbing] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function ajukanBimbingan(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'subject.required'=>trans('validation.required'),
                'date.required'=>trans('validation.required'),
                'start_time.required'=>trans('validation.required'),
                'end_time.required'=>trans('validation.required'),
                'type.required'=>trans('validation.required'),
                'link.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'dosen' => 'Dosen',
                'subject' => 'Subject',
                'date' => 'Date',
                'start_time' => 'Start Time',
                'end_time' => 'End Time',
                'type' => 'Type',
                'link' => 'Link',
            ]; 

            $validator = Validator::make($input, [
                'subject'=> 'required', 
                'date'=> 'required', 
                'start_time'=> 'required', 
                'end_time'=> 'required', 
                'type'=> 'required', 
                'link'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $ajukan = new AssignmentSubmissionGuidance;
            $ajukan->subject = $input['subject'];
            $ajukan->date = $input['date'];
            $ajukan->start_time = $input['start_time'];
            $ajukan->end_time = $input['end_time'];
            $ajukan->type = $input['type'];
            $ajukan->link = $input['link'];
            $ajukan->notes_for_dosen = $input['notes_for_dosen'];
            $ajukan->notes_for_pasis = $input['notes_for_pasis'];
            $ajukan->personal_mentor_id = $id;
            $ajukan->status = 'pending';
            $ajukan->save();

            insert_log([
                'model' => $ajukan,
                'action' => 'created',
                'data' => 'Mengajukan bimbingan '.$input['subject']
            ]);

            return $this->sendResponse(200, trans('validation.created'), $ajukan);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment ajukanBimbingan] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function ubahStatus(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'status.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'status' => 'Status',
            ]; 

            $validator = Validator::make($input, [
                'status'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $submission = AssignmentSubmissionGuidance::find($id);
            $submission->status = $input['status'];
            $submission->save();

            return $this->sendResponse(200, trans('validation.created'), $submission);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment ubahStatus] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function uploadNaskah(Request $request, $submission_id)
    {
        try {
            $input = $request->all();
            $messages = [
                'file_naskah.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'file_naskah' => 'File Naskah',
            ]; 

            $validator = Validator::make($input, [
                'file_naskah'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $bimbingan = new AssignmentGuidance;

            $file_name_naskah = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file_naskah']->getClientOriginalName()));
            $path = public_path()."/assets/admin/ams/bimbingan";

            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            $input['file_naskah']->move($path,$file_name_naskah);
            $bimbingan->file_naskah = $file_name_naskah;

            if($request->hasFile('file_lc'))
            {
                $file_name_lc = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file_lc']->getClientOriginalName()));
                $bimbingan->file_lc = $file_name_lc;
            }

            $bimbingan->notes_for_dosen = $input['notes_for_dosen'];
            $bimbingan->notes_for_pasis = $input['notes_for_pasis'];
            $bimbingan->status = $input['status'];
            $bimbingan->submission_id = $submission_id;

            $bimbingan->save();

            return $this->sendResponse(200, trans('validation.created'), $submission);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment uploadNaskah] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function uploadRevisiNaskah(Request $request, $submission_id, $guidance_id)
    {
        try {
            $input = $request->all();
            $messages = [
                'file_revisi.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'file_revisi' => 'File Revisi',
            ]; 

            $validator = Validator::make($input, [
                'file_revisi'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $bimbingan = AssignmentGuidance::find($guidance_id);

            $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file_revisi']->getClientOriginalName()));
            $path = public_path()."/assets/admin/ams/bimbingan";

            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            $input['file_revisi']->move($path,$file_name);
            $bimbingan->file_revisi = $file_name;

            $bimbingan->notes_for_dosen = $input['notes_for_dosen'];
            $bimbingan->notes_for_pasis = $input['notes_for_pasis'];
            $bimbingan->status = $input['status'];
            $bimbingan->submission_id = $submission_id;

            $bimbingan->save();

            return $this->sendResponse(200, trans('validation.created'), $submission);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment uploadNaskah] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function deletePembimbing($id)
    {
        try {
            $asgn = AssignmentPersonalMentor::find($id);
            if(!$asgn){
                return $this->sendResponse(400, 'Data not found', (object)["name" => ["Data not found"]]);
            }
            try {
                $asgn->delete();

                insert_log([
                  'model' => $asgn,
                  'action' => 'deleted',
                  'parameter' => $asgn
                ]);
            } catch (\Throwable $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }
            return $this->sendResponse(200,trans('validation.deleted'),$asgn);
        } catch (\Throwable $e) {
            LogSystem::error('[Bimbingan destroy] : ' . $e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }
}
