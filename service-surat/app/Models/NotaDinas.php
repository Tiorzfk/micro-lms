<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_nota_dinas';
    protected $fillable = ['perihal' , 'surat' , 'status' , 'user_id', 'dicek_oleh'];

    public function user()
    {
        return $this->hasOne('App\Models\Auth\UserLite', 'id', 'user_id');
    }

    public function dicek()
    {
        return $this->hasOne('App\Models\Auth\User', 'id', 'dicek_oleh');
    }

    public function tujuan()
    {
        return $this->hasMany('Modules\DMS\Entities\TujuanNotaDinas', 'id_nota_dinas', 'id')->where('jenis','Tujuan');
    }

    public function tembusan()
    {
        return $this->hasMany('Modules\DMS\Entities\TujuanNotaDinas', 'id_nota_dinas', 'id')->where('jenis','Tembusan')->where('is_sistem', 0);
    }

    public function lampiran()
    {
        return $this->hasMany('Modules\DMS\Entities\LampiranNotaDinas', 'id_nota_dinas', 'id');
    }

    public function attachment()
    {
        return $this->hasMany('Modules\DMS\Entities\AttachmentNotaDinas', 'id_nota_dinas', 'id');
    }

    public function revisi()
    {
        return $this->hasMany('Modules\DMS\Entities\RevisiNotaDinas', 'id_nota_dinas', 'id');
    }

    public function perbaikan()
    {
        return $this->hasMany('Modules\DMS\Entities\PerbaikanNotaDinas', 'id_nota_dinas', 'id');
    }
}
