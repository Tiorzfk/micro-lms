<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Elearning\Entities\CoursesContent;

class Courses extends Model
{
    use HasFactory;

    public $label = 'Course';

    protected $connection = 'elearning';
    protected $table = 'courses';
    protected $appends = ['ns_url', 'total_contents'];

    protected $fillable = [
        'name',
        'created_by',
        'mp_id',
        'description',
        'active',
        'photo',
        'ns',
        'by_system',
        'enrol_all_student',
        'slug'
    ];

    public function getNsUrlAttribute()
    {

        if($this->ns)
        {
            $url = config('app.url').'/assets/admin/elearning/ns/'.$this->ns;
        }else{
            $url = null;
        }
        return $url;
    }

    public function getTotalContentsAttribute()
    {
        $content = CoursesContent::where('course_id', $this->id)
                                    ->where('type', '!=', 'heading')
                                    ->count();
        return $content;
    }

    public function mp()
    {
        return $this->hasOne('\Modules\Admin\Entities\KurikulumRpp','id', 'mp_id');
    }

    public function contents()
    {
        return $this->hasMany('\Modules\Elearning\Entities\CoursesContent', 'course_id', 'id')->orderBy('order', "ASC");
    }
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\CoursesFactory::new();
    }
}
