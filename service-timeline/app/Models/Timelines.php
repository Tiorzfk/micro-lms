<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Carbon;
use Auth;
use App\Models\User;
use App\Models\TimelineLikes;
use App\Models\Timelines;

class Timelines extends Model
{
    use HasFactory;
    public $label = 'Timelines';
    protected $connection = 'ams';
    protected $table = 'timelines';
    protected $fillable = ['user_id','parent_id','post','file','type','reply_id','size','created_at','updated_at'];
    protected $appends = ['datetime','likes','users','like_post','comment_post','count_comment','reply','file_url','name_file'];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\TimelinesFactory::new();
    }

    public function getFileUrlAttribute() {
        $url = config('app.url').'/assets/admin/ams/timeline/'.$this->file;
        $cekExist = public_path().'/assets/admin/ams/timeline/'.$this->file;
        $url = config('app.url').'/assets/admin/ams/timeline/'.$this->file;
        return $url;
    }
    public function getNameFileAttribute() {
        $data = explode('@',$this->file);
        if(count($data) > 1){
            $name = $data[0].$data[count($data)-1];
        }else{
            $name = $data[0].$data[count($data)-1];
        }
       
       
        return $name;
    }

    public function getDatetimeAttribute()
    {
            if(isset($this->created_at)){
                $data = (new Carbon\Carbon($this->created_at))->diffForHumans();
                return $data;
            }else{
                return '-';
            }
    }
    public function getCountCommentAttribute()
    {   
       $data  = Timelines::where('parent_id', $this->id)->count();
           return $data;
    }
    public function getReplyAttribute()
    {   
        if ($this->reply_id) {
            $data  = User::select('name','id')->find($this->reply_id);
            return $data;
        }else{
            return null;
        }
    }
    public function getCommentPostAttribute()
    { 
        return '';
            
    }
    public function getLikesAttribute()
    {
       return TimelineLikes::where('timeline_id',$this->id)->count();
    }
    public function getLikePostAttribute()
    {
        $data = TimelineLikes::where('timeline_id',$this->id)->where('user_id', Auth::user()->id)->first();
        if($data){
           return true;
        }else{
            return false;
        }
    }
    public function getUsersAttribute()
    {
       return User::select('id','name','avatar')->find($this->user_id);
    }
    
    public function comment()
    {
        return $this->hasMany('Modules\Ams\Entities\Timelines','parent_id')->orderBy('created_at', 'DESC');
    }

    public function comments()
    {
        return $this->comment()->with('comments')->orderBy('created_at', 'DESC');
    }

}
