const apiAdapter = require('../../apiAdapter');
const {
  SURAT_APPS_SERVICE_HOST,
  SURAT_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(SURAT_APPS_SERVICE_HOST, SURAT_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });

    const surat = await api.post('/nota-dinas', req.body);
    const data = surat.data.data;

    return res.json({
      status: 'success',
      data: data
    });

  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}