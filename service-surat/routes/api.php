<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'nota-dinas'], function() {
    Route::get('/' , 'NotaDinasController@index')->name('nota.dinas');
    Route::get('/lampiran/{id}' , 'NotaDinasController@lampiran')->name('nota.dinas.lampiran');
    Route::get('/jejak/{id}' , 'NotaDinasController@jejakNotaDinas')->name('nota.dinas.jejak');
    Route::get('/perbaikan/{id}' , 'NotaDinasController@revisiNotaDinas')->name('nota.dinas.perbikan');
    Route::post('/' , 'NotaDinasController@store')->name('nota.dinas.post');
    Route::get('jabatan-user', 'DmsSuratMasukController@JabatanUser')->name('jabatan.user');
    Route::post('/revisi-nota-dinas' , 'NotaDinasController@storeRevisiNotaDinas')->name('nota.dinas.post.revisi');
    Route::post('/perbaikan-nota-dinas' , 'NotaDinasController@storePerbaikanNotaDinas')->name('nota.dinas.post.perbaikan');
    Route::post('/perbaikan-nota-dinas/draft' , 'NotaDinasController@storePerbaikanNotaDinasDraft')->name('nota.dinas.post.perbaikan.draft');
    Route::post('/verifikasi' , 'NotaDinasController@storeVerifikasiSukses')->name('nota.dinas.post.verifikasi');
    Route::post('/edit/{id}' , 'NotaDinasController@editnotadinas')->name('nota.dinas.edit');
    Route::post('/delete/{id}' , 'NotaDinasController@delete')->name('nota.dinas.delete');
    Route::post('/read' , 'NotaDinasController@readFile')->name('nota.dinas.readFile');
});
