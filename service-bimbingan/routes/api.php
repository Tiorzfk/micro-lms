<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('bimbingan')->group(function() {
    Route::get('/', '\App\Http\Controllers\BimbinganController@index');
    Route::post('/{id}/set', '\App\Http\Controllers\BimbinganController@deletePembimbing');
    Route::post('/{id}/ajukan', '\App\Http\Controllers\BimbinganController@ajukanBimbingan');
    Route::put('/{id}/status', '\App\Http\Controllers\BimbinganController@ubahStatus');
    Route::post('/{submission_id}/upload-naskah', '\App\Http\Controllers\BimbinganController@uploadNaskah');
    Route::post('/{submission_id}/upload-revisi-naskah/{guidance_id}', '\App\Http\Controllers\BimbinganController@uploadRevisiNaskah');
});