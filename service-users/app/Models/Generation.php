<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Generation extends Model
{
    use HasFactory;

    public $label = 'Angkatan';
    protected $connection = 'admin';

    protected $fillable = [
        'created_by', 
        'kurikulum_skep_id', 
        'kurikulum_skep_juknis_id',
        'name',
        'open_date',
        'close_date',
        'info',
    ];
    protected $table ='generations';

    protected $appends = ['is_active'];

    protected static function newFactory()
    {
        return \Modules\Admin\Database\factories\GenerationFactory::new();
    }

    public function getIsActiveAttribute()
    {
        if(\Carbon\Carbon::parse($this->open_date) <= \Carbon\Carbon::parse(\Carbon\Carbon::now()->format('Y-m-d')) && 
        \Carbon\Carbon::parse($this->close_date) >= \Carbon\Carbon::parse(\Carbon\Carbon::now()->format('Y-m-d'))){
            return 1;
        }else{
            return 0;
        }

    }
}
