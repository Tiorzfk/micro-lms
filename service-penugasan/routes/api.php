<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('groups')->group(function() {
    Route::get('/{id}', 'AssignmentGroupsController@index');
    Route::get('/{id}/show', 'AssignmentGroupsController@show');
    Route::post('/', 'AssignmentGroupsController@store');
    Route::post('/{id}/upload-tugas', 'AssignmentGroupsController@uploadTugas');
    Route::delete('/{id}/upload-tugas', 'AssignmentGroupsController@deleteTugas');
    Route::delete('/{id}', 'AssignmentGroupsController@destroy');
});

Route::prefix('personal')->group(function() {
    Route::get('/', 'AssignmentPersonalController@index');
    Route::get('/{id}', 'AssignmentPersonalController@show');
    Route::post('/', 'AssignmentPersonalController@store');
    Route::post('/{id}/pembimbing', 'AssignmentPersonalController@setPembimbing');
    Route::post('/{id}/upload-tugas', 'AssignmentPersonalController@uploadTugas');
    Route::delete('/{id}/upload-tugas', 'AssignmentPersonalController@deleteTugas');
    Route::put('/{id}', 'AssignmentPersonalController@update');
    Route::delete('/{id}', 'AssignmentPersonalController@destroy');
    Route::get('/{id}/pasis-pengumpulan-tugas', 'AssignmentPersonalController@listTugasPasis');
});