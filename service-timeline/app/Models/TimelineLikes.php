<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimelineLikes extends Model
{
    use HasFactory;
    protected $connection = 'ams';
    protected $table = 'timeline_likes';
    protected $fillable = ['user_id','timeline_id'];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\TimelineLikesFactory::new();
    }
}
