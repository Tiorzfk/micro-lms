<?php

namespace Modules\Ams\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentPersonal extends Model
{
    use HasFactory;

    public $label = 'Assignment Personal';

    protected $connection = 'ams';
    protected $table = 'assignment_personal';
    protected $fillable = [
        'created_by',
        'sbs_id',
        'description',
        'file',
        'deadline',
        'is_guidance',
        'generation_id',
        'stages_id'
    ];
    protected $appends = ['file_url'];

    public function getFileUrlAttribute() {
        if($this->file){
            $url = config('app.url').'/assets/admin/ams/personal/'.$this->file;

            if(!file_exists(public_path().'/assets/admin/ams/personal/'.$this->file))
            {
                return null;
            }
            
            return $url;
        }else{
            return null;
        }
    }
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentPersonalFactory::new();
    }
}
