const addSurat = require('./addSurat');
const getSurat = require('./getSurat');

module.exports = {
    getSurat,
    addSurat,
};