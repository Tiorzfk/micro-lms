const addTimeline = require('./addTimeline');
const getTimeline = require('./getTimeline');

module.exports = {
    getTimeline,
    addTimeline,
};