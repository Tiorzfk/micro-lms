<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContentUserProgress extends Model
{
    use HasFactory;

    public $label = 'Course Progress';
    protected $connection = 'elearning';
    protected $table = 'content_user_progress';

    protected $fillable = [
        'content_id',
        'user_id',
        'status'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\ContentUserProgressFactory::new();
    }
}
