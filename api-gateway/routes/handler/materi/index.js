const getCourse = require('./getCourse');
const getMateri = require('./getMateri');
const addMateri = require('./addMateri');
const editMateri = require('./editMateri');
const deleteMateri = require('./deleteMateri');

module.exports = {
    getCourse,
    addMateri,
    editMateri,
    deleteMateri,
    getMateri,
};