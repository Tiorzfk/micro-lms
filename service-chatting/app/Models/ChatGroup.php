<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ChatGroup extends Model
{
    use HasFactory;

    public $label = 'Chat Group';

    protected $connection = 'ams';
    protected $table = 'chat_group';
    protected $fillable = [
        'user_id_sender',
        'group_id',
        'message',
        'file'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\ChatGroupFactory::new();
    }
}
