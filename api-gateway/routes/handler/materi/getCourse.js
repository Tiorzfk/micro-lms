const apiAdapter = require('../../apiAdapter');
const {
  MATERI_APPS_SERVICE_HOST,
  MATERI_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(MATERI_APPS_SERVICE_HOST, MATERI_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    const course = await api.get(`/courses`);
    return res.json(course.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}