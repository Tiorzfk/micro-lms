<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Admin\Entities\DosenMapel;
use Modules\Elearning\Entities\Questions;

class KurikulumRpp extends Model
{
    use HasFactory;

    public $label = 'Kurikulum RPP';
    protected $table ='kurikulum_rpp';
    protected $fillable = [
        'kurikulum_skep_id',
        'parent_id',
        'kode_departemen',
        'created_by',
        'stage_id',
        'name',
        'kode',
        'keterangan',
    ];
    protected $appends = ['dosen','dosen_cadangan'];
    
    protected static function newFactory()
    {
        return \Modules\Admin\Database\factories\KurikulumRppFactory::new();
    }

    public function departement()
    {
        return $this->hasOne('\Modules\Admin\Entities\Departement','kode', 'kode_departemen');
    }

    public function child()
    {
        return $this->hasMany('\Modules\Admin\Entities\KurikulumRpp','parent_id');
    }

    public function childs()
    {
        return $this->child()->with('childs');
    }

    public function getDosenAttribute()
    {
        $dosen = DosenMapel::with('dosen')
                    ->where('generation_id', get_angkatan_aktif()->id)
                    ->where('kurikulum_rpp_id', $this->id)
                    ->where('type', 'main')
                    ->first();
        return $dosen ? $dosen->dosen->name : '-';
    }

    public function getDosenCadanganAttribute()
    {
        $dosen = DosenMapel::with('dosen')
                    ->where('generation_id', get_angkatan_aktif()->id)
                    ->where('kurikulum_rpp_id', $this->id)
                    ->where('type', 'sub')
                    ->get();
        return $dosen->pluck('dosen.name');
    }

    public function getActionAttribute()
    {
        if(count($this->child()->get()) > 0)
        {
            return '<div class="align-items-center justify-content-start" style="display:flex">
                        <button class="btn btn-sm btn-success m-0 me-2" @click="addData()"><img :src="`/img/icon/addBorder.svg`"/></button>
                        <button class="btn btn-sm btn-warning m-0 me-2"><img :src="`/img/icon/edit.svg`"/></button>
                        <button class="btn btn-sm btn-danger m-0 me-2"><img :src="`/img/icon/delete.svg`"/></button>
                    </div>';
        }
    }
}
