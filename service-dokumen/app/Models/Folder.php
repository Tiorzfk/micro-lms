<?php

namespace Modules\DMS\Entities;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $table = 'dms_folder';

    protected $fillable = ['id_user', 'nama', 'deskripsi', 'type'];
    
    public function has_role()
    {
        return $this->hasMany('\Modules\DMS\Entities\FolderHasRole', 'id_folder', 'id')->limit(30);
    }

    public function user()
    {
        return $this->hasOne('\Modules\DMS\Entities\User', 'id', 'id_user');
    }
}
