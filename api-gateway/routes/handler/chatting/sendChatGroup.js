const apiAdapter = require('../../apiAdapter');
const {
  CHATTING_APPS_SERVICE_HOST,
  CHATTING_APPS_SERVICE_PORT,
} = process.env;

const api = apiAdapter(CHATTING_APPS_SERVICE_HOST, CHATTING_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    const personal = await api.post('/', req.body);
    const data = personal.data.data;

    return res.json({
      status: 'success',
      data: data
    });

  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}