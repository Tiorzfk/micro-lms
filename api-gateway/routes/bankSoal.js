
const express = require('express');
const router = express.Router();

const bankSoalHandler = require('./handler/bank-soal');
const verifyToken = require('../middlewares/verifyToken');

router.post('/category', bankSoalHandler.getCategory);
router.get('/', verifyToken, bankSoalHandler.getBankSoal);

router.get('/show/:id', verifyToken, bankSoalHandler.getDetailBankSoal);
router.post('/', verifyToken, bankSoalHandler.addBankSoal);
router.put('/:id', verifyToken, bankSoalHandler.editBankSoal);
router.delete('/:id', verifyToken, bankSoalHandler.deleteBankSoal);

module.exports = router;
