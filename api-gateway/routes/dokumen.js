
const express = require('express');
const router = express.Router();

const dokumenHandler = require('./handler/dokumen');
const verifyToken = require('../middlewares/verifyToken');

router.post('/file', dokumenHandler.addFile);
router.post('/folder', dokumenHandler.addFolder);
router.get('/file', verifyToken, dokumenHandler.getFile);
router.get('/folder', verifyToken, dokumenHandler.getFolder);

module.exports = router;
