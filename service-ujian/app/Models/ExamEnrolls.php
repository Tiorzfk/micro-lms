<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamEnrolls extends Model
{
    use HasFactory;
    protected $connection = 'elearning';
    protected $table = 'exam_enrolls';
    protected $fillable = ['exam_id','user_id','istrue','status'];
    
    public function exam()
    {
        return $this->hasOne('\App\Models\Exams','id', 'exam_id');
    }

    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\ExamEnrollsFactory::new();
    }
}
