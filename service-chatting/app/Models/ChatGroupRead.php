<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ChatGroupRead extends Model
{
    use HasFactory;

    public $label = 'Chat Group';

    protected $connection = 'ams';
    protected $table = 'chat_group';
    protected $fillable = [
        'group_id',
        'user_id',
        'status'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\ChatGroupReadFactory::new();
    }
}
