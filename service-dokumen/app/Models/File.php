<?php

namespace Modules\DMS\Entities;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    
    protected $table = 'dms_file';

    // protected $connection = 'alins_'\Config::get('database.default');

    protected $fillable = ['id_user', 'id_folder', 'nama', 'deskripsi', 'type', 'size'];

    public function folder()
    {
        return $this->hasOne('\Modules\DMS\Entities\Folder', 'id', 'id_folder');
    }
    
    public function user()
    {
        return $this->hasOne('\Modules\DMS\Entities\User', 'id', 'id_user');
    }

    public function has_role()
    {
        return $this->hasMany('\Modules\DMS\Entities\FileHasRole', 'id_file', 'id');
    }
}
