<?php

namespace Modules\DMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\API\BaseController as BaseController;

use Modules\DMS\Entities\Folder;
use Modules\DMS\Entities\FolderPasis;
use Modules\DMS\Entities\File;
use Modules\DMS\Entities\FilePasis;
use Modules\DMS\Entities\FileHasRole;
use Modules\DMS\Entities\FolderHasRole;
use Validator;
use App\Models\Auth\User;
use App\Models\Akademik\Siswa;
use App\Models\Akademik\SiswaDataKelompok;
use App\Models\Akademik\SiswaKelompok;

class DmsFolderController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $columns = ['id', 'id_user', 'nama', 'deskripsi'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');
        $query = Folder::where('type', 'private')->orderBy('id', 'DESC');

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('id', 'like', '%' . $searchValue . '%')
                ->orWhere('id_user', 'like', '%' . $searchValue . '%')
                ->orWhere('nama', 'like', '%' . $searchValue . '%')
                ->orWhere('deskripsi', 'like', '%' . $searchValue . '%');
            });
        }

        $projects = $query->paginate($length);

        return $this->sendResponse(['data' => $projects, 'draw' => $request->input('draw')], 'Data folder berhasil diambil');
    }

    public function folderSaya(Request $request, $id, $type) {
        $columns = ['id', 'id_user', 'nama', 'deskripsi'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');
        $user = User::find($id);
        if($type == 'private' || $type == 'public'){
            $role = User::with('roles')->find($id);
            if($role) {
                $query = Folder::with('has_role')
                    ->where('type', $type)
                    ->where('id_user', $id)
                    ->orderBy($columns[$column], $dir);
            }else{
                $query = Folder::with('has_role')
                ->where('type', $type)
                ->where('id_user', $id)
                ->orderBy($columns[$column], $dir);
            }
        }else{
            if($user->roles[0]->name == 'pasis') {
                $query = FolderPasis::orderBy($columns[$column], $dir);
            }else{
                $query = Folder::with('has_role')->where('type', 'private')->orderBy($columns[$column], $dir);
            }
        }

        if ($searchValue) {
            $query = $query->where(function($query) use ($searchValue) {
                $query->where('id', 'like', '%' . $searchValue . '%')
                ->orWhere('id_user', 'like', '%' . $searchValue . '%')
                ->orWhere('nama', 'like', '%' . $searchValue . '%')
                ->orWhere('deskripsi', 'like', '%' . $searchValue . '%');
            });
        }

        if($type == 'public') {
            $role = User::with('roles')->find($id);
            if($role) {
                $query->orwhereHas('has_role', function ($query) use ($id) {
                    $query->where('id_role', $id);
                });
                
                if ($searchValue) {
                    $query = $query->where(function($query) use ($searchValue) {
                        $query->where('id', 'like', '%' . $searchValue . '%')
                        ->orWhere('id_user', 'like', '%' . $searchValue . '%')
                        ->orWhere('nama', 'like', '%' . $searchValue . '%')
                        ->orWhere('deskripsi', 'like', '%' . $searchValue . '%');
                    });
                }
            }
        }

        $projects = $query->paginate($length);
        
        $kelompok = SiswaKelompok::select('siswa_kelompok.id', 'siswa_kelompok_id', 'sdk.nama')
                    ->join('siswa_data_kelompok as sdk', 'sdk.id', 'siswa_kelompok.siswa_kelompok_id')
                    ->where('sekolah_angkatan_id', angkatan_aktif()['angkatan_id'])
                    ->where('tahap', strtolower(angkatan_aktif()['nama']) == 'seskoau' ? bulan_tahap() : 1)
                    ->get()
                    ->unique('siswa_kelompok_id')
                    ->values();

        return $this->sendResponse([
            'data' => $projects, 
            'draw' => $request->input('draw'), 
            'angkatan_aktif' => angkatan_aktif(),
            'kelompok_pasis' => $kelompok ,
            'tahap_aktif' => bulan_tahap()
        ], 'Data folder saya berhasil diambil');
    }

    public function setPermission(Request $request) {
        $pasisMancanegara = $request->input('pasisMancanegara');
        $user = User::find($request->id_user);

        if($user->roles[0]->name == 'bintara') {
            $siswamn = Siswa::where('matra', 'MN')->get()->pluck('nrp');

            $idUserPasisAktif = collect(User::pasis())->pluck('id')->toArray();
            $dataUser = User::with('roles')->whereHas('roles', function($query) {
                $query->whereNotIn('name', ['pasis', 'caraka']);
            })
            ->orWhereIn('id', $idUserPasisAktif); 

            if($pasisMancanegara) {
                $dataUser = $dataUser->whereNotIn('nrp', $siswamn)->get()->pluck('id');
            }else{
                $dataUser = $dataUser->get()->pluck('id');
            }

            foreach($dataUser as $id) {
                $hasRole = FolderHasRole::where('id_folder', $request->id_folder)->where('id_role', $id)->first();

                if(!$hasRole) {
                    $permission = new FolderHasRole;
                    $permission->id_role = $id;
                    $permission->id_folder = $request->id_folder;
                    try{
                        $permission->save();
                    }catch(\Exception $e){
                        return $this->sendError($e, 'Folder gagal dibagikan');
                    };
                    
                    $file = File::where('id_folder', $request->id_folder)->get();
                    foreach($file as $value) {
                        $permissionFile = new FileHasRole;
                        $permissionFile->id_role = $id;
                        $permissionFile->id_file = $value['id'];
                        try{
                            $permissionFile->save();
                        }catch(\Exception $e){
                            return $this->sendError($e, 'File gagal dibagikan');
                        };
                    }        
                }            
            }
        }else{
            $lainnya = $request->lainnya;
            
            //yang melakukan share folder
            $hasRoleUser = FolderHasRole::where('id_folder', $request->id_folder)->where('id_role', $request->id_user)->first();
            if(!$hasRoleUser) {
                $permission = new FolderHasRole;
                $permission->id_role = $request->id_user;
                $permission->id_folder = $request->id_folder;
                try{
                    $permission->save();
                }catch(\Exception $e){
                    return $this->sendError($e, 'Folder gagal dibagikan');
                };       
            }
            $file = File::where('id_folder', $request->id_folder)->get();
            foreach($file as $value) {
                $cekPermissionFile = FileHasRole::where('id_file', $value['id'])->where('id_role', $request->id_user)->first();
                if(!$cekPermissionFile) {
                    $permissionFile = new FileHasRole;
                    $permissionFile->id_role = $request->id_user;
                    $permissionFile->id_file = $value['id'];
                    try{
                        $permissionFile->save();
                    }catch(\Exception $e){
                        return $this->sendError($e, 'File gagal dibagikan');
                    };
                }
            } 

            $request->id_role = collect($request->id_role)->pluck('id');

            if($lainnya['dosen']) {
                $dosen_id = collect(User::dosen())->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($dosen_id);
            }

            if($lainnya['patun']) {
                $patun_id = collect(User::patun())->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($patun_id);
                
            }

            if($lainnya['seluruh_pasis']) {
                $pasis_id = collect(User::pasis())->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($pasis_id);
            }else if($lainnya['kelompok_pasis'] && $lainnya['id_kelompok_pasis']) {
                $pasis_kelompok = SiswaKelompok::with('DataSiswa')
                    ->select('siswa_kelompok.id', 'siswa_id', 'siswa_kelompok_id', 'sdk.nama')
                    ->join('siswa_data_kelompok as sdk', 'sdk.id', 'siswa_kelompok.siswa_kelompok_id')
                    ->where('sekolah_angkatan_id', angkatan_aktif()['angkatan_id'])
                    ->where('tahap', strtolower(angkatan_aktif()['nama']) == 'seskoau' ? bulan_tahap() : 1)
                    ->where('siswa_kelompok_id', $lainnya['id_kelompok_pasis'])
                    ->get()
                    ->pluck('DataSiswa.nrp')
                    ->toArray();

                $kelompok_pasis = user::whereIn('nrp', $pasis_kelompok)->get()->pluck('id')->toArray();
                $request->id_role = $request->id_role->merge($kelompok_pasis);
            }

            $request->id_role = $request->id_role->unique();

            foreach($request->id_role as $role_id) {
                $hasRole = FolderHasRole::where('id_folder', $request->id_folder)->where('id_role', $role_id)->first();

                if(!$hasRole) {
                    $permission = new FolderHasRole;
                    $permission->id_role = $role_id;
                    $permission->id_folder = $request->id_folder;
                    try{
                        $permission->save();
                    }catch(\Exception $e){
                        return $this->sendError($e, 'Folder gagal dibagikan');
                    };                        
                }
                $file = File::where('id_folder', $request->id_folder)->get();
                foreach($file as $value) {
                    $cekPermissionFile = FileHasRole::where('id_file', $value['id'])->where('id_role', $role_id)->first();
                    if(!$cekPermissionFile) {
                        $permissionFile = new FileHasRole;
                        $permissionFile->id_role = $role_id;
                        $permissionFile->id_file = $value['id'];
                        try{
                            $permissionFile->save();
                        }catch(\Exception $e){
                            return $this->sendError($e, 'File gagal dibagikan');
                        };
                    }
                }  
            }
        }

        return $this->sendResponse([], 'Folder berhasil dibagikan');
    }

    public function deleteSetPermission(Request $request) {
        $permission = FolderHasRole::find($request->id);
        try{
            $permission->delete();
        }catch(\Exception $e){
            return $this->sendError($e, 'Role gagal dihapus');
        };

        return $this->sendResponse($permission, 'Role berhasil dihapus');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dms::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'nama.unique'    => 'Nama Folder sudah terpakai.',
            'nama.required'    => 'Nama Folder harus diisi.'
        ];
        $validator = Validator::make($input, [
            'id_user' => 'required',
            'nama' => 'required'
        ], $messages);        

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find($request->id_user);
        // if($user->roles[0]->name == 'pasis' && $request->type == 'public') {
        //     $data = new FolderPasis;
        //     $data->id_user = $request->id_user;
        //     $data->nama = $request->nama;
        //     $data->deskripsi = $request->deskripsi ? $request->deskripsi : null;
        //     // $data->type = $request->type;
        //     $data->save();
        // }else{
            $data = new Folder;
            $data->id_user = $request->id_user;
            $data->nama = $request->nama;
            $data->deskripsi = $request->deskripsi ? $request->deskripsi : null;
            $data->type = $request->type;
            $data->save();
        // }

        return $this->sendResponse($data, 'Folder berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data = Folder::find($id);
        if(!$data)
          return $this->sendError('Folder not found.');

        return $this->sendResponse($data, 'Folder retrieved successfully.');
    }

    public function file(Request $request, $id, $id_user = null) {
        $id_user_pasis = $request->input('id_user');
        $type = $request->input('type');
        $user = User::find($id_user_pasis);

        $columns = ['id_user', 'id_folder', 'nama', 'deskripsi', 'type', 'size', 'created_at'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $page = $request->input('page') ? $request->input('page') : 1;
        $rows = $request->input('rows') ? $request->input('rows') : 3;

        if($id_user) {
            $role = User::with('roles')->find($id_user);

            $query = File::with('has_role')
                ->where('id_folder', $id)
                ->where(function ($query) use ($id_user, $role) {
                    $query->where('id_user', $id_user)
                    ->orwhereHas('has_role', function ($query) use ($id_user) {
                        $query->where('id_role', $id_user);
                    });
                })
                
                ->orderBy($columns[$column], $dir);
        }else{
            $role = User::with('roles')->find($id_user);

            $query = File::with('has_role')
                    ->where('id_folder', $id)
                    ->where(function ($query) use ($id_user_pasis, $role) {
                        $query->where('id_user', $id_user_pasis)
                        ->orwhereHas('has_role', function ($query) use ($id_user_pasis) {
                            $query->where('id_role', $id_user_pasis);
                        });
                    })
                    ->orderBy($columns[$column], $dir);
        }

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('id', 'like', '%' . $searchValue . '%')
                ->orWhere('id_user', 'like', '%' . $searchValue . '%')
                ->orWhere('nama', 'like', '%' . $searchValue . '%')
                ->orWhere('deskripsi', 'like', '%' . $searchValue . '%')
                ->orWhere('type', 'like', '%' . $searchValue . '%')
                ->orWhere('size', 'like', '%' . $searchValue . '%');
            });
        }

        // $projects = $query->limit($rows)->offset($page)->get();
        $projects = $query->paginate($length ? $length : 10);

        $kelompok = SiswaKelompok::select('siswa_kelompok.id', 'siswa_kelompok_id', 'sdk.nama')
                    ->join('siswa_data_kelompok as sdk', 'sdk.id', 'siswa_kelompok.siswa_kelompok_id')
                    ->where('sekolah_angkatan_id', angkatan_aktif()['angkatan_id'])
                    ->where('tahap', strtolower(angkatan_aktif()['nama']) == 'seskoau' ? bulan_tahap() : 1)
                    ->get()
                    ->unique('siswa_kelompok_id')
                    ->values();

        return $this->sendResponse([
            'data' => $projects, 
            'draw' => $request->input('draw'),
            'angkatan_aktif' => angkatan_aktif(),
            'kelompok_pasis' => $kelompok ,
            'tahap_aktif' => bulan_tahap()
        ], 'Data file berhasil diambil');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('dms::edit');
    }

    public function detail($id) {
        $id_user = request()->input('id_user');
        $type = request()->input('type');
        $user = User::with('roles')->select('id', 'first_name', 'last_name')->find($id_user);

        // if($user->roles[0]->name == 'pasis' && $type == 'public') {
        //     $data = FolderPasis::find($id);
        //     //calculate size file in this folder
        //     $fileSize = FilePasis::where('id_folder', $data->id)->get()->pluck('size')->sum();
        //     $data->file_size = $fileSize;
        // }else{
            if($user->roles[0]->name == 'bintara') {
                $data = Folder::with(['user' => function($query) {
                    $query->select(['id', 'first_name', 'last_name']);
                }])->find($id);
                $data->has_role = [];
            }else{
                $data = Folder::with(['has_role.role', 'user' => function($query) {
                    $query->select(['id', 'first_name', 'last_name']);
                }])->find($id);
            }

            //calculate size file in this folder
            $fileSize = File::where('id_folder', $data->id)->get()->pluck('size')->sum();
            $data->file_size = $fileSize;
        // }

        return $this->sendResponse($data, 'Detail Folder retrieved successfully.');

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'id_user' => 'required',
            'nama' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find($request->id_user);
        // if($user->roles[0]->name == 'pasis' && $input['type'] == 'public') {
        //     $data = FolderPasis::findOrFail($id);
        //     $data->nama = $request->nama;
        //     $data->deskripsi = $request->deskripsi ? $request->deskripsi : null;
        //     try{
        //         $data->save();
        //     }catch(\Exception $e){
        //         return $this->sendError('Folder gagal dupdate', $e);
        //     };
        // }else{
            $data = Folder::findOrFail($id);
            $data->nama = $request->nama;
            $data->deskripsi = $request->deskripsi ? $request->deskripsi : null;
            try{
                $data->save();
            }catch(\Exception $e){
                return $this->sendError('Folder gagal dupdate', $e);
            };
        // }

        return $this->sendResponse($data, 'Nama folder berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $id_user = request()->input('id_user');
        $type = request()->input('type');

        $user = User::find($id_user);
        // if($user->roles[0]->name == 'pasis' && $type == 'public') {
        //     $data = FolderPasis::findOrFail($id);
        //     $data->delete();
        // }else{
            $data = Folder::findOrFail($id);
            $data->delete();
        // }
        return $this->sendResponse($data, 'Folder berhasil dihapus');
    }
}
