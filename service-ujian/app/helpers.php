<?php
use Carbon\Carbon;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Log as LogSystem;

if (!function_exists('getDay')) {
    function getDay($hari)
    {
        switch($hari){
            case 'Sunday':
                $hari_ini = "Minggu";
            break;
            case 'Monday':			
                $hari_ini = "Senin";
            break;
            case 'Tuesday':
                $hari_ini = "Selasa";
            break;
            case 'Wednesday':
                $hari_ini = "Rabu";
            break;
            case 'Thursday':
                $hari_ini = "Kamis";
            break;
            case 'Friday':
                $hari_ini = "Jumat";
            break;
            case 'Saturday':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";		
            break;
        }
        return $hari_ini;
    }
}

if (!function_exists('get_date')) {
    function get_date($date, $time = false, $detik = false)
    {
        $setting = (object) ['value' => 'en'];

        if($setting->value)
        {
            $format = $setting->value;
        }else{
            $format = 'd-m-Y';
        }

        if($time)
        {
            $format .= ' H:i';

            if($detik)
            {
                $format .= ':s';
            }
        }

        if($date == 'now')
        {
            $d = Carbon::now()->format($format);
        }else{
            $d = Carbon::parse($date)->format($format);
        }

        return $d;
    }
}

if (!function_exists('auth_login')) {
    function auth_login()
    {
        $user = json_decode(session('user_login'));

        return $user;
    }
}

?>