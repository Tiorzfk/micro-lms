/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoad_ams

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 23/08/2021 18:51:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assignment_group_collects
-- ----------------------------
DROP TABLE IF EXISTS `assignment_group_collects`;
CREATE TABLE `assignment_group_collects`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_group_collects_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_group_collects_group_id_index`(`group_id`) USING BTREE,
  INDEX `assignment_group_collects_sender_id_index`(`sender_id`) USING BTREE,
  CONSTRAINT `assignment_group_collects_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_group_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_group_collects_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_group_collects
-- ----------------------------

-- ----------------------------
-- Table structure for assignment_group_users
-- ----------------------------
DROP TABLE IF EXISTS `assignment_group_users`;
CREATE TABLE `assignment_group_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `sbs_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_group_users_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_group_users_sbs_id_index`(`sbs_id`) USING BTREE,
  INDEX `assignment_group_users_group_id_index`(`group_id`) USING BTREE,
  CONSTRAINT `assignment_group_users_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_group_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_group_users_sbs_id_foreign` FOREIGN KEY (`sbs_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_group_users
-- ----------------------------

-- ----------------------------
-- Table structure for assignment_groups
-- ----------------------------
DROP TABLE IF EXISTS `assignment_groups`;
CREATE TABLE `assignment_groups`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `generation_id` bigint(20) UNSIGNED NOT NULL,
  `stages_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_groups_created_by_index`(`created_by`) USING BTREE,
  INDEX `assignment_groups_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `assignment_groups_stages_id_index`(`stages_id`) USING BTREE,
  CONSTRAINT `assignment_groups_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_groups_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `seskoad_admin`.`generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_groups_stages_id_foreign` FOREIGN KEY (`stages_id`) REFERENCES `seskoad_admin`.`stages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_groups
-- ----------------------------

-- ----------------------------
-- Table structure for assignment_guidance
-- ----------------------------
DROP TABLE IF EXISTS `assignment_guidance`;
CREATE TABLE `assignment_guidance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_naskah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_lc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `file_revisi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `notes_for_pasis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_dosen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `submission_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `status` enum('pending','revisi','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `revisi_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_guidance_submission_id_index`(`submission_id`) USING BTREE,
  CONSTRAINT `assignment_guidance_submission_id_foreign` FOREIGN KEY (`submission_id`) REFERENCES `assignment_submission_guidance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_guidance
-- ----------------------------
INSERT INTO `assignment_guidance` VALUES (1, '1629442275_c.pdf', '(rev)_1629442313_b.pdf', '1629451717_hasil-revisi.pdf', 'asd asd asd asdsa', NULL, '2021-08-19 13:12:30', '2021-08-20 16:28:37', 3, 'revisi', '2021-08-20 16:28:37');
INSERT INTO `assignment_guidance` VALUES (4, '1629452013_hasil-revisi.pdf', '1629452454_(rev)-c.pdf', '1629452367_revisibgs.pdf', NULL, NULL, '2021-08-20 16:32:07', '2021-08-20 16:40:54', 3, 'revisi', '2021-08-20 16:40:54');
INSERT INTO `assignment_guidance` VALUES (10, '1629452769_hasil-perbaikan.pdf', '1629452781_lc1.pdf', NULL, NULL, NULL, '2021-08-20 16:46:09', '2021-08-20 16:46:21', 3, 'pending', NULL);
INSERT INTO `assignment_guidance` VALUES (11, NULL, '1629453101_lc1.pdf', NULL, NULL, NULL, '2021-08-20 16:51:22', '2021-08-20 16:51:49', 4, 'pending', NULL);

-- ----------------------------
-- Table structure for assignment_personal
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal`;
CREATE TABLE `assignment_personal`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `sbs_id` bigint(20) UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` datetime(0) NOT NULL,
  `is_guidance` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `generation_id` bigint(20) UNSIGNED NOT NULL,
  `stages_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_created_by_index`(`created_by`) USING BTREE,
  INDEX `assignment_personal_sbs_id_index`(`sbs_id`) USING BTREE,
  INDEX `assignment_personal_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `assignment_personal_stages_id_index`(`stages_id`) USING BTREE,
  CONSTRAINT `assignment_personal_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `seskoad_admin`.`generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_sbs_id_foreign` FOREIGN KEY (`sbs_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_stages_id_foreign` FOREIGN KEY (`stages_id`) REFERENCES `seskoad_admin`.`stages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_personal
-- ----------------------------
INSERT INTO `assignment_personal` VALUES (6, 1, 22, 'sfdfsd fsdf sdfsd', '1628846937_MedelEtAl-GECON17.pdf', '2021-08-25 16:28:00', 1, '2021-08-13 16:28:57', '2021-08-13 16:28:57', 1, 1);

-- ----------------------------
-- Table structure for assignment_personal_collect
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal_collect`;
CREATE TABLE `assignment_personal_collect`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_collect_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_personal_collect_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `assignment_personal_collect_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_collect_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_personal_collect
-- ----------------------------
INSERT INTO `assignment_personal_collect` VALUES (1, 6, 19, 'asdasd.rar', '2021-08-16 09:50:35', NULL);
INSERT INTO `assignment_personal_collect` VALUES (2, 6, 20, 'tugas.pdf', '2021-08-16 12:09:54', NULL);

-- ----------------------------
-- Table structure for assignment_personal_mentor
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal_mentor`;
CREATE TABLE `assignment_personal_mentor`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `mentor_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_mentor_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_personal_mentor_user_id_index`(`user_id`) USING BTREE,
  INDEX `assignment_personal_mentor_mentor_id_index`(`mentor_id`) USING BTREE,
  CONSTRAINT `assignment_personal_mentor_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_mentor_mentor_id_foreign` FOREIGN KEY (`mentor_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_mentor_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_personal_mentor
-- ----------------------------
INSERT INTO `assignment_personal_mentor` VALUES (6, 6, 19, 3, '2021-08-16 16:41:01', NULL);

-- ----------------------------
-- Table structure for assignment_submission_guidance
-- ----------------------------
DROP TABLE IF EXISTS `assignment_submission_guidance`;
CREATE TABLE `assignment_submission_guidance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time(0) NOT NULL,
  `end_time` time(0) NOT NULL,
  `type` enum('online','offline') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_dosen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_pasis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` enum('pending','approved','canceled','rejected','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `personal_mentor_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_submission_guidance_personal_mentor_id_index`(`personal_mentor_id`) USING BTREE,
  CONSTRAINT `assignment_submission_guidance_personal_mentor_id_foreign` FOREIGN KEY (`personal_mentor_id`) REFERENCES `assignment_personal_mentor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assignment_submission_guidance
-- ----------------------------
INSERT INTO `assignment_submission_guidance` VALUES (3, 'pengajuan bab 1', '2021-08-18', '06:00:00', '10:00:00', 'offline', 'wm1', 'pengajuan bab 1 pengajuan bab 1', NULL, 'approved', '2021-08-18 15:40:27', '2021-08-18 16:41:25', 6);
INSERT INTO `assignment_submission_guidance` VALUES (4, 'pengajuan bab2', '2021-08-31', '07:00:00', '09:00:00', 'online', 'http://zoom.com', 'asd as das das das sdsdsd', NULL, 'approved', '2021-08-18 15:45:15', '2021-08-19 11:18:46', 6);

-- ----------------------------
-- Table structure for chat_group
-- ----------------------------
DROP TABLE IF EXISTS `chat_group`;
CREATE TABLE `chat_group`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id_sender` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_group_user_id_sender_index`(`user_id_sender`) USING BTREE,
  INDEX `chat_group_group_id_index`(`group_id`) USING BTREE,
  CONSTRAINT `chat_group_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_group_user_id_sender_foreign` FOREIGN KEY (`user_id_sender`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_group
-- ----------------------------
INSERT INTO `chat_group` VALUES (1, 18, 1, 'hai bagong', NULL, '2021-08-06 11:20:09', '2021-08-06 11:20:09');
INSERT INTO `chat_group` VALUES (2, 19, 1, 'apa ai kamu', NULL, '2021-08-06 11:27:54', '2021-08-06 11:27:54');
INSERT INTO `chat_group` VALUES (3, 19, 1, 'hohoho', NULL, '2021-08-06 11:38:12', '2021-08-06 11:38:12');
INSERT INTO `chat_group` VALUES (4, 19, 1, 'zxc', NULL, '2021-08-06 12:38:02', '2021-08-06 12:38:02');
INSERT INTO `chat_group` VALUES (5, 19, 1, 'dfgfd', NULL, '2021-08-06 12:40:54', '2021-08-06 12:40:54');
INSERT INTO `chat_group` VALUES (6, 19, 1, 'sdfsdf', NULL, '2021-08-06 12:59:11', '2021-08-06 12:59:11');
INSERT INTO `chat_group` VALUES (7, 18, 1, 'njing', NULL, '2021-08-06 12:59:18', '2021-08-06 12:59:18');
INSERT INTO `chat_group` VALUES (8, 18, 1, 'njing', NULL, '2021-08-06 12:59:26', '2021-08-06 12:59:26');
INSERT INTO `chat_group` VALUES (9, 18, 1, 'holaa', NULL, '2021-08-06 12:59:39', '2021-08-06 12:59:39');
INSERT INTO `chat_group` VALUES (10, 19, 1, 'hihi', NULL, '2021-08-06 13:02:56', '2021-08-06 13:02:56');
INSERT INTO `chat_group` VALUES (11, 19, 1, 'hai', NULL, '2021-08-06 13:21:51', '2021-08-06 13:21:51');
INSERT INTO `chat_group` VALUES (12, 19, 1, 's', NULL, '2021-08-06 13:22:19', '2021-08-06 13:22:19');
INSERT INTO `chat_group` VALUES (13, 19, 1, 'hola', NULL, '2021-08-06 13:25:52', '2021-08-06 13:25:52');
INSERT INTO `chat_group` VALUES (14, 18, 1, 'pa njing', NULL, '2021-08-06 13:26:06', '2021-08-06 13:26:06');
INSERT INTO `chat_group` VALUES (15, 19, 1, 'oy', NULL, '2021-08-06 13:28:44', '2021-08-06 13:28:44');
INSERT INTO `chat_group` VALUES (16, 18, 1, 'apa boy', NULL, '2021-08-06 13:29:07', '2021-08-06 13:29:07');
INSERT INTO `chat_group` VALUES (17, 18, 1, '🙂🙂🙂🙂', NULL, '2021-08-06 13:29:12', '2021-08-06 13:29:12');
INSERT INTO `chat_group` VALUES (18, 18, 1, 'uhuy', NULL, '2021-08-06 13:30:16', '2021-08-06 13:30:16');
INSERT INTO `chat_group` VALUES (19, 18, 1, 'sd', NULL, '2021-08-06 13:30:23', '2021-08-06 13:30:23');
INSERT INTO `chat_group` VALUES (20, 19, 1, 'ggg', NULL, '2021-08-06 13:31:07', '2021-08-06 13:31:07');
INSERT INTO `chat_group` VALUES (21, 19, 1, 'ssss', NULL, '2021-08-06 13:31:11', '2021-08-06 13:31:11');
INSERT INTO `chat_group` VALUES (22, 19, 1, 'tesss', NULL, '2021-08-06 13:49:23', '2021-08-06 13:49:23');
INSERT INTO `chat_group` VALUES (23, 18, 1, 'haii', NULL, '2021-08-09 13:31:20', '2021-08-09 13:31:20');

-- ----------------------------
-- Table structure for chat_group_read
-- ----------------------------
DROP TABLE IF EXISTS `chat_group_read`;
CREATE TABLE `chat_group_read`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('new','read') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_group_read_group_id_index`(`group_id`) USING BTREE,
  INDEX `chat_group_read_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `chat_group_read_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_group_read_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_group_read
-- ----------------------------
INSERT INTO `chat_group_read` VALUES (1, 1, 18, 'new', '2021-08-09 13:31:20', '2021-08-09 13:31:49');

-- ----------------------------
-- Table structure for chat_people
-- ----------------------------
DROP TABLE IF EXISTS `chat_people`;
CREATE TABLE `chat_people`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_id_friend` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_people_user_id_index`(`user_id`) USING BTREE,
  INDEX `chat_people_user_id_friend_index`(`user_id_friend`) USING BTREE,
  CONSTRAINT `chat_people_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_people_user_id_friend_foreign` FOREIGN KEY (`user_id_friend`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_people
-- ----------------------------
INSERT INTO `chat_people` VALUES (10, 18, 1, '2021-08-04 17:00:12', '2021-08-04 17:00:12');
INSERT INTO `chat_people` VALUES (16, 1, 18, '2021-08-05 10:53:49', '2021-08-05 10:53:49');
INSERT INTO `chat_people` VALUES (18, 27, 1, '2021-08-05 11:31:41', '2021-08-05 11:31:41');
INSERT INTO `chat_people` VALUES (19, 1, 16, '2021-08-05 11:31:55', '2021-08-05 11:31:55');
INSERT INTO `chat_people` VALUES (20, 16, 1, '2021-08-05 11:31:55', '2021-08-05 11:31:55');
INSERT INTO `chat_people` VALUES (21, 1, 22, '2021-08-05 11:58:05', '2021-08-05 11:58:05');
INSERT INTO `chat_people` VALUES (22, 22, 1, '2021-08-05 11:58:05', '2021-08-05 11:58:05');
INSERT INTO `chat_people` VALUES (23, 1, 25, '2021-08-05 11:58:19', '2021-08-05 11:58:19');
INSERT INTO `chat_people` VALUES (24, 25, 1, '2021-08-05 11:58:19', '2021-08-05 11:58:19');
INSERT INTO `chat_people` VALUES (25, 1, 28, '2021-08-05 11:58:35', '2021-08-05 11:58:35');
INSERT INTO `chat_people` VALUES (26, 28, 1, '2021-08-05 11:58:35', '2021-08-05 11:58:35');
INSERT INTO `chat_people` VALUES (27, 1, 20, '2021-08-05 13:11:21', '2021-08-05 13:11:21');
INSERT INTO `chat_people` VALUES (28, 20, 1, '2021-08-05 13:11:21', '2021-08-05 13:11:21');
INSERT INTO `chat_people` VALUES (29, 1, 26, '2021-08-05 13:12:18', '2021-08-05 13:12:18');
INSERT INTO `chat_people` VALUES (30, 26, 1, '2021-08-05 13:12:18', '2021-08-05 13:12:18');
INSERT INTO `chat_people` VALUES (31, 1, 24, '2021-08-05 13:12:23', '2021-08-05 13:12:23');
INSERT INTO `chat_people` VALUES (32, 24, 1, '2021-08-05 13:12:23', '2021-08-05 13:12:23');
INSERT INTO `chat_people` VALUES (33, 1, 19, '2021-08-05 13:12:45', '2021-08-05 13:12:45');
INSERT INTO `chat_people` VALUES (34, 19, 1, '2021-08-05 13:12:45', '2021-08-05 13:12:45');
INSERT INTO `chat_people` VALUES (35, 1, 23, '2021-08-05 13:12:49', '2021-08-05 13:12:49');
INSERT INTO `chat_people` VALUES (36, 23, 1, '2021-08-05 13:12:49', '2021-08-05 13:12:49');
INSERT INTO `chat_people` VALUES (37, 18, 19, '2021-08-06 11:29:42', '2021-08-06 11:29:42');
INSERT INTO `chat_people` VALUES (40, 19, 18, '2021-08-06 13:59:04', '2021-08-06 13:59:04');

-- ----------------------------
-- Table structure for chat_personal
-- ----------------------------
DROP TABLE IF EXISTS `chat_personal`;
CREATE TABLE `chat_personal`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id_sender` bigint(20) UNSIGNED NOT NULL,
  `user_id_receiver` bigint(20) UNSIGNED NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` enum('sent','read') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'sent',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status_read` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_personal_user_id_sender_index`(`user_id_sender`) USING BTREE,
  INDEX `chat_personal_user_id_receiver_index`(`user_id_receiver`) USING BTREE,
  CONSTRAINT `chat_personal_user_id_receiver_foreign` FOREIGN KEY (`user_id_receiver`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_personal_user_id_sender_foreign` FOREIGN KEY (`user_id_sender`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 120 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat_personal
-- ----------------------------
INSERT INTO `chat_personal` VALUES (44, 1, 18, 'oy', NULL, 'read', '2021-08-04 10:21:43', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (45, 1, 18, 'oy123', NULL, 'read', '2021-08-04 10:21:56', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (46, 1, 18, 'asd', NULL, 'read', '2021-08-05 10:25:16', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (47, 1, 18, 'hai', NULL, 'read', '2021-08-05 10:29:31', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (48, 1, 18, 'asdasd', NULL, 'read', '2021-08-05 10:50:00', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (49, 1, 18, 'dfdd', NULL, 'read', '2021-08-05 10:52:49', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (50, 1, 18, 'asdasd', NULL, 'read', '2021-08-05 10:53:08', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (51, 1, 18, '🙂', NULL, 'read', '2021-08-05 10:53:53', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (52, 1, 18, '😃', NULL, 'read', '2021-08-05 10:54:33', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (53, 1, 18, 'asdasdasd', NULL, 'read', '2021-08-05 10:54:41', '2021-08-09 14:11:17', 0);
INSERT INTO `chat_personal` VALUES (54, 1, 18, 'ddd', NULL, 'read', '2021-08-05 10:55:03', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (55, 1, 18, 'a', NULL, 'read', '2021-08-05 10:55:08', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (56, 1, 18, 'gd🙂', NULL, 'read', '2021-08-05 10:57:44', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (57, 1, 18, 'sdfds', NULL, 'read', '2021-08-05 11:20:22', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (58, 1, 27, 'hohoho', NULL, 'sent', '2021-08-05 11:31:44', '2021-08-05 11:31:44', 0);
INSERT INTO `chat_personal` VALUES (59, 1, 16, 'kamu jelek', NULL, 'sent', '2021-08-05 11:32:27', '2021-08-09 13:07:58', 0);
INSERT INTO `chat_personal` VALUES (60, 1, 22, 'heh bodoh', NULL, 'sent', '2021-08-05 11:58:09', '2021-08-09 13:07:53', 0);
INSERT INTO `chat_personal` VALUES (61, 1, 25, 'jelek jelek jelek', NULL, 'sent', '2021-08-05 11:58:23', '2021-08-09 13:07:47', 0);
INSERT INTO `chat_personal` VALUES (62, 1, 22, 'hallooo', NULL, 'sent', '2021-08-05 13:11:16', '2021-08-09 13:07:53', 0);
INSERT INTO `chat_personal` VALUES (63, 1, 20, 'dfsdfsdf', NULL, 'sent', '2021-08-05 13:11:25', '2021-08-05 13:11:25', 0);
INSERT INTO `chat_personal` VALUES (64, 1, 18, 'fghfgh', NULL, 'read', '2021-08-05 14:01:24', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (65, 1, 18, 'sad', NULL, 'read', '2021-08-05 15:30:38', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (66, 1, 18, 'asdad', NULL, 'read', '2021-08-05 15:34:14', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (67, 1, 18, 'a', NULL, 'read', '2021-08-05 15:34:24', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (68, 1, 18, 'njing', NULL, 'read', '2021-08-05 15:38:33', '2021-08-09 14:11:13', 0);
INSERT INTO `chat_personal` VALUES (69, 18, 1, 'apa njing', NULL, 'sent', '2021-08-05 15:38:41', '2021-08-09 13:21:17', 0);
INSERT INTO `chat_personal` VALUES (70, 18, 19, 'haii', NULL, 'read', '2021-08-06 11:29:44', '2021-08-09 14:14:58', 0);
INSERT INTO `chat_personal` VALUES (71, 19, 18, 'apa', NULL, 'read', '2021-08-06 11:29:55', '2021-08-09 13:43:33', 0);
INSERT INTO `chat_personal` VALUES (72, 18, 19, 'gpp', NULL, 'read', '2021-08-06 11:31:04', '2021-08-09 14:14:58', 0);
INSERT INTO `chat_personal` VALUES (73, 19, 18, 'teu baleg', NULL, 'read', '2021-08-06 11:31:23', '2021-08-09 13:43:33', 0);
INSERT INTO `chat_personal` VALUES (74, 19, 18, 'asd', NULL, 'read', '2021-08-06 11:32:21', '2021-08-09 13:43:33', 0);
INSERT INTO `chat_personal` VALUES (75, 19, 18, 'dddd', NULL, 'read', '2021-08-06 11:32:30', '2021-08-09 13:43:33', 0);
INSERT INTO `chat_personal` VALUES (76, 19, 18, 'ddd', NULL, 'read', '2021-08-06 11:32:41', '2021-08-09 13:43:33', 0);
INSERT INTO `chat_personal` VALUES (77, 18, 19, 'ssss', NULL, 'read', '2021-08-06 11:34:12', '2021-08-09 14:14:54', 0);
INSERT INTO `chat_personal` VALUES (78, 18, 19, 'heyy', NULL, 'read', '2021-08-06 13:30:05', '2021-08-09 14:14:54', 0);
INSERT INTO `chat_personal` VALUES (79, 19, 18, 'apa plok', NULL, 'read', '2021-08-06 13:30:38', '2021-08-09 13:53:11', 0);
INSERT INTO `chat_personal` VALUES (80, 19, 18, 'scccc', NULL, 'read', '2021-08-06 13:50:47', '2021-08-09 13:55:32', 0);
INSERT INTO `chat_personal` VALUES (81, 19, 18, 'ddd', NULL, 'read', '2021-08-06 13:51:04', '2021-08-09 13:55:32', 0);
INSERT INTO `chat_personal` VALUES (82, 19, 18, 'fff', NULL, 'read', '2021-08-06 13:54:56', '2021-08-09 14:00:28', 0);
INSERT INTO `chat_personal` VALUES (83, 19, 18, 'sadsd', NULL, 'read', '2021-08-06 13:57:06', '2021-08-09 14:00:28', 0);
INSERT INTO `chat_personal` VALUES (84, 18, 19, 'dfdfdf', NULL, 'read', '2021-08-06 13:57:18', '2021-08-09 14:14:54', 0);
INSERT INTO `chat_personal` VALUES (85, 19, 18, 'zzz', NULL, 'read', '2021-08-06 13:58:14', '2021-08-09 14:03:14', 0);
INSERT INTO `chat_personal` VALUES (86, 18, 19, 'gggg', NULL, 'read', '2021-08-06 13:58:19', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (87, 19, 19, 'hehe', NULL, 'sent', '2021-08-06 13:58:52', '2021-08-06 13:58:52', 0);
INSERT INTO `chat_personal` VALUES (88, 18, 19, 'tesss', NULL, 'read', '2021-08-09 13:51:05', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (89, 18, 19, 'tesss', NULL, 'read', '2021-08-09 13:53:25', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (90, 18, 19, 'hai', NULL, 'read', '2021-08-09 13:53:32', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (91, 18, 19, 'lalala', NULL, 'read', '2021-08-09 13:55:35', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (92, 18, 19, 'spopdka', NULL, 'read', '2021-08-09 13:55:47', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (93, 18, 19, 'zxczxc', NULL, 'read', '2021-08-09 14:00:33', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (94, 18, 19, 'lll', NULL, 'read', '2021-08-09 14:00:51', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (95, 18, 19, 's', NULL, 'read', '2021-08-09 14:00:56', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (96, 18, 19, 'haihai', NULL, 'read', '2021-08-09 14:03:21', '2021-08-09 14:14:50', 0);
INSERT INTO `chat_personal` VALUES (97, 18, 19, 'kmkm', NULL, 'read', '2021-08-09 14:05:45', '2021-08-09 14:30:12', 0);
INSERT INTO `chat_personal` VALUES (98, 19, 18, 'hai juga', NULL, 'read', '2021-08-09 14:06:10', '2021-08-09 14:28:53', 0);
INSERT INTO `chat_personal` VALUES (99, 18, 19, 'iauaaua', NULL, 'read', '2021-08-09 14:07:19', '2021-08-09 14:31:14', 0);
INSERT INTO `chat_personal` VALUES (100, 18, 19, 'asdas', NULL, 'read', '2021-08-09 14:08:41', '2021-08-09 14:31:14', 0);
INSERT INTO `chat_personal` VALUES (101, 19, 18, 'mmm', NULL, 'read', '2021-08-09 14:08:58', '2021-08-09 14:31:30', 0);
INSERT INTO `chat_personal` VALUES (102, 18, 19, 'asd', NULL, 'read', '2021-08-09 14:09:03', '2021-08-09 14:33:17', 0);
INSERT INTO `chat_personal` VALUES (103, 18, 19, 'haii', NULL, 'read', '2021-08-09 14:11:50', '2021-08-09 14:33:36', 0);
INSERT INTO `chat_personal` VALUES (104, 19, 18, 'haiii', NULL, 'read', '2021-08-09 14:13:24', '2021-08-09 14:35:08', 0);
INSERT INTO `chat_personal` VALUES (105, 19, 18, 'asdasd', NULL, 'read', '2021-08-09 14:13:37', '2021-08-09 14:35:08', 0);
INSERT INTO `chat_personal` VALUES (106, 18, 19, 'ksjdksdj', NULL, 'read', '2021-08-09 14:13:41', '2021-08-09 14:35:10', 0);
INSERT INTO `chat_personal` VALUES (107, 19, 18, 'oy', NULL, 'read', '2021-08-09 14:30:17', '2021-08-09 14:38:17', 0);
INSERT INTO `chat_personal` VALUES (108, 19, 18, 'haiii', NULL, 'read', '2021-08-09 14:31:18', '2021-08-09 14:38:36', 0);
INSERT INTO `chat_personal` VALUES (109, 18, 19, 'asdkasodk', NULL, 'read', '2021-08-09 14:31:34', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (110, 18, 19, 'psps', NULL, 'read', '2021-08-09 14:32:15', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (111, 18, 19, 'pppp', NULL, 'read', '2021-08-09 14:32:36', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (112, 18, 19, 'jijijijij', NULL, 'read', '2021-08-09 14:33:25', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (113, 18, 19, 'asdasd', NULL, 'read', '2021-08-09 14:33:51', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (114, 18, 19, 'baababa', NULL, 'read', '2021-08-09 14:35:18', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (115, 18, 19, 'pppp', NULL, 'read', '2021-08-09 14:35:51', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (116, 18, 19, 'apsdpsad', NULL, 'read', '2021-08-09 14:36:05', '2021-08-09 14:38:27', 0);
INSERT INTO `chat_personal` VALUES (117, 19, 18, 'ooooo', NULL, 'read', '2021-08-09 14:38:30', '2021-08-09 14:50:20', 0);
INSERT INTO `chat_personal` VALUES (118, 19, 18, 'sososo', NULL, 'read', '2021-08-09 14:38:41', '2021-08-09 14:50:20', 0);
INSERT INTO `chat_personal` VALUES (119, 18, 19, 'bangkong', NULL, 'sent', '2021-08-09 14:50:24', '2021-08-09 14:50:24', 0);

-- ----------------------------
-- Table structure for notification
-- ----------------------------
DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `recipient_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `status` enum('D','R') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'D',
  `routes_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `notification_sender_id_index`(`sender_id`) USING BTREE,
  INDEX `notification_recipient_id_index`(`recipient_id`) USING BTREE,
  CONSTRAINT `notification_recipient_id_foreign` FOREIGN KEY (`recipient_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notification_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notification
-- ----------------------------
INSERT INTO `notification` VALUES (1, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 11:36:00', '2021-08-20 11:36:00');
INSERT INTO `notification` VALUES (2, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 11:38:17', '2021-08-20 11:38:17');
INSERT INTO `notification` VALUES (3, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 13:08:23', '2021-08-20 13:08:23');
INSERT INTO `notification` VALUES (4, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=6', 'Bimbingan', 'Pasis Pasis Tes 2telah mengirim naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 13:51:15', '2021-08-20 13:51:15');
INSERT INTO `notification` VALUES (5, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 13:51:53', '2021-08-20 13:51:53');
INSERT INTO `notification` VALUES (6, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 14:48:05', '2021-08-20 14:48:05');
INSERT INTO `notification` VALUES (7, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 15:22:45', '2021-08-20 15:22:45');
INSERT INTO `notification` VALUES (8, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:28:37', '2021-08-20 16:28:37');
INSERT INTO `notification` VALUES (9, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=6', 'Bimbingan', 'Pasis Pasis Tes 2telah mengirim naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:33:33', '2021-08-20 16:33:33');
INSERT INTO `notification` VALUES (10, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=6', 'Bimbingan', 'Pasis Pasis Tes 2telah mengirim naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:33:51', '2021-08-20 16:33:51');
INSERT INTO `notification` VALUES (11, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:39:27', '2021-08-20 16:39:27');
INSERT INTO `notification` VALUES (12, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:39:58', '2021-08-20 16:39:58');
INSERT INTO `notification` VALUES (13, 1, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=3', 'Bimbingan', 'Pebimbing telah mengirim revisi naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:40:54', '2021-08-20 16:40:54');
INSERT INTO `notification` VALUES (14, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=6', 'Bimbingan', 'Pasis Pasis Tes 2telah mengirim naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:46:21', '2021-08-20 16:46:21');
INSERT INTO `notification` VALUES (15, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=6', 'Bimbingan', 'Pasis Pasis Tes 2telah mengirim naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:51:22', '2021-08-20 16:51:22');
INSERT INTO `notification` VALUES (16, 19, 3, 'D', 'AMS/Home/Tab/Bimbingan/Detail/detailBimbingan?id=6', 'Bimbingan', 'Pasis Pasis Tes 2telah mengirim naskah pada bimbingan N2 di sfdfsd fsdf sdfsd', NULL, '2021-08-20 16:51:41', '2021-08-20 16:51:41');
INSERT INTO `notification` VALUES (17, 1, 18, 'D', 'Elearning/Evaluasi', 'Evaluasi', 'Ada evaluasi baru UTS Operasi yang harus dikerjakan', NULL, '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `notification` VALUES (18, 1, 19, 'D', 'Elearning/Evaluasi', 'Evaluasi', 'Ada evaluasi baru UTS Operasi yang harus dikerjakan', NULL, '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `notification` VALUES (19, 1, 20, 'D', 'Elearning/Evaluasi', 'Evaluasi', 'Ada evaluasi baru UTS Operasi yang harus dikerjakan', NULL, '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `notification` VALUES (20, 1, 22, 'D', 'Elearning/Evaluasi', 'Evaluasi', 'Ada evaluasi baru UTS Operasi yang harus dikerjakan', NULL, '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `notification` VALUES (21, 1, 25, 'D', 'Elearning/Evaluasi', 'Evaluasi', 'Ada evaluasi baru UTS Operasi yang harus dikerjakan', NULL, '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `notification` VALUES (22, 1, 26, 'D', 'Elearning/Evaluasi', 'Evaluasi', 'Ada evaluasi baru UTS Operasi yang harus dikerjakan', NULL, '2021-08-21 04:02:15', '2021-08-21 04:02:15');

-- ----------------------------
-- Table structure for timeline_likes
-- ----------------------------
DROP TABLE IF EXISTS `timeline_likes`;
CREATE TABLE `timeline_likes`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `timeline_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `timeline_likes_user_id_index`(`user_id`) USING BTREE,
  INDEX `timeline_likes_timeline_id_index`(`timeline_id`) USING BTREE,
  CONSTRAINT `timeline_likes_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `timeline_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of timeline_likes
-- ----------------------------

-- ----------------------------
-- Table structure for timeline_readings
-- ----------------------------
DROP TABLE IF EXISTS `timeline_readings`;
CREATE TABLE `timeline_readings`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `timeline_id` bigint(20) UNSIGNED NOT NULL,
  `is_reading` bigint(20) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `timeline_readings_user_id_index`(`user_id`) USING BTREE,
  INDEX `timeline_readings_timeline_id_index`(`timeline_id`) USING BTREE,
  CONSTRAINT `timeline_readings_timeline_id_foreign` FOREIGN KEY (`timeline_id`) REFERENCES `timelines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `timeline_readings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of timeline_readings
-- ----------------------------

-- ----------------------------
-- Table structure for timelines
-- ----------------------------
DROP TABLE IF EXISTS `timelines`;
CREATE TABLE `timelines`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `post` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` enum('image','document') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `reply_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `size` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `timelines_user_id_index`(`user_id`) USING BTREE,
  INDEX `timelines_parent_id_index`(`parent_id`) USING BTREE,
  INDEX `timelines_reply_id_index`(`reply_id`) USING BTREE,
  CONSTRAINT `timelines_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `timelines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `timelines_reply_id_foreign` FOREIGN KEY (`reply_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `timelines_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of timelines
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
