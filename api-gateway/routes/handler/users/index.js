const login = require('./login');
const getUser = require('./getUser');
const logout = require('./logout');

module.exports = {
  login,
  getUser,
  logout
};