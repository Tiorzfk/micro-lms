<?php

namespace Modules\DMS\Entities;

use Illuminate\Database\Eloquent\Model;

class FolderPasis extends Model
{
    protected $table = 'dms_folder_pasis';

    protected $fillable = ['id_user', 'nama', 'deskripsi'];
}
