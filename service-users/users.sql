/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : micro_users

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 22/08/2021 15:09:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_mobile` int(11) NULL DEFAULT NULL,
  `title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of applications
-- ----------------------------

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for master_permissions
-- ----------------------------
DROP TABLE IF EXISTS `master_permissions`;
CREATE TABLE `master_permissions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `application_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `master_permission_application_id_index`(`application_id`) USING BTREE,
  CONSTRAINT `master_permissions_application_id_foreign` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of master_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2021_08_17_194542_create_permission_tables', 2);

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES (2, 'App\\Models\\User', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `permissions_name_guard_name_unique`(`name`, `guard_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `refresh_tokens`;
CREATE TABLE `refresh_tokens`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of refresh_tokens
-- ----------------------------
INSERT INTO `refresh_tokens` VALUES (1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjoxLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImVtYWlsIjoidGlvcmV6YWZrQG91dGxvb2suY29tIiwiZW1haWxfdmVyaWZpZWRfYXQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIxLTA1LTA3VDA4OjUxOjA1LjAwMDAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMS0wOC0wN1QwMDozMzowNC4wMDAwMDBaIiwiZ2VuZXJhdGlvbl9pZCI6bnVsbCwia29ycHNfaWQiOjE2LCJ1c2VybmFtZSI6ImFkbWluIiwibnJwIjoiOTk5OSIsImxhc3RfbG9naW4iOiIyMDIxLTA4LTA3VDAwOjMzOjA0Ljg3NTU3MloiLCJpc19wYXNzd29yZF9jaGFuZ2VkIjoxLCJhY3RpdmVfc3RhdHVzIjoxLCJyYW5rc19pZCI6MywiZGVsZXRlZF9hdCI6bnVsbCwibGFuZyI6ImlkIiwiZGF0ZV9mb3JtYXQiOiJkLW0tWSIsImlzbG9naW4iOjEsImF2YXRhciI6bnVsbCwicG9pbnQiOjEyNSwibGV2ZWwiOjEsImphYmF0YW4iOm51bGwsImRlcGFydGVtZW5faWQiOm51bGx9LCJpYXQiOjE2MjgyOTYzODQsImV4cCI6MTYyODM4Mjc4NH0.7H6TpUH9rJKr1VQ4wpWzdgWORGvtwRqhAhI2RVrrbm4', 1, '2021-08-07 00:33:05', '2021-08-07 00:33:05');
INSERT INTO `refresh_tokens` VALUES (2, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjoxLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImVtYWlsIjoidGlvcmV6YWZrQG91dGxvb2suY29tIiwiZW1haWxfdmVyaWZpZWRfYXQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIxLTA1LTA3VDA4OjUxOjA1LjAwMDAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMS0wOC0xN1QxMzo0OTozNy4wMDAwMDBaIiwiZ2VuZXJhdGlvbl9pZCI6bnVsbCwia29ycHNfaWQiOjE2LCJ1c2VybmFtZSI6ImFkbWluIiwibnJwIjoiOTk5OSIsImxhc3RfbG9naW4iOiIyMDIxLTA4LTE3VDEzOjQ5OjM3LjM5MjYwNFoiLCJpc19wYXNzd29yZF9jaGFuZ2VkIjoxLCJhY3RpdmVfc3RhdHVzIjoxLCJyYW5rc19pZCI6MywiZGVsZXRlZF9hdCI6bnVsbCwibGFuZyI6ImlkIiwiZGF0ZV9mb3JtYXQiOiJkLW0tWSIsImlzbG9naW4iOjEsImF2YXRhciI6bnVsbCwicG9pbnQiOjEyNSwibGV2ZWwiOjEsImphYmF0YW4iOm51bGwsImRlcGFydGVtZW5faWQiOm51bGx9LCJpYXQiOjE2MjkyMDgxNzcsImV4cCI6MTYyOTI5NDU3N30.dkEPxZ99fcYugvrPsrN3EbmBNESNo84QYfjrq9XLmdE', 1, '2021-08-17 13:49:37', '2021-08-17 13:49:37');
INSERT INTO `refresh_tokens` VALUES (3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjoxLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImVtYWlsIjoidGlvcmV6YWZrQG91dGxvb2suY29tIiwiZW1haWxfdmVyaWZpZWRfYXQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIxLTA1LTA3VDA4OjUxOjA1LjAwMDAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMS0wOC0xN1QxNDowMToxNy4wMDAwMDBaIiwiZ2VuZXJhdGlvbl9pZCI6bnVsbCwia29ycHNfaWQiOjE2LCJ1c2VybmFtZSI6ImFkbWluIiwibnJwIjoiOTk5OSIsImxhc3RfbG9naW4iOiIyMDIxLTA4LTE3VDE0OjAxOjE3Ljg5NTY0NFoiLCJpc19wYXNzd29yZF9jaGFuZ2VkIjoxLCJhY3RpdmVfc3RhdHVzIjoxLCJyYW5rc19pZCI6MywiZGVsZXRlZF9hdCI6bnVsbCwibGFuZyI6ImlkIiwiZGF0ZV9mb3JtYXQiOiJkLW0tWSIsImlzbG9naW4iOjEsImF2YXRhciI6bnVsbCwicG9pbnQiOjEyNSwibGV2ZWwiOjEsImphYmF0YW4iOm51bGwsImRlcGFydGVtZW5faWQiOm51bGx9LCJpYXQiOjE2MjkyMDg4NzcsImV4cCI6MTYyOTI5NTI3N30.qEp62m0lJuY37dPs00UwoSqd_RCJCqUos2C2kRdIWv0', 1, '2021-08-17 14:01:18', '2021-08-17 14:01:18');
INSERT INTO `refresh_tokens` VALUES (4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjoxLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImVtYWlsIjoidGlvcmV6YWZrQG91dGxvb2suY29tIiwiZW1haWxfdmVyaWZpZWRfYXQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIxLTA1LTA3VDA4OjUxOjA1LjAwMDAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMS0wOC0xN1QxNDoyMToyMi4wMDAwMDBaIiwiZ2VuZXJhdGlvbl9pZCI6bnVsbCwia29ycHNfaWQiOjE2LCJ1c2VybmFtZSI6ImFkbWluIiwibnJwIjoiOTk5OSIsImxhc3RfbG9naW4iOiIyMDIxLTA4LTE3VDE0OjIxOjIyLjM5MTk3NFoiLCJpc19wYXNzd29yZF9jaGFuZ2VkIjoxLCJhY3RpdmVfc3RhdHVzIjoxLCJyYW5rc19pZCI6MywiZGVsZXRlZF9hdCI6bnVsbCwibGFuZyI6ImlkIiwiZGF0ZV9mb3JtYXQiOiJkLW0tWSIsImlzbG9naW4iOjEsImF2YXRhciI6bnVsbCwicG9pbnQiOjEyNSwibGV2ZWwiOjEsImphYmF0YW4iOm51bGwsImRlcGFydGVtZW5faWQiOm51bGx9LCJpYXQiOjE2MjkyMTAwODIsImV4cCI6MTYyOTgxNDg4Mn0.xhdStxyRPi_Gb__97uXVPd6IoZ44FT-914iSDLyA8ls', 1, '2021-08-17 14:21:22', '2021-08-17 14:21:22');
INSERT INTO `refresh_tokens` VALUES (5, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjoxLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImVtYWlsIjoidGlvcmV6YWZrQG91dGxvb2suY29tIiwiZW1haWxfdmVyaWZpZWRfYXQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIxLTA1LTA3VDA4OjUxOjA1LjAwMDAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMS0wOC0yMFQyMDowODo1OS4wMDAwMDBaIiwiZ2VuZXJhdGlvbl9pZCI6bnVsbCwia29ycHNfaWQiOjE2LCJ1c2VybmFtZSI6ImFkbWluIiwibnJwIjoiOTk5OSIsImxhc3RfbG9naW4iOiIyMDIxLTA4LTIwVDIwOjA4OjU5Ljg4MzE1NloiLCJpc19wYXNzd29yZF9jaGFuZ2VkIjoxLCJhY3RpdmVfc3RhdHVzIjoxLCJyYW5rc19pZCI6MywiZGVsZXRlZF9hdCI6bnVsbCwibGFuZyI6ImlkIiwiZGF0ZV9mb3JtYXQiOiJkLW0tWSIsImlzbG9naW4iOjEsImF2YXRhciI6bnVsbCwicG9pbnQiOjEyNSwibGV2ZWwiOjEsImphYmF0YW4iOm51bGwsImRlcGFydGVtZW5faWQiOm51bGx9LCJpYXQiOjE2Mjk0OTAxMzksImV4cCI6MTYzMDA5NDkzOX0.p4CaXDlWYFmbpHGpUAu9K4xhT5fYg_kys5L0kIcBQI4', 1, '2021-08-20 20:09:00', '2021-08-20 20:09:00');

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_name_guard_name_unique`(`name`, `guard_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (2, 'Administrator', 'web', '2021-08-17 20:02:44', '2021-08-17 20:02:44');
INSERT INTO `roles` VALUES (3, 'pasis', 'web', '2021-08-21 04:38:10', '2021-08-21 04:38:10');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `generation_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `korps_id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nrp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `is_password_changed` tinyint(4) NOT NULL DEFAULT 0,
  `active_status` int(11) NOT NULL DEFAULT 1,
  `ranks_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `lang` enum('id','en') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'id',
  `date_format` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'd-m-Y',
  `islogin` tinyint(4) NOT NULL DEFAULT 0,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `point` bigint(20) NOT NULL DEFAULT 0,
  `level` int(11) NOT NULL DEFAULT 1,
  `jabatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `departemen_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_nrp_unique`(`nrp`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `users_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `users_korps_id_index`(`korps_id`) USING BTREE,
  INDEX `users_ranks_id_index`(`ranks_id`) USING BTREE,
  CONSTRAINT `users_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `users_korps_id_foreign` FOREIGN KEY (`korps_id`) REFERENCES `korps` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `users_ranks_id_foreign` FOREIGN KEY (`ranks_id`) REFERENCES `ranks` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Administrator', 'tiorezafk@outlook.com', NULL, '$2y$10$1X7YW9vKj2ZQlMH8oPmuieXLYiyEb5ZZtxCHQXdS7aRzBmUTSV3eW', NULL, '2021-05-07 08:51:05', '2021-08-20 20:08:59', NULL, 16, 'admin', '9999', '2021-08-20 20:08:59', 1, 1, 3, NULL, 'id', 'd-m-Y', 1, NULL, 125, 1, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
