const apiAdapter = require('../../apiAdapter');
const {
  BANK_SOAL_APPS_SERVICE_HOST,
  BANK_SOAL_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(BANK_SOAL_APPS_SERVICE_HOST, BANK_SOAL_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    const category = await api.get(`/category`);
    return res.json(category.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}