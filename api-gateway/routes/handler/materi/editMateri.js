const apiAdapter = require('../../apiAdapter');
const {
  MATERI_APPS_SERVICE_HOST,
  MATERI_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(MATERI_APPS_SERVICE_HOST, MATERI_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    const id = req.params.id;

    const materi = await api.put('/contents/', req.body);
    const data = materi.data.data;

    return res.json({
      status: 'success',
      data: data
    });

  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}