const apiAdapter = require('../../apiAdapter');
const {
  BANK_SOAL_APPS_SERVICE_HOST,
  BANK_SOAL_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(BANK_SOAL_APPS_SERVICE_HOST, BANK_SOAL_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    const question = await api.get(`/questions`);
    return res.json(question.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}