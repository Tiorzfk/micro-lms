/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : micro_ujian

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 22/08/2021 15:09:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for exam_answer
-- ----------------------------
DROP TABLE IF EXISTS `exam_answer`;
CREATE TABLE `exam_answer`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `isMarked` tinyint(4) NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_answer_exam_id_index`(`exam_id`) USING BTREE,
  INDEX `exam_answer_question_id_index`(`question_id`) USING BTREE,
  INDEX `exam_answer_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `exam_answer_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exam_answer_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `exam_questions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exam_answer_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exam_answer
-- ----------------------------

-- ----------------------------
-- Table structure for exam_enrolls
-- ----------------------------
DROP TABLE IF EXISTS `exam_enrolls`;
CREATE TABLE `exam_enrolls`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status` enum('none','progress','done','repeat','repeat-progress') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_enrolls_exam_id_index`(`exam_id`) USING BTREE,
  INDEX `exam_enrolls_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `exam_enrolls_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_enrolls_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exam_enrolls
-- ----------------------------

-- ----------------------------
-- Table structure for exam_question_answers
-- ----------------------------
DROP TABLE IF EXISTS `exam_question_answers`;
CREATE TABLE `exam_question_answers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_question_id` bigint(20) UNSIGNED NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `istrue` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_question_answers_exam_question_id_index`(`exam_question_id`) USING BTREE,
  CONSTRAINT `exam_question_answers_exam_question_id_foreign` FOREIGN KEY (`exam_question_id`) REFERENCES `exam_questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exam_question_answers
-- ----------------------------
INSERT INTO `exam_question_answers` VALUES (7, 9, 'jawaban 1 salah', '0', '2021-08-21 13:52:12', '2021-08-21 13:52:12');
INSERT INTO `exam_question_answers` VALUES (8, 9, 'jawaban 2 salah', '0', '2021-08-21 13:52:12', '2021-08-21 13:52:12');
INSERT INTO `exam_question_answers` VALUES (9, 9, 'jawaban 3 benar', '1', '2021-08-21 13:52:12', '2021-08-21 13:52:12');

-- ----------------------------
-- Table structure for exam_questions
-- ----------------------------
DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE `exam_questions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `question_name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `serial_number` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `mp_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_questions_exam_id_index`(`exam_id`) USING BTREE,
  INDEX `exam_questions_mp_id_index`(`mp_id`) USING BTREE,
  CONSTRAINT `exam_questions_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_questions_mp_id_foreign` FOREIGN KEY (`mp_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exam_questions
-- ----------------------------
INSERT INTO `exam_questions` VALUES (7, 15, '<p>ini esai baru dibuat ya</p>', 2, 1, '2021-08-21 13:52:12', '2021-08-21 13:52:12', 23);
INSERT INTO `exam_questions` VALUES (8, 15, '<p>Jelaskan sejarang SESKOAD ?</p>', 2, 2, '2021-08-21 13:52:12', '2021-08-21 13:52:12', 23);
INSERT INTO `exam_questions` VALUES (9, 15, '<p>Pilihlah jawaban yang benar !</p>', 1, 3, '2021-08-21 13:52:12', '2021-08-21 13:52:12', 23);

-- ----------------------------
-- Table structure for exams
-- ----------------------------
DROP TABLE IF EXISTS `exams`;
CREATE TABLE `exams`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `generation_id` bigint(20) UNSIGNED NOT NULL,
  `stage_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `sbs_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NULL DEFAULT NULL,
  `start_date` datetime(0) NOT NULL,
  `end_date` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('none','onprogress','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `enrol` varchar(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'semua pasis',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exams_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `exams_stage_id_index`(`stage_id`) USING BTREE,
  INDEX `exams_created_by_index`(`created_by`) USING BTREE,
  INDEX `exams_mp_id_index`(`sbs_id`) USING BTREE,
  CONSTRAINT `exams_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exams_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `seskoad_admin`.`generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exams_mp_id_foreign` FOREIGN KEY (`sbs_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exams_stage_id_foreign` FOREIGN KEY (`stage_id`) REFERENCES `seskoad_admin`.`stages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exams
-- ----------------------------
INSERT INTO `exams` VALUES (15, 1, 1, 1, 22, 'UTS Operasi', '<p>ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi </p>', NULL, '2021-08-21 00:00:00', '2021-08-21 13:52:11', '2021-08-21 13:52:11', '2021-08-21 13:52:11', 'uts-operasi-135211', 'none', 'semua pasis');

SET FOREIGN_KEY_CHECKS = 1;
