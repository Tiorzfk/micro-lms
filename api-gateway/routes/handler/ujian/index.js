const getUjian = require('./getUjian');
const getDetailUjian = require('./getDetailUjian');
const mulaiUjian = require('./mulaiUjian');
const reviewUjian = require('./reviewUjian');
const addUjian = require('./addUjian');
const editUjian = require('./editUjian');
const answerUjian = require('./answerUjian');
const deleteUjian = require('./deleteUjian');

module.exports = {
    getUjian,
    getDetailUjian,
    mulaiUjian,
    reviewUjian,
    addUjian,
    answerUjian,
    deleteUjian,
    editUjian
};