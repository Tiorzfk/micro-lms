
const express = require('express');
const router = express.Router();

const nilaiHandler = require('./handler/penilaian-dk');
const verifyToken = require('../middlewares/verifyToken');

router.post('/', nilaiHandler.getNilai);
router.get('/', verifyToken, nilaiHandler.simpanNilai);

module.exports = router;
