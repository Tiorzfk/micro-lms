<?php

namespace Modules\Ams\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentGroupCollect extends Model
{
    use HasFactory;

    public $label = 'Assignment Group Collect';

    protected $connection = 'ams';
    protected $table = 'assignment_group_collects';
    protected $fillable = [
        'assignment_id',
        'sender_id',
        'group_id',
        'file',
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentGroupCollectFactory::new();
    }

    public function group()
    {
        return $this->hasOne('\Modules\Admin\Entities\Group','id', 'group_id');
    }
}
