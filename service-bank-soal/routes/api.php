<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('questions')->group(function() {
    Route::get('/', 'App\Http\Controllers\QuestionController@index');
    Route::get('/get-question-sbs/{id}', 'App\Http\Controllers\QuestionController@getQuestionSbs');
    Route::get('/show/{id}', 'App\Http\Controllers\QuestionController@show');
    Route::post('/', 'App\Http\Controllers\QuestionController@store');
    Route::put('/{id}', 'App\Http\Controllers\QuestionController@update');
    Route::delete('/{id}', 'App\Http\Controllers\QuestionController@destroy');
});

Route::prefix('category')->group(function() {
    Route::get('/', 'App\Http\Controllers\QuestionCategoryController@index');
    Route::post('/', 'App\Http\Controllers\QuestionController@store');
    Route::put('/{id}', 'App\Http\Controllers\QuestionController@update');
    Route::delete('/{id}', 'App\Http\Controllers\QuestionController@destroy');
});