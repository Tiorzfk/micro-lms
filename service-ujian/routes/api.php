<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckUserLogin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware([CheckUserLogin::class])->prefix('exams')->group(function() {
    Route::get('/', 'App\Http\Controllers\ExamController@index');
    Route::get('/show/{id}', 'App\Http\Controllers\ExamController@show');
    Route::get('/edit/{id}', 'App\Http\Controllers\ExamController@edit');
    Route::get('/preview/{id}', 'App\Http\Controllers\ExamController@preview');
    Route::get('/mulai/{slug}', 'App\Http\Controllers\ExamController@mulai');
    Route::get('/get-sbs', 'App\Http\Controllers\SubjectsController@getSbs');
    Route::post('/', 'App\Http\Controllers\ExamController@store');
    Route::post('/answer', 'App\Http\Controllers\ExamController@storeAnswer');
    Route::post('/tandai', 'App\Http\Controllers\ExamController@storeTandai');
    Route::post('/done', 'App\Http\Controllers\ExamController@storeSelesai');
    Route::put('/{id}', 'App\Http\Controllers\ExamController@update');
    Route::put('/status-enrol/{id}', 'App\Http\Controllers\ExamController@updateStatusEnrol');
    Route::get('enrol/{id}', 'App\Http\Controllers\ExamController@listEnrol');
    Route::delete('/{id}', 'App\Http\Controllers\ExamController@destroy');
    Route::get('/time-left/{id}', 'App\Http\Controllers\ExamController@timeLeft');
    Route::get('/review/{id}', 'App\Http\Controllers\ExamController@review');
});