const axios = require('axios');

const { TIMEOUT } = process.env;

module.exports = (baseUrl, port) => {
  let instance = axios.create({
    baseURL: 'http://'+baseUrl+':'+port+'/api',
    timeout: parseInt(TIMEOUT)
  });

  return instance  
}