/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoad_ams

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 14/08/2021 21:30:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assignment_group_collects
-- ----------------------------
DROP TABLE IF EXISTS `assignment_group_collects`;
CREATE TABLE `assignment_group_collects`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_group_collects_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_group_collects_group_id_index`(`group_id`) USING BTREE,
  INDEX `assignment_group_collects_sender_id_index`(`sender_id`) USING BTREE,
  CONSTRAINT `assignment_group_collects_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_group_collects_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_group_users
-- ----------------------------
DROP TABLE IF EXISTS `assignment_group_users`;
CREATE TABLE `assignment_group_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `sbs_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_group_users_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_group_users_sbs_id_index`(`sbs_id`) USING BTREE,
  INDEX `assignment_group_users_group_id_index`(`group_id`) USING BTREE,
  CONSTRAINT `assignment_group_users_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_group_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_group_users_sbs_id_foreign` FOREIGN KEY (`sbs_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_groups
-- ----------------------------
DROP TABLE IF EXISTS `assignment_groups`;
CREATE TABLE `assignment_groups`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `generation_id` bigint(20) UNSIGNED NOT NULL,
  `stages_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_groups_created_by_index`(`created_by`) USING BTREE,
  INDEX `assignment_groups_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `assignment_groups_stages_id_index`(`stages_id`) USING BTREE,
  CONSTRAINT `assignment_groups_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_groups_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `seskoad_admin`.`generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_groups_stages_id_foreign` FOREIGN KEY (`stages_id`) REFERENCES `seskoad_admin`.`stages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_guidance
-- ----------------------------
DROP TABLE IF EXISTS `assignment_guidance`;
CREATE TABLE `assignment_guidance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_naskah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_lc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_revisi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` enum('pending','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `notes_for_pasis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_dosen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_personal
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal`;
CREATE TABLE `assignment_personal`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `sbs_id` bigint(20) UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` datetime(0) NOT NULL,
  `is_guidance` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `generation_id` bigint(20) UNSIGNED NOT NULL,
  `stages_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_created_by_index`(`created_by`) USING BTREE,
  INDEX `assignment_personal_sbs_id_index`(`sbs_id`) USING BTREE,
  INDEX `assignment_personal_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `assignment_personal_stages_id_index`(`stages_id`) USING BTREE,
  CONSTRAINT `assignment_personal_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `seskoad_admin`.`generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_sbs_id_foreign` FOREIGN KEY (`sbs_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_stages_id_foreign` FOREIGN KEY (`stages_id`) REFERENCES `seskoad_admin`.`stages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_personal_collect
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal_collect`;
CREATE TABLE `assignment_personal_collect`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_collect_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_personal_collect_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `assignment_personal_collect_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_collect_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_personal_mentor
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal_mentor`;
CREATE TABLE `assignment_personal_mentor`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `mentor_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_mentor_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_personal_mentor_user_id_index`(`user_id`) USING BTREE,
  INDEX `assignment_personal_mentor_mentor_id_index`(`mentor_id`) USING BTREE,
  CONSTRAINT `assignment_personal_mentor_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_mentor_mentor_id_foreign` FOREIGN KEY (`mentor_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_mentor_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_submission_guidance
-- ----------------------------
DROP TABLE IF EXISTS `assignment_submission_guidance`;
CREATE TABLE `assignment_submission_guidance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `guidance_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `subject` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time(0) NOT NULL,
  `end_time` time(0) NOT NULL,
  `type` enum('online','offline') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_dosen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_pasis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` enum('pending','approved','canceled','rejected','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_submission_guidance_user_id_index`(`user_id`) USING BTREE,
  INDEX `assignment_submission_guidance_guidance_id_index`(`guidance_id`) USING BTREE,
  INDEX `assignment_submission_guidance_assignment_id_index`(`assignment_id`) USING BTREE,
  CONSTRAINT `assignment_submission_guidance_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_submission_guidance_guidance_id_foreign` FOREIGN KEY (`guidance_id`) REFERENCES `assignment_guidance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_submission_guidance_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
