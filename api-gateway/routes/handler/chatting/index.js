const addFriend = require('./addFriend');
const getFriend = require('./getFriend');
const getGroup = require('./getGroup');
const getGroupConv = require('./getGroupConv');
const getPersonalConv = require('./getPersonalConv');
const sendChatGroup = require('./sendChatGroup');
const sendChatPersonal = require('./sendChatPersonal');

module.exports = {
    addFriend,
    getFriend,
    getGroup,
    getGroupConv,
    getPersonalConv,
    sendChatGroup,
    sendChatPersonal,
};