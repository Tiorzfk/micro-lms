<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerbaikanNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_perbaikan_nota_dinas';
    protected $fillable = ['id_nota_dinas' , 'perbaikan_surat' ,'id_revisi'];
}
