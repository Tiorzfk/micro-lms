<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('chat')->group(function() {
    Route::get('/friend', 'ChatController@listFriend');
    Route::get('/group', 'ChatController@listGroup');
    Route::post('/friend', 'ChatController@storeFriend');
    Route::delete('/', 'ChatController@destroy');
    Route::post('/', 'ChatController@store');
    Route::get('/', 'ChatController@index');

    Route::get('/group-conv', 'ChatController@indexGroup');
    Route::post('/group', 'ChatController@storeChatGroup');
    
    Route::get('/notif', 'ChatController@notif');
    Route::post('/read', 'ChatController@read');
});