<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use DB;
use Modules\Admin\Entities\KurikulumRpp;
use Modules\Admin\Entities\Departement;
use Modules\Elearning\Entities\Questions;
use Modules\Elearning\Entities\QuestionCategory;
use Modules\Elearning\Entities\QuestionAnswers;

class QuestionCategoryController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $searchValue =  $request->input('search');
            $query = QuestionCategory::orderBy('id');
    
            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                    $query->where('name', 'like', '%' . $searchValue . '%');
                });
             }
            $query = $query->paginate(10);
            return $this->sendResponse(200, trans('validation.success'), $query);
            } catch (\Throwable $th) {
                return $this->sendResponse(500, $this->messageError(), $th->getMessage());
            }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('elearning::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('elearning::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('elearning::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
