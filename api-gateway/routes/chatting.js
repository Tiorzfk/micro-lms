
const express = require('express');
const router = express.Router();

const chattingHandler = require('./handler/chatting');
const verifyToken = require('../middlewares/verifyToken');

router.post('/chat-group', chattingHandler.sendChatGroup);
router.post('/chat-personal', chattingHandler.sendChatPersonal);
router.get('/chat-group', verifyToken, chattingHandler.getGroupConv);
router.get('/chat-personal', verifyToken, chattingHandler.getPersonalConv);

module.exports = router;
