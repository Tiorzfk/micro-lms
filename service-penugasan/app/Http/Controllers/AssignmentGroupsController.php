<?php

namespace Modules\Ams\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Ams\Entities\AssignmentGroupUsers;
use Modules\Ams\Entities\AssignmentGroups;
use Modules\Ams\Entities\AssignmentGroupCollect;
use App\Models\User;
use Carbon\Carbon;
use Modules\Admin\Entities\Group;
use Modules\Admin\Entities\UserGroup;
use Modules\Admin\Entities\KurikulumRpp;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Illuminate\Support\Facades\Log as LogSystem;
use Validator;
use DB;
use Auth;

class AssignmentGroupsController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, $id)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');
            $stage_id = $request->input('stage_id');
            $departement_id = $request->input('departement_id');
            $role = \Auth::user()->rolesname->toArray();
            $stage_id = $request->input('stage_id');
            $sortBy = $request->input('sortBy');
            $tab = $request->input('tab');
            $now = Carbon::parse(Carbon::now()->format('Y-m-d H:i:s'));


            $query = AssignmentGroupUsers::select('assignment_group_users.id','assignment_group_users.group_id','assignment_group_users.sbs_id','assignment_group_users.created_at', 'assignment_group_users.description','assignment_group_users.assignment_id', 'assignment_group_users.file',
                        'assignment_group_users.deadline','parent.generation_id',
                        'parent.stages_id', 'dep.kode as kode_departemen','dep.id as dep_id', 'dep.name as name_departemen','parent.generation_id',
                        'dep.color as dep_color')
                        ->join('assignment_groups as parent', 'parent.id', 'assignment_group_users.assignment_id')
                        ->join('seskoad_admin.kurikulum_rpp as sbs', 'sbs.id', 'assignment_group_users.sbs_id')
                        ->join('seskoad_admin.departement as dep', 'sbs.kode_departemen', 'dep.kode')
                        ->where('parent.generation_id', get_angkatan_aktif()->id);
            
            $tugas = AssignmentGroupCollect::whereIn('assignment_id', $query->get()->pluck('id')->toArray())
                        ->pluck('assignment_id')
                        ->toArray();

            if($tab == 'penugasan')
            {
                $query->where('deadline', '>=', $now);
            }else if($tab == 'penugasan selesai')
            {
                $query->join('assignment_group_collects as apc', 'apc.assignment_id', 'assignment_group_users.id')
                        ->where('deadline', '<', $now);
            }else if($tab == 'penugasan terlewat')
            {
                $query->where('deadline', '<', $now)
                        ->whereNotIn('assignment_group_users.id', $tugas);
            }

            $status_group = false;
            if(in_array('staf departemen', $role))
            {
                $dep = Departement::find(\Auth::user()->departemen_id);
                $sbs_id = KurikulumRpp::where('kode_departemen', $dep->kode)->get()->pluck('id')->toArray();
                $query->whereIn('sbs_id', $sbs_id);
            }else if(in_array('pasis', $role))
            {   
                $getStage = get_stage_aktif();
                $group = UserGroup::select('user_id','stage_id','is_leader','group_id')
                ->where('user_id',Auth::user()->id)->where('is_patun', 0);
                if($stage_id)
                {   
                    $group = $group->where('stage_id',$getStage->id)->first();
                    if(!$group){
                        $status_group = true;
                    }
                    $query = $query->where('assignment_group_users.group_id',$group->group_id)->where('parent.stages_id', $stage_id);
                }else{
                    $group = $group->where('stage_id',$getStage->id)->first();
                    if(!$group){
                        $status_group = true;
                    }
                    if($group){
                        $query = $query->where('assignment_group_users.group_id',$group->group_id);
                    }
                }
            }else{
                $query = $query->where('assignment_group_users.group_id',$id);
                if($stage_id)
                {
                    $query->where('parent.stages_id', $stage_id);
                }
            }
            
            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                      $query->where('description', 'like', '%' . $searchValue . '%');
                      $query->orWhere('deadline', 'like', '%' . $searchValue . '%');
                });
            }

            if($departement_id){
                $query = $query->where('dep.id',$departement_id);
            }

            if($sortBy == 'Terbaru')
            {
                $query->orderBy('assignment_group_users.created_at', 'ASC');
            }elseif($sortBy == 'Terlama'){
                $query->orderBy('assignment_group_users.created_at', 'DESC');
            }elseif($sortBy == '7 Hari Sebelumnya'){
                $last7Day = Carbon::parse(Carbon::now()->subDays(7)->format('Y-m-d'));
                $query->where(DB::raw('assignment_group_users.created_at', '%Y-%m-%d'), '>=', $last7Day);
            }elseif($sortBy == '30 Hari Sebelumnya'){
                $last30Day = Carbon::parse(Carbon::now()->subDays(30)->format('Y-m-d'));
                $query->where(DB::raw('assignment_group_users.created_at', '%Y-%m-%d'), '>=', $last30Day);
            }else{
                $query->orderBy('id', 'DESC');
            }
            
            $query = $query->paginate($length ? $length : 100);
            $query->transform(function($q) {
                $q->sbs =   KurikulumRpp::select('id', 'name')->find($q->sbs_id);
                $datas = AssignmentGroups::find($q->assignment_id);
                $q->name_user =  User::find($datas->created_by)?User::find($datas->created_by)->name:'';
                return $q;
            });
            
            if(in_array('pasis', $role) && $status_group){
                $query = [];
                $query = paginate(collect($query));
            }

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment indexGroups] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('ams::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $cn = DB::connection('ams');
        try {
            $cn->beginTransaction();
            $input = $request->all();
            $messages = [
                'sbs_id.required'=>trans('validation.required'),
                'description.required'=>trans('validation.required'),
                'file.required'=>trans('validation.required'),
                'deadline.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'sbs_id' => trans('ams.sbs_id'),
                'description' => trans('ams.topic'),
                'file' => trans('ams.sheet'),
                'deadline' => trans('ams.deadline'),
            ]; 

            $validator = Validator::make($input, [
                'sbs_id'=> 'required', 
                'description'=> 'required', 
                'file'=> 'required', 
                'deadline'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            if(!$request->hasFile('file'))
            {
                return $this->sendResponse(400, 'Validation Error.', (object) ['file' => [trans('ams.sheet').' '.trans('ams.required')]]);
            }
            $parent_asgn = new AssignmentGroups;
            $parent_asgn->created_by = \Auth::user()->id;
            $parent_asgn->generation_id = get_angkatan_aktif()->id;
            $parent_asgn->stages_id = get_stage_aktif()->id;
            $parent_asgn->save();

            $getGroup =  Group::select('id','name')->has('lead')->orderBy('id')->get();
            $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
            foreach ($getGroup as $key => $value) {
               $asgn = new AssignmentGroupUsers;
               $asgn->sbs_id = $input['sbs_id'];
               $asgn->group_id = $value->id;
               $asgn->assignment_id = $parent_asgn->id;
               $asgn->description = $input['description'];
              
              
            //    dd($file_name);
            if($key == 0){
                   $path = public_path()."/assets/admin/ams/groups";
                   if (!file_exists($path)) {
                       mkdir($path, 0777, true);
                   }
                   $input['file']->move($path,$file_name);
                }
               $asgn->file = $file_name;
               $asgn->deadline = $input['deadline'];
               $asgn->save();
                try {
                    $getPasis = UserGroup::where('group_id',$value->id)->where('is_patun', 0)->where('stage_id',get_stage_aktif()->id)->pluck('user_id');
                    foreach ($getPasis as $key => $user_id) {
                        addNotif(
                        Auth::user()->id,
                        $user_id,
                        'AMS/Home/Tab/Penugasan/Detail/'.$asgn->id.'/kelompokPenugasan',
                        null,
                        'Anda mendapatkan penugasan baru bernama '.$asgn->description.'','Penugasan Kelompok'
                    );
                    }
                } catch (\Throwable $th) {
                    $cn->rollBack();
                    LogSystem::error('[Assignment storeGroups] : ' . $th);
                    return $this->sendResponse(500, $this->messageError(), $th->getMessage());
                }

               insert_log([
                    'model' => $asgn,
                    'action' => 'created',
                    'data' =>'Kelompok '. $value->name.' '.$asgn->description
                 ]);
            }           
            
            $cn->commit();
            return $this->sendResponse(200, trans('validation.created'), $asgn);
        } catch (\Throwable $th) {
            $cn->rollBack();
            LogSystem::error('[Assignment storeGroups] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $query = AssignmentGroupUsers::select('assignment_group_users.id','assignment_group_users.group_id','assignment_group_users.created_at', 'assignment_group_users.description', 'assignment_group_users.file',
        'assignment_group_users.deadline','assignment_group_users.file','parent.generation_id',
        'parent.stages_id','db2.name as name_group', 'dep.kode as kode_departemen','dep.id as dep_id', 'dep.name as name_departemen','parent.generation_id',
        'dep.color as dep_color')
        ->join('seskoad_admin.groups as db2','assignment_group_users.group_id','=','db2.id')
        ->join('assignment_groups as parent', 'parent.id', 'assignment_group_users.assignment_id')
        ->join('seskoad_admin.kurikulum_rpp as sbs', 'sbs.id', 'assignment_group_users.sbs_id')
        ->join('seskoad_admin.departement as dep', 'sbs.kode_departemen', 'dep.kode')
        ->find($id);

        $query['tugas_pasis'] = AssignmentGroupCollect::select('assignment_group_collects.id', 'assignment_group_collects.file', 
                                'assignment_group_collects.created_at', 'groups.name')
                                ->join('seskoad_admin.groups', 'groups.id', 'assignment_group_collects.group_id')
                                ->where('assignment_id', $query->id)
                                ->first();
        if($query['tugas_pasis']){
            $query['tugas_pasis']['tanggal'] = get_date(\Carbon\Carbon::parse($query['tugas_pasis']->created_at), true);
            // dd($query['tugas_pasis']);
        }
        return $this->sendResponse(200, trans('validation.created'), $query);

    }

    public function deleteTugas(Request $request, $id)
    {
        try {
            $tugas = AssignmentGroupCollect::where('assignment_id', $id)
                        ->first();

            if($tugas)
            {
                $path = public_path()."/assets/admin/ams/tugas-groups";
                //delete old file
                if(file_exists($path.'/'.$tugas->file))
                {
                    unlink($path.'/'.$tugas->file);
                }

                $tugas->delete();
                insert_log([
                    'model' => $tugas,
                    'action' => 'deleted',
                    'parameter' => $tugas
                ]);
            }

            return $this->sendResponse(200, trans('validation.deleted'), $tugas);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment uploadTugas] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('ams::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'sbs_id.required'=>trans('validation.required'),
                'description.required'=>trans('validation.required'),
                'file.required'=>trans('validation.required'),
                'deadline.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'sbs_id' => trans('ams.sbs_id'),
                'description' => trans('ams.topic'),
                'file' => trans('ams.sheet'),
                'deadline' => trans('ams.deadline'),
            ]; 

            $validator = Validator::make($input, [
                'sbs_id'=> 'required', 
                'description'=> 'required', 
                'file'=> 'required', 
                'deadline'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $asgn_old = AssignmentGroupUsers::find($id);
            $asgn = AssignmentGroupUsers::find($id);
            if($input['editKs'] == 'true'){
                $parentAsgn = AssignmentGroups::find($asgn->assignment_id);
                $childAsgn = AssignmentGroupUsers::where('assignment_id', $parentAsgn->id)->get();
                if($request->hasFile('file')) {
                    $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                    $path = public_path()."/assets/admin/ams/personal";
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $input['file']->move($path,$file_name);
    
                    //delete old file
                    if(file_exists($path.'/'.$asgn->file))
                    {
                        unlink($path.'/'.$asgn->file);
                    }
                }

                foreach ($childAsgn as $key => $value) {
                    $oldData = $value;
                    $value->sbs_id = $input['sbs_id'];
                    $value->description = $input['description'];
                    $value->deadline = $input['deadline'];
                    if($request->hasFile('file')) {
                        $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                        $value->file = $file_name;
                    }
                     $value->save();
                     insert_log([
                        'model' => $value,
                        'action' => 'modified',
                        'parameter' => $oldData
                    ]);
                    }

            }else{
                $asgn->sbs_id = $input['sbs_id'];
                $asgn->description = $input['description'];
                $asgn->deadline = $input['deadline'];
    
                if($request->hasFile('file')) {
                    $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                    $path = public_path()."/assets/admin/ams/personal";
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $input['file']->move($path,$file_name);
    
                    //delete old file
                    if(file_exists($path.'/'.$asgn->file))
                    {
                        unlink($path.'/'.$asgn->file);
                    }
                    
                    $asgn->file = $file_name;
                }
                    
                $asgn->save();
    
                insert_log([
                    'model' => $asgn,
                    'action' => 'modified',
                    'parameter' => $asgn_old
                ]);
            }
           
            return $this->sendResponse(200, trans('validation.created'), $asgn);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment storePersonal] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function uploadTugas(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'file.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'file' => trans('ams.file_tugas'),
            ]; 

            $validator = Validator::make($input, [
                'file'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            if(!$request->hasFile('file'))
            {
                return $this->sendResponse(400, 'Validation Error.', (object) ['file' => [trans('ams.file_tugas').' '.trans('ams.required')]]);
            }

            $asgn =  AssignmentGroupUsers::find($id);
            if(!$asgn) return $this->sendResponse(400, 'assignment id not found', (object) ['assignmentp' => ['assignment not found']]);

            $cekKetua = UserGroup::where('user_id', Auth::user()->id)->where('is_leader', 1)->where('group_id',$asgn->group_id)->first();
            if(!$cekKetua)return $this->sendResponse(400, trans('ams.upload_assignment'), (object) ['upload_assignment' => [trans('ams.upload_assignment')]]);
            
            $now = \Carbon\Carbon::parse(date('y-m-d H:i:s'));
            $deadline = \Carbon\Carbon::parse($asgn->deadline);
            // dd($now .' '. $deadline);
            if($now > $deadline)return $this->sendResponse(400, 'batas pengumpulan sudah habis', (object) ['upload_assignment' => ['batas pengumpulan sudah habis']]);
          

            $tugas = AssignmentGroupCollect::where('assignment_id', $id)
                        ->first();

            $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
            $path = public_path()."/assets/admin/ams/tugas-groups";
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            if($tugas)
            {
                //delete old file
                if(file_exists($path.'/'.$tugas->file))
                {
                    unlink($path.'/'.$tugas->file);
                }
            }else{
                $tugas = new AssignmentGroupCollect;
                $tugas->assignment_id = $id;
                $tugas->group_id = $asgn->group_id;
                $tugas->sender_id = Auth::user()->id;
            }
            $input['file']->move($path,$file_name);

            $tugas->file = $file_name;

            $tugas->save();

            return $this->sendResponse(200, trans('validation.created'), $tugas);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment uploadTugas] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Request $request ,$id)
    {
        try {
            $data = AssignmentGroupUsers::find($id);
            if(!$data) return $this->sendResponse(400, 'assignment id not found', (object) ['assignmentp' => ['assignment not found']]);
            if($request->isChecked){
                $parent = AssignmentGroups::find($data->assignment_id);
                $childs = AssignmentGroupUsers::where('assignment_id', $data->assignment_id)->get();
                foreach ($childs as $key => $value) {
                    if ($key == 0) {
                        $cekExist = public_path().'/assets/admin/ams/groups/'.$value->file;
                        if(file_exists($cekExist) && $data->file) {
                            unlink($cekExist);
                        }
                    }
                   $value->delete();
                   insert_log([
                    'model' => $value,
                    'action' => 'deleted',
                    'parameter' => $value
                ]);
                }
                $parent->delete();
                return $this->sendResponse(200,trans('validation.deleted'),$parent);
            }else{
                $checkParent = AssignmentGroupUsers::where('assignment_id', $data->assignment_id)->where('id' ,'!=',$id)->count();
                if($checkParent == 0){
                   $parent = AssignmentGroups::find($data->assignment_id);
                   $cekExist = public_path().'/assets/admin/ams/groups/'.$data->file;
                   if(file_exists($cekExist) && $data->file) {
                       unlink($cekExist);
                   }
                }
                $data->delete();
                insert_log([
                    'model' => $data,
                    'action' => 'deleted',
                    'parameter' => $data
                ]);
                return $this->sendResponse(200,trans('validation.deleted'),$data);
            }
        } catch (\Throwable $th) {
            dd($th);
            LogSystem::error('[Assignment deleteGroups] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }
}
