<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class QuestionCategory extends Model
{
    use HasFactory;
    protected $table = 'questions_category';
    protected $fillable = ['id','name','icon','description'];
}
