<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'penilaian'],function(){
    Route::get('/pasis','App\Http\Controllers\DKV2Controller@getPasis');
    Route::get('/preview','App\Http\Controllers\DKV2Controller@getPreview');
    Route::get('/aspek','App\Http\Controllers\DKV2Controller@getAspek');
    Route::post('/nilai','App\Http\Controllers\DKV2Controller@storeNilaiDraft');
    Route::post('/total-nilai','App\Http\Controllers\DKV2Controller@storeTotalNilaiDraft');
    Route::post('/nilai-departemen','App\Http\Controllers\DKV2Controller@storeNilaiDepartemen');
    Route::get('/rekap-nilai','App\Http\Controllers\DKV2Controller@rekapNilai');
});
