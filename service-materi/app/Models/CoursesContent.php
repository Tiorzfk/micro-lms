<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Elibrary\Entities\MasterEbook;

class CoursesContent extends Model
{
    use HasFactory;

    public $label = 'Contents';

    protected $connection = 'elearning';
    protected $table = 'courses_content';
    protected $appends = ['content_url', 'detail_ebook'];

    protected $fillable = [
        'created_by',
        'course_id',
        'name',
        'type',
        'file',
        'time',
        'complete_by',
        'order',
    ];

    public function getContentUrlAttribute()
    {
        $url = '/assets/admin/elearning/content/'.$this->type.'/'.$this->file;
        $baseUrl = config('app.url').$url;
        if($this->type == 'scorm')
        {
            $cek = public_path().$url.'\story_html5.html';
            if(file_exists($cek))
            {
                return $baseUrl.'/story_html5.html';
            }else{
                $path = public_path().$url;

                foreach(new \DirectoryIterator($path) as $dir)
                {
                    if ($dir->isDot()) continue;

                    if ($dir->isDir())
                    {
                        return $baseUrl.'/'.$dir.'/story_html5.html';
                    }
                }
            }
        }else if($this->type == 'heading')
        {
            return null;
        }else if($this->type == 'pdf' || $this->type == 'audio'){
            return config('app.url').'/assets/admin/elearning/content/'.$this->type.'/'.$this->course_id.'/'.$this->file;
        }else if($this->type == 'video'){
            if (strpos($this->file, 'http') !== false) {
                return $this->file;
            }
            return config('app.url').'/assets/admin/elearning/content/'.$this->type.'/'.$this->course_id.'/'.$this->file;
        }else if($this->type == 'ebook'){
            $file = MasterEbook::find($this->file);

            return $file ? $file->ebook_url : '';
        }else{
            return $baseUrl;
        }
    }

    public function getDetailEbookAttribute()
    {
        if($this->type == 'ebook') {
            $ebook = MasterEbook::find($this->file);
            return $ebook;
        }else{
            return null;
        }
    }

    public function course()
    {
        return $this->hasOne('\Modules\Elearning\Entities\Courses','id', 'course_id');
    }

    public function questions()
    {
        return $this->hasMany('\Modules\Elearning\Entities\CourseContentQuestion','content_id', 'id');
    }
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\CoursesContentFactory::new();
    }
}
