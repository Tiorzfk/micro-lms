<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RevisiLampiranNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_nota_dinas_revisi_lampiran';
    protected $fillable = ['id_revisi' , 'id_lampiran' , 'revisi_lampiran'];
}
