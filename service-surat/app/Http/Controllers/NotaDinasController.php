<?php

namespace Modules\DMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\API\BaseController as BaseController;

use App\Models\NotaDinas;
use App\Models\LampiranNotaDinas;
use App\Models\TujuanNotaDinas;
use App\Models\TembusanNotaDinas;
use App\Models\AttachmentNotaDinas;
use App\Models\RevisiNotaDinas;
use App\Models\PerbaikanNotaDinas;
use App\Models\RevisiLampiranNotaDinas;
use KubAT\PhpSimple\HtmlDomParser;
use Validator;
use App\Models\Notif;
use Illuminate\Support\Facades\Log;

class NotaDinasController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $columns = ['id', 'dari', 'prihal', 'nama_tujuan'];

        $length = $request->input('length');
        $dir = $request->input('dir');
        $searchValue = $request->input('search');
        $jenis = $request->jenis;

        if($jenis == 'dibuat')
        {
            $query =  NotaDinas::with(['user'  , 'dicek' ,'tujuan.user' , 'tembusan' , 'attachment'])
                    ->where('user_id' , $request->user_id)
                    ->orWhere('dicek_oleh' , $request->user_id)
                    ->orderBy('dms_nota_dinas.created_at','DESC');
        }else
        {
            $query = TujuanNotaDinas::select("*" ,'dms_tujuan_nota_dinas.user_id as user_id')
            ->join('dms_nota_dinas', 'dms_nota_dinas.id' ,'=' , 'dms_tujuan_nota_dinas.id_nota_dinas')
            ->where('dms_tujuan_nota_dinas.user_id' , $request->user_id)
            ->with(['user', 'notadinas.tujuan.user'])
            ->where('dms_nota_dinas.status' , 'terkirim')
            ->orderBy('dms_nota_dinas.created_at','DESC');
            
        }
       

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('perihal', 'like', '%' . $searchValue . '%')
                ->orWhereHas('user', function($data) use($searchValue){
                    $data->where('first_name', 'like', '%' . $searchValue . '%')
                    ->orWhere('last_name', 'like', '%' . $searchValue . '%')
                    ->orWhereRaw("concat(first_name, ' ', last_name) like '%$searchValue%' ");
                });
            });
        }

        $projects = $query->paginate($length);

        return $this->sendResponse(['data' => $projects, 'draw' => $request->input('draw')], 'Data surat keluar berhasil diambil');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dms::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
          $input = $request->all();
          $messages = [
              'tujuan.required'    => 'Tujuan belum dipilih.',
              'tujuan.array'    => 'Tujuan Yang Dikirim Harus Array.',
              'perihal.required'    => 'Perihal belum diisi.',
              'tembuasan.required'    => 'Tembusan belum dipilih.',
              'tembusan.array'    => 'Tembusan Yang Dikirim Harus Array.',
              'surat.required'    => 'File Surat belum diisi.',
              'user_id' => 'Pembuat surat tidak ditemukan, hubungi administator untuk mengecek sistem'
          ];
  
          $validator = Validator::make($input, [
              'tujuan' => 'required|array',
              'perihal' => 'required',
              'surat' => 'required',
           //   'tembusan' => 'required|array',
              'user_id' => 'required'
          ], $messages);
  
          if($validator->fails()){
              return $this->sendError('Validation Error.', $validator->errors());
          }
          
          $user = User::find($input['user_id']);
          $role = $user->getRoleNames();

          $nota_dinas = new NotaDinas;
          $nota_dinas->perihal = $input['perihal'];
          $nota_dinas->user_id = $input['user_id'];
          $nota_dinas->surat = $input['surat'];
          if($request->switchbutton == 1) {
            $nota_dinas->status = 'legalisir';
          }else{
            $nota_dinas->status = 'dicek';
          }
          $nota_dinas->type = $request->switchbutton;
          
          $tembusanSubSatker = [];

          if($request->file_surat)
          {
            $surat = \uploadbase64File($request->file_surat, '/modules/dms/nota-dinas/file/', $request->perihal);
            $nota_dinas->file_surat = $surat['filename'];
          }

          $tmbsTujuanDari = collect($input['tembusan'])->pluck('id')->merge(collect($input['tujuan'])->pluck('id'))->push($input['user_id']);

          if($role[0] == 'staf departemen')
          {
            $findKadept = User::where('departemen_id' , $user->departemen_id)->role(['kadept'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;

            $tembusanSubSatker = User::select('id', 'departemen_id')
                                  ->whereNotIn('id', $tmbsTujuanDari)
                                  ->role('staf departemen')
                                  ->where('departemen_id', $user->departemen_id)
                                  ->get()
                                  ->pluck('id');
          }
          else if($role[0] == 'staf dirdik')
          {
            $findKadept = User::role(['dirdik'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;

            $tembusanSubSatker = User::select('id', 'departemen_id')
                                  ->whereNotIn('id', $tmbsTujuanDari)
                                  ->role('staf dirdik')
                                  ->get()
                                  ->pluck('id');
          }
          else if ($role[0] == 'staf dirmin')
          {
            $findKadept = User::role(['dirmin'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;

            $tembusanSubSatker = User::select('id', 'departemen_id')
                                  ->whereNotIn('id', $tmbsTujuanDari)
                                  ->role('staf dirmin')
                                  ->get()
                                  ->pluck('id');
          }else if($role[0] == 'pustak')
          {
            $findKadept = User::role(['kapustak'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;
          }else if (strtolower(($role[0]) == 'senat' || strtolower($role[0]) == 'korsis' || strtolower($role[0]) == 'staf korsis'))
          {
            $findKadept = User::role(['dankorsis'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;

            $tembusanSubSatker = User::select('id', 'departemen_id')
                                  ->whereNotIn('id', $tmbsTujuanDari)
                                  ->role(['korsis', 'staf korsis'])
                                  ->get()
                                  ->pluck('id');
          }else if (strtolower($role[0]) == 'staf oyu')
          {
            $findKadept = User::role(['kapusoyu'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;

            $tembusanSubSatker = User::select('id', 'departemen_id')
                                  ->whereNotIn('id', $tmbsTujuanDari)
                                  ->role(['staf oyu'])
                                  ->get()
                                  ->pluck('id');
          }else{
            $nota_dinas->dicek_oleh = $input['user_id'];
            $nota_dinas->status = "terkirim";

            $kepalaSatker = ['kadept', 'kapustak', 'dankorsis', 'dirdik', 'dirmin', 'kapusoyu'];
            if(in_array(strtolower($user->roles[0]->name), $kepalaSatker)) {
              if(strtolower($user->roles[0]->name) == 'kadept') {
                $tembusanSubSatker = User::select('id', 'departemen_id')
                  ->whereNotIn('id', $tmbsTujuanDari)
                  ->role(satkerWithSubSatker(strtolower($user->roles[0]->name)))
                  ->where('departemen_id', $user->departemen_id)
                  ->get()
                  ->pluck('id');
              }else{
                $tembusanSubSatker = User::select('id', 'departemen_id')
                  ->whereNotIn('id', $tmbsTujuanDari)
                  ->role(satkerWithSubSatker(strtolower($user->roles[0]->name)))
                  ->get()
                  ->pluck('id');
              }
            }
          }

          $nota_dinas->save();
          
          if(!empty($input['tembusan']))
          {
            foreach($input['tembusan'] as $k => $val)
            {
              if($val){
                $tembusan = new TujuanNotaDinas;
                $tembusan->user_id = $val['id'];
                $tembusan->jenis = 'Tembusan';
                $tembusan->id_nota_dinas = $nota_dinas->id;
                $tembusan->save();
              }
            }

            foreach($tembusanSubSatker as $k => $tsb)
            {
              if($tsb){
                $tembusan = new TujuanNotaDinas;
                $tembusan->user_id = $tsb;
                $tembusan->jenis = 'Tembusan';
                $tembusan->id_nota_dinas = $nota_dinas->id;
                $tembusan->is_sistem = 1;
                $tembusan->save();
              }
            }
          }
         
          if(!empty($input['tujuan']))
          {
            foreach($input['tujuan'] as $key => $value)
            {
              if ($value) {
                $tujuan = new TujuanNotaDinas;
                $tujuan->user_id = $value['id'];
                $tujuan->jenis = "Tujuan";
                $tujuan->id_nota_dinas = $nota_dinas->id;
                $tujuan->save();
              }
            }
          }

          if(count($input['lampiran']) > 0)
          {
            foreach($input['lampiran'] as $ke => $val)
            {
                $lampiran = new LampiranNotaDinas;
                $lampiran->lampiran = $val['file'];
                $lampiran->id_nota_dinas = $nota_dinas->id;
                $lampiran->save();
            }
          }

          if(count($input['atachment']) > 0)
          {
            foreach($input['atachment'] as $ke => $val)
            {
                $file = \uploadbase64Doc($val, '/modules/dms/nota-dinas/file/');
                // dd($file);
                $attach = new AttachmentNotaDinas;
                $attach->file = $file;
                $attach->tipe = explode('.',$file)[1];
                $attach->id_nota_dinas = $nota_dinas->id;
                $attach->save();  
            }
          }
       
          return $this->sendResponse($nota_dinas, 'Surat berhasil dibuat, silahkan lihat surat anda pada menu jejak surat.');
    }

    public function editnotadinas(Request $request, $id){
        $input = $request->all();
          $messages = [
              'tujuan.required'    => 'Tujuan belum dipilih.',
              'tujuan.array'    => 'Tujuan Yang Dikirim Harus Array.',
              'perihal.required'    => 'Perihal belum diisi.',
              'tembuasan.required'    => 'Tembusan belum dipilih.',
              'tembusan.array'    => 'Tembusan Yang Dikirim Harus Array.',
              'surat.required'    => 'File Surat belum diisi.',
              'user_id' => 'Pembuat surat tidak ditemukan, hubungi administator untuk mengecek sistem'
            ];
  
          $validator = Validator::make($input, [
              'tujuan' => 'required|array',
              'perihal' => 'required',
              'surat' => 'required',
              'tembusan' => 'required|array',
              'user_id' => 'required'
          ], $messages);
  
          if($validator->fails()){
              return $this->sendError('Validation Error.', $validator->errors());
          }
          
          $user = User::find($input['user_id']);
          $role = $user->getRoleNames();
          $nota_dinas = NotaDinas::find($id);
          $nota_dinas->perihal = $input['perihal'];
          $nota_dinas->user_id = $input['user_id'];
          $nota_dinas->surat = $input['surat'] ;
          $nota_dinas->status = 'dicek';
          
          if(count($input['tembusan']) > 0) {
            TujuanNotaDinas::where(['id_nota_dinas' => $id])->delete();
          }else if(count($input['tujuan']) > 0) {
            TujuanNotaDinas::where(['id_nota_dinas' => $id])->delete();
          }
    
          if($role[0] == 'staf departemen')
          {
            $findKadept = User::where('departemen_id' , $user->departemen_id)->role(['kadept'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;
          }
          else if($role[0] == 'staf dirdik')
          {
            $findKadept = User::role(['dirdik'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;
          }
          else if ($role[0] == 'staf dirmin')
          {
            $findKadept = User::role(['dirmin'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;
          }else if($role[0] == 'pustak')
          {
            $findKadept = User::role(['kapustak'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;
          }else if($role[0] == 'senat')
          {
            $findKadept = User::role(['korsis'])->first();
            $nota_dinas->dicek_oleh = $findKadept->id;
          }else{
            $nota_dinas->dicek_oleh = $input['user_id'];
          }

          $nota_dinas->save();

          if(!empty($input['tembusan']))
          {
            foreach($input['tembusan'] as $k => $val)
            {
              if($val){
                $tembusan = new TujuanNotaDinas;
                $tembusan->user_id = $val['id'];
                $tembusan->jenis = 'Tembusan';
                $tembusan->id_nota_dinas = $nota_dinas->id;
                $tembusan->save();
              }
            }
          }
          
          if(!empty($input['tujuan']))
          {
            foreach($input['tujuan'] as $key => $value)
            {
              if($value){
                $tujuan = new TujuanNotaDinas;
                $tujuan->user_id = $value['id'];
                $tujuan->jenis = "Tujuan";
                $tujuan->id_nota_dinas = $nota_dinas->id;
                $tujuan->save();
              }
            }
          }

          if(count($input['lampiran']) > 0)
          {
            foreach($input['lampiran'] as $ke => $val)
            {
                $lampiran = new LampiranNotaDinas;
                $lampiran->lampiran = $val['file'];
                $lampiran->id_nota_dinas = $nota_dinas->id;
                $lampiran->save();
            }
          }

          if(count($input['atachment']) > 0)
          {
            foreach($input['atachment'] as $ke => $val)
            {
                $file = \uploadbase64File($val, '/modules/dms/nota-dinas/file/');
                $attach = new AttachmentNotaDinas;
                $attach->file = $file['filename'];
                $attach->tipe = $file['type'];
                $attach->id_nota_dinas = $nota_dinas->id;
                $attach->save();
            }
          }
       
          return $this->sendResponse($nota_dinas, 'Surat berhasil dibuat, silahkan lihat surat anda pada menu jejak surat.');
    
    }


    public function jejakNotaDinas($id)
    {
        $query =  NotaDinas::with(['user' ,'lampiran','dicek', 'tujuan.user' , 'tembusan.user' , 'attachment' , 'revisi.perbaikan' , 'perbaikan'])->find($id);
        return $this->sendResponse($query , "Jejak Surat Diterima");
    }


    public function lampiran($id)
    {
        $query =  LampiranNotaDinas::find($id);
        return $this->sendResponse($query , "Jejak Surat Diterima");
    }


    public function revisiNotaDinas($id)
    {
        $query =  revisiNotaDinas::with('lampiran')->find($id);
        return $this->sendResponse($query , "Jejak Surat Diterima");
    }


    public function storeRevisiNotaDinas(Request $request)
    {
        $input = $request->all();
        //  dd($input['id_role_pemeriksa']);
          $messages = [
              'id_nota_dinas.required'    => 'ID Nota Dinas Masih Kosong.',
              'surat_revisi.required'    => 'Surat Revisi Masih Kosong.',
              'user_revisi.required' => "ID user revisi Masih kosong"
          ];
  
          $validator = Validator::make($input, [
              'id_nota_dinas' => 'required',
              'surat_revisi' => 'required',
              'user_revisi' => 'required',

          ], $messages);

          $status = null ;

          if($request->status == 'teruskan')
          {
            $tujuan = TujuanNotaDinas::where('id_nota_dinas' ,$input['id_nota_dinas'])->get();
            $status = 'legalisir';
            $nota_dinas = NotaDinas::find($input['id_nota_dinas']);

            $nota_dinas->status = $status;
            $nota_dinas->save();
           
            return $this->sendResponse($nota_dinas , "Data berhasil disimpan");
          }else
          {
            $status = 'revisi';
            $nota_dinas = NotaDinas::find($input['id_nota_dinas']);
            $nota_dinas->status = $status;
            $nota_dinas->save();

            $revisi = new RevisiNotaDinas;
            $revisi->id_nota_dinas = $input['id_nota_dinas'];
            $revisi->surat_revisi = $input['content'];
            $revisi->catatan = $input['catatan']?$input['catatan']:'';
            $revisi->user_revisi = $input['user_revisi'];
            $revisi->save();

            if($request->lampiran)
            {
                if(count($request->lampiran) > 0)
                {
                    foreach($request->lampiran as $key => $val)
                    {
                        $revisilampiran = new RevisiLampiranNotaDinas;
                        $revisilampiran->id_revisi = $revisi->id;
                        $revisilampiran->id_lampiran = $val['id'];
                        $revisilampiran->lampiran_revisi = $val['lampiran'];
                        $revisilampiran->save();
                    }
                } 
            }

            return $this->sendResponse($revisi , "Data berhasil disimpan");
          }
    }

    public function storePerbaikanNotaDinas(Request $request)
    {
        $input = $request->all();
        //  dd($input['id_role_pemeriksa']);
          $messages = [
              'id_nota_dinas.required'    => 'ID Nota Dinas Masih Kosong.',
              'perbaikan_surat.required'    => 'Surat Perbaikan Masih Kosong.',
              'id_revisi.required' => 'ID Revisi Surat'
          ];
  
          $validator = Validator::make($input, [
              'id_nota_dinas' => 'required',
              'perbaikan_surat' => 'required',
              'id_revisi' => 'required'
          ], $messages);

          //update status nota dinas
          $nota_dinas = NotaDinas::find($input['id_nota_dinas']);
          $nota_dinas->status = 'perbaikan';
          $nota_dinas->surat = $input['perbaikan_surat'];
          $nota_dinas->save();


          
          //simpan log perbaikan
          $revisi = new PerbaikanNotaDinas;
          $revisi->id_nota_dinas = $input['id_nota_dinas'];
          $revisi->id_revisi = $input['id_revisi'];
          $revisi->perbaikan_surat = $input['perbaikan_surat'];
          $revisi->save();
          

          if($request->lampiran)
          {
             if(count($request->lampiran) > 0)
             {
                 foreach($request->lampiran as $key => $value)
                 {
                     $lampiran = LampiranNotaDinas::find($value['id_lampiran']);
                     if($lampiran)
                     {
                         $lampiran->lampiran = $value['lampiran_revisi'];
                         $lampiran->save();
                     }
                 }
             } 
          }


          return $this->sendResponse($revisi , "Revisi berhasil dikirim");
    }


    public function storePerbaikanNotaDinasDraft(Request $request)
    {
        $input = $request->all();
        //  dd($input['id_role_pemeriksa']);
          $messages = [
              'id_nota_dinas.required'    => 'ID Nota Dinas Masih Kosong.',
              'perbaikan_surat.required'    => 'Surat Perbaikan Masih Kosong.',
              'id_revisi.required' => 'ID Revisi Surat'
          ];
  
          $validator = Validator::make($input, [
              'id_nota_dinas' => 'required',
              'perbaikan_surat' => 'required',
              'id_revisi' => 'required'
          ], $messages);

          
          //simpan log perbaikan
          $revisi = revisiNotaDinas::find($input['id_revisi']);
          $revisi->surat_revisi = $input['perbaikan_surat'];
          $revisi->save();


          if($request->lampiran)
          {
             if(count($request->lampiran) > 0)
             {
                 foreach($request->lampiran as $key => $value)
                 {
                     $lampiran = RevisiLampiranNotaDinas::find($value['id']);
                     if($lampiran)
                     {
                         $lampiran->lampiran_revisi = $value['lampiran_revisi'];
                         $lampiran->save();
                     }
                 }
             } 
          }

          return $this->sendResponse($revisi , "Revisi berhasil disimpan");
    }

    public function storeVerifikasiSukses(Request $request)
    {
          $input = $request->all();
        //  dd($input['id_role_pemeriksa']);
          $messages = [
              'id_nota_dinas.required'    => 'ID Nota Dinas Masih Kosong.',
          ];
  
          $validator = Validator::make($input, [
              'id_nota_dinas' => 'required',
          ], $messages);

       // $perbaikan = PerbaikanNotaDinas::where('id_nota_dinas' , $input['id_nota_dinas'])->orderBy('created_at' , 'DESC')->first();

         //update status nota dinas
        $nota_dinas = NotaDinas::findOrFail($input['id_nota_dinas']);
        //if($perbaikan)
        //{
          //  $html = HtmlDomParser::str_get_html($perbaikan['surat_perbaikan']);
        //}else
        //{
        $html = HtmlDomParser::str_get_html($nota_dinas['surat']);
        //}
         
         //manipulate dom cap
         if($html->find('.tanda_tangan', 0)) {
             $html->find('.tanda_tangan', 0)->outertext = '<img src="/modules/dms/cap/cap.png" style="width:60px"></img>';
         }

         $nota_dinas->status = 'terkirim';
         $nota_dinas->surat = $html;
         $nota_dinas->save();

         return $this->sendResponse([] , "Data berhasil di disimpan");
    }

    public function readFile(Request $request) {
      $surat = TujuanNotaDinas::where('id_nota_dinas', $request->id_surat)
              ->where('user_id', $request->id_user)
              ->update(['status_read' => 1]);

      return $this->sendResponse($surat, "Data berhasil di update");
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('dms::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('dms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete(Request $request, $id)
    {

        $data = NotaDinas::find($id);
        if($data) {
          NotaDinas::where('id', $data->id)->delete();  
          TujuanNotaDinas::where('id_nota_dinas', $data->id)->delete();
          return $this->sendResponse($data, 'Data berhasil dihapus');
        }else{
          return $this->sendResponse([], 'Data tidak ditemukan.');
        }
    }

}
