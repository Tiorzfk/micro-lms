<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamQuestions extends Model
{
    use HasFactory;
    protected $connection = 'elearning';
    protected $table = 'exam_questions';
    protected $fillable = ['exam_id','question_name','categories_id','serial_number','mp_id'];
    protected $appends = ['first_answer'];
    
    public function kategori()
    {
        return $this->hasOne('\Modules\Elearning\Entities\QuestionCategory', 'id', 'categories_id');
    }

    public function rightAnswer()
    {
        return $this->hasMany('\Modules\Elearning\Entities\ExamQuestionAnswers', 'exam_question_id', 'id')->orderBy('id','ASC');
    }

    public function answer()
    {
        return $this->hasMany('\Modules\Elearning\Entities\ExamQuestionAnswers', 'exam_question_id', 'id')->orderBy('id','ASC');
    }

    public function getFirstAnswerAttribute()
    {
        $data = QuestionAnswers::where('question_id', $this->id)->where('istrue', '1')->orderBy('id','ASC')->get();
        return $data;
    }

    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\ExamQuestionsFactory::new();
    }
}
