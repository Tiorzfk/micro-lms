<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\RefreshTokens;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        try {
            $messages = [
                'username.required' => trans('validation.required'),
                'password.required' => trans('validation.required'),
            ];
            
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required',
            ] , $messages);
            
            if($validator->fails()){
                return response()->json([
                    'status' => 'error',
                    'data' => $validator->errors()
                ], 400);
            }
            
            $credentials = request(['username', 'password']);

            if (! $token = auth()->attempt($credentials)) {
                return response()->json([
                    'status' => 'error',
                    'data' => 'Username atau password yang anda masukan salah'
                ],404);
            }
              
            $user = User::find(\Auth::user()->id);
            $user->islogin = 1;
            $user->last_login = \Carbon\Carbon::now();
            $user->save();
            
            return response()->json([
                'status' => 'success',
                'data' => $user
            ]);
        } catch(\Throwable $e) {
            Log::error('LoginController [login] '. $e);
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ],500);
        }
    }

    public function refreshTokens(Request $request)
    {
        try {
            $messages = [
                'refresh_token.required' => trans('validation.required'),
                'user_id.required' => trans('validation.required'),
            ];
            
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'refresh_token' => 'required',
            ] , $messages);
            
            if($validator->fails()){
                return response()->json([
                    'status' => 'error',
                    'data' => $validator->errors()
                ], 400);
            }
            
            $user = new RefreshTokens;
            $user->user_id = $request->user_id;
            $user->token = $request->refresh_token;
            $user->save();
            
            return response()->json([
                'status' => 'success',
                'data' => $user
            ]);
        } catch(\Throwable $e) {
            return response()->json([
                'status' => 'error',
                'data' => $e->getMessage()
            ],500);
        }
    }

    public function myRoles(Request $request, $id)
    {
        $user = User::with('roles')->find($id)->roles;

        if(count($user) > 0)
        {
            $user = $user->pluck('name')->toArray();
        }else{
            $user = [];
        }

        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function getUsers(Request $request)
    {
        $user = User::role($request->role)->orderBy('id', 'DESC');

        if($request->generation_id)
        {
            $user->where('generation_id', $request->generation_id);
        }

        if($request->jabatan)
        {
            $user->where('jabatan', $request->jabatan);
        }

        $user = $user->get();

        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function getDetailUser(Request $request, $id)
    {
        $user = User::find($id);
        
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
}
