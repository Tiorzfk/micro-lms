<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('courses')->group(function() {
    Route::get('/', 'CoursesController@index');
    Route::get('/{id}', 'CoursesController@show');
    Route::post('/', 'CoursesController@store');
    Route::post('/enrol', 'CoursesController@enrol');
    Route::post('/upload-ns', 'CoursesController@uploadNs');
    Route::put('/{id}', 'CoursesController@update');
    Route::delete('/{id}', 'CoursesController@destroy');
    Route::get('/report/{id}', 'CoursesController@report');
});
Route::prefix('contents')->group(function() {
    Route::get('/{id}', 'ContentsController@index');
    Route::get('/detail/{id}', 'ContentsController@show');
    Route::post('/', 'ContentsController@store');
    Route::post('/progress', 'ContentsController@storeProgress');
    Route::post('/order', 'ContentsController@orderContent');
    Route::put('/{id}', 'ContentsController@update');
    Route::delete('/{id}', 'ContentsController@destroy');
});
