<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CourseContentQuestion extends Model
{
    use HasFactory;
    public $label = 'Contents With Question';

    protected $connection = 'elearning';
    protected $table = 'course_content_question';

    protected $fillable = [
        'content_id',
        'question_id',
    ];

    public function question()
    {
        return $this->hasOne('\Modules\Elearning\Entities\Questions','id', 'question_id');
    }
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\CourseContentQuestionFactory::new();
    }
}
