const addGroup = require('./addGroup');
const addPersonal = require('./addPersonal');
const getGroup = require('./getGroup');
const getPersonal = require('./getPersonal');
const uploadTugasGroup = require('./uploadTugasGroup');
const uploadTugasPersonal = require('./uploadTugasPersonal');

module.exports = {
    addGroup,
    addPersonal,
    getGroup,
    getPersonal,
    uploadTugasGroup,
    uploadTugasPersonal,
};