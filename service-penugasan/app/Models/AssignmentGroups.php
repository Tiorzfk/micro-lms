<?php

namespace Modules\Ams\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentGroups extends Model
{
    use HasFactory;

    protected $connection = 'ams';
    protected $table = 'assignment_groups';
    protected $fillable = [
        'created_by',
        'generation_id',
        'stages_id'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentGroupsFactory::new();
    }
}
