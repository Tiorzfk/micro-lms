<?php

namespace Modules\Ams\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use DB;
use Auth;
use Modules\Ams\Entities\AssignmentPersonal;
use Modules\Ams\Entities\AssignmentPersonalCollect;
use Illuminate\Support\Facades\Log as LogSystem;
use Modules\Admin\Entities\Departement;
use Modules\Admin\Entities\KurikulumRpp;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserLite;
use ZanySoft\Zip\Zip;
use ZanySoft\Zip\ZipManager;

class AssignmentPersonalController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');
            $stage_id = $request->input('stage_id');
            $role = \Auth::user()->rolesname->toArray();
            $sortBy = $request->input('sortBy');
            $tab = $request->input('tab');
            $now = Carbon::now();

            $query = AssignmentPersonal::select('assignment_personal.id', 'assignment_personal.description as name', 'assignment_personal.file',
                        'assignment_personal.deadline','assignment_personal.is_guidance','assignment_personal.generation_id',
                        'assignment_personal.stages_id', 'dep.kode as kode_departemen', 'dep.name as name_departemen',
                        'dep.color as dep_color', 'assignment_personal.created_at', 'users.name as name_user', 'assignment_personal.sbs_id')
                        ->join('seskoad_admin.kurikulum_rpp as sbs', 'sbs.id', 'assignment_personal.sbs_id')
                        ->join('seskoad_admin.departement as dep', 'sbs.kode_departemen', 'dep.kode')
                        ->join('seskoad_admin.users as users', 'users.id', 'assignment_personal.created_by')
                        ->where('assignment_personal.generation_id', get_angkatan_aktif()->id);

            if(in_array('staf departemen', $role))
            {
                $dep = Departement::find(\Auth::user()->departemen_id);
                $sbs_id = KurikulumRpp::where('kode_departemen', $dep->kode)->get()->pluck('id')->toArray();
                $query->whereIn('sbs_id', $sbs_id);
            }else if(in_array('dosen', $role))
            {
                
            }else if(in_array('pasis', $role))
            {
                $tugas = AssignmentPersonalCollect::whereIn('assignment_id', $query->get()->pluck('id')->toArray())
                        ->where('user_id', \Auth::user()->id)
                        ->get()
                        ->pluck('assignment_id')
                        ->toArray();

                if($tab == 'penugasan')
                {
                    $query->where('deadline', '>=', $now);
                }else if($tab == 'selesai')
                {
                    $query->join('assignment_personal_collect as apc', 'apc.assignment_id', 'assignment_personal.id')
                            ->where('deadline', '<', $now);
                }else if($tab == 'terlewat')
                {
                    $query->where('deadline', '<', $now)
                            ->whereNotIn('id', $tugas);
                }
            }
            
            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                      $query->where('description', 'like', '%' . $searchValue . '%');
                      $query->orWhere('deadline', 'like', '%' . $searchValue . '%');
                });
            }

            if($stage_id)
            {
                $query->where('stages_id', $stage_id);
            }else{
                $query->where('stages_id', get_stage_aktif()->id);
            }

            if($sortBy)
            {
                if(strtolower($sortBy) == 'terbaru')
                {
                    $query->orderBy('id', 'DESC');

                }else if(strtolower($sortBy) == 'terlama')
                {
                    $query->orderBy('id', 'ASC');
                }else if(strtolower($sortBy) == '7 hari sebelumnya')
                {
                    $last7Day = Carbon::parse(Carbon::now()->subDays(7)->format('Y-m-d'));
                    $query->where(DB::raw('assignment_personal.created_at', '%Y-%m-%d'), '>=', $last7Day);
                }else if(strtolower($sortBy) == '30 hari sebelumnya')
                {
                    $last30Day = Carbon::parse(Carbon::now()->subDays(30)->format('Y-m-d'));
                    $query->where(DB::raw('assignment_personal.created_at', '%Y-%m-%d'), '>=', $last30Day);
                }else{
                    $query->orderBy('id', 'DESC');
                }
            }else{
                $query->orderBy('id', 'DESC');
            }

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) use ($now, $role) {
                $q->deadline_format = get_date($q->deadline, true);
                $q->created_at_format = get_date($q->created_at, true);
                $deadline = Carbon::parse($q->deadline);

                if(in_array('pasis', $role))
                {
                    $cek = AssignmentPersonalCollect::where('assignment_id', $q->id)
                        ->where('user_id', \Auth::user()->id)
                        ->first();
                    if($cek && $now > $deadline)
                    {
                        $q->status = 'Selesai';
                    }else if($cek && $now <= $deadline){
                        $q->status = 'Selesai';
                    }else if(!$cek && $now > $deadline){
                        $q->status = 'Terlewat';
                    }else{
                        $q->status = 'Belum dikumpulkan';
                    }
                }else{
                    $count_pasis = User::role('pasis')->where('generation_id', $q->generation_id)
                                ->where('active_status' , 1)
                                ->get()
                                ->count();

                    $count_tugas = AssignmentPersonalCollect::where('assignment_id', $q->id)->get()->count();

                    $q->status = $count_tugas.' / '.$count_pasis.' Terkumpul';
                }

                $q->sbs = KurikulumRpp::select('id', 'name')->find($q->sbs_id);
                
                return $q;
            });

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment indexPersonal] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'sbs_id.required'=>trans('validation.required'),
                'description.required'=>trans('validation.required'),
                'file.required'=>trans('validation.required'),
                'deadline.required'=>trans('validation.required'),
                'file.max'=>trans('validation.max'),
                'file.mimes'=>trans('validation.mimes'),
            ];

            $niceNames = [
                'sbs_id' => trans('ams.sbs_id'),
                'description' => trans('ams.topic'),
                'file' => trans('ams.sheet'),
                'deadline' => trans('ams.deadline'),
            ]; 

            $validator = Validator::make($input, [
                'sbs_id'=> 'required', 
                'description'=> 'required', 
                'file'=> 'required|max:200000|mimes:pdf,doc,docx,zip,rar', 
                'deadline'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            if(!$request->hasFile('file'))
            {
                return $this->sendResponse(400, 'Validation Error.', (object) ['file' => [trans('ams.sheet').' '.trans('ams.required')]]);
            }

            $asgn = new AssignmentPersonal;
            $asgn->created_by = \Auth::user()->id;
            $asgn->generation_id = get_angkatan_aktif()->id;
            $asgn->stages_id = get_stage_aktif()->id;
            $asgn->sbs_id = $input['sbs_id'];
            $asgn->description = $input['description'];
            $asgn->deadline = $input['deadline'];
            $asgn->is_guidance = $input['is_guidance'] ? 1 : 0;

            $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
            $path = public_path()."/assets/admin/ams/personal";
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $input['file']->move($path,$file_name);

            $asgn->file = $file_name;

            $asgn->save();
            try {
                $pasis_aktif = User::select('id')->role('pasis')->where('generation_id',get_angkatan_aktif()->id)->pluck('id');
                foreach ($pasis_aktif as $key => $value) {
                    addNotif(
                            Auth::user()->id,
                            $value,
                            'AMS/Home/Tab/Penugasan/Detail/'.$asgn->id.'/mandiriPenugasan',
                            null,
                            'Anda mendapatkan penugasan baru bernama '.$asgn->description.'','Penugasan Mandiri'
                        );
                }
            } catch (\Throwable $th) {
                LogSystem::error('[Assignment storePersonal] : ' . $th);
                return $this->sendResponse(500, $this->messageError(), $th->getMessage());
            }

            // addNotif(
            //     Auth::user()->id,
            //     $user,
            //     'Ams/',
            //     null,
            //     'Anda mendapatkan penugasan baru bernama '.$asgn->description.'','Evaluasi'
            // );

            insert_log([
                'model' => $asgn,
                'action' => 'created',
                'data' => $asgn->description
            ]);

            return $this->sendResponse(200, trans('validation.created'), $asgn);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment storePersonal] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function uploadTugas(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'file.required'=>trans('validation.required'),
                'file.max'=>trans('validation.max'),
                'file.mimes'=>trans('validation.mimes'),
            ];

            $niceNames = [
                'file' => trans('ams.file_tugas'),
            ]; 

            $validator = Validator::make($input, [
                'file'=> 'required|max:100000|mimes:pdf,doc,docx,zip,rar', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            if(!$request->hasFile('file'))
            {
                return $this->sendResponse(400, 'Validation Error.', (object) ['file' => [trans('ams.file_tugas').' '.trans('ams.required')]]);
            }

            $tugas = AssignmentPersonalCollect::where('assignment_id', $id)
                        ->where('user_id', \Auth::user()->id)
                        ->first();

            $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
            $path = public_path()."/assets/admin/ams/tugas-personal";
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $input['file']->move($path,$file_name);

            if($tugas)
            {
                //delete old file
                if(file_exists($path.'/'.$tugas->file))
                {
                    unlink($path.'/'.$tugas->file);
                }
            }else{
                $tugas = new AssignmentPersonalCollect;
                $tugas->assignment_id = $id;
                $tugas->user_id = \Auth::user()->id;
            }

            $tugas->file = $file_name;
            $tugas->save();

            insert_log([
                'model' => $tugas,
                'action' => 'uploaded',
                'data' => 'Tugas '.AssignmentPersonal::find($id)->description
            ]);

            return $this->sendResponse(200, trans('validation.created'), $tugas);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment uploadTugas] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function listTugasPasis(Request $request, $id)
    {
        try {
            $assignment = AssignmentPersonal::find($id);
            $url = config('app.url').'/assets/admin/ams/tugas-personal/';

            $pasis = User::role('pasis')
                    ->select('users.id','users.name','users.nrp','users.generation_id','users.active_status',
                    'apc.file as file_tugas', 'apc.created_at as tanggal', 'apm.id as assignment_personal_mentor_id', 'apm.mentor_id',
                    'users_mentor.name as pembimbing','apc.assignment_id', DB::raw('CONCAT("'.$url.'", apc.file) as file_url'))
                    ->leftJoin('seskoad_ams.assignment_personal_collect as apc', 'apc.user_id', 'users.id')
                    ->leftJoin('seskoad_ams.assignment_personal_mentor as apm', 'apm.user_id', 'users.id')
                    ->leftJoin('users as users_mentor', 'users_mentor.id', 'apm.mentor_id')
                    ->where('users.generation_id', $assignment->generation_id)
                    ->where('users.active_status' , 1)
                    ->where(function($q) use ($id) {
                        $q->where('apc.assignment_id' , $id)
                            ->orWhere('apc.assignment_id' , null);
                        return $q;
                    })
                    ->get();

            return $this->sendResponse(200, trans('validation.success'), $pasis);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment listTugasPasis] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }
    
    public function deleteTugas(Request $request, $id)
    {
        try {
            $tugas = AssignmentPersonalCollect::where('assignment_id', $id)
                        ->where('user_id', \Auth::user()->id)
                        ->first();

            if($tugas)
            {
                $path = public_path()."/assets/admin/ams/tugas-personal";
                //delete old file
                if(file_exists($path.'/'.$tugas->file))
                {
                    unlink($path.'/'.$tugas->file);
                }

                $tugas->delete();
                insert_log([
                    'model' => $tugas,
                    'action' => 'deleted',
                    'parameter' => $tugas
                ]);
            }

            return $this->sendResponse(200, trans('validation.deleted'), $tugas);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment uploadTugas] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try {
            $role = \Auth::user()->rolesname->toArray();
            $now = Carbon::now();

            $q = AssignmentPersonal::select('assignment_personal.id', 'assignment_personal.description as name', 'assignment_personal.file',
                        'assignment_personal.deadline','assignment_personal.is_guidance','assignment_personal.generation_id',
                        'assignment_personal.stages_id', 'dep.kode as kode_departemen', 'dep.name as name_departemen',
                        'dep.color as dep_color', 'assignment_personal.created_at', 'sbs.name as sbs_name', 'users.name as name_user')
                        ->join('seskoad_admin.kurikulum_rpp as sbs', 'sbs.id', 'assignment_personal.sbs_id')
                        ->join('seskoad_admin.departement as dep', 'sbs.kode_departemen', 'dep.kode')
                        ->join('seskoad_admin.users as users', 'users.id', 'assignment_personal.created_by')
                        ->find($id);

            $q->deadline_format = get_date($q->deadline, true);
            $q->created_at_format = get_date($q->created_at, true);
            $deadline = Carbon::parse($q->deadline);

            $cek = AssignmentPersonalCollect::select('assignment_personal_collect.id', 'assignment_personal_collect.file', 
                    'assignment_personal_collect.created_at', 'users.name')
                    ->join('seskoad_admin.users', 'users.id', 'assignment_personal_collect.user_id')
                    ->where('assignment_id', $q->id)
                    ->where('user_id', \Auth::user()->id)
                    ->first();
            
            $q->file_tugas = $cek ? $cek->file_url : null;
            $q->file_tugas_name = $cek ? $cek->file : null;
            $q->file_tugas_by = $cek ? $cek->name : null;
            $q->file_tugas_date = $cek ? get_date($cek->created_at, true) : null;
            $q->tugas_is_overdue = $now > $deadline ? true : false;

            if(in_array('pasis', $role))
            {
                if($cek && $now > $deadline)
                {
                    $q->status = 'Selesai';
                }else if($cek && $now <= $deadline){
                    $q->status = 'Selesai';
                }else if(!$cek && $now > $deadline){
                    $q->status = 'Terlewat';
                }else{
                    $q->status = 'Belum dikumpulkan';
                }

            }else{
                $pasis = User::role('pasis')
                            ->select('id','generation_id','active_status')
                            ->where('generation_id', $q->generation_id)
                            ->where('active_status' , 1)
                            ->get();

                $count_tugas = AssignmentPersonalCollect::where('assignment_id', $q->id)->get()->count();

                $q->status = $count_tugas.' / '.$pasis->count().' Terkumpul';
            }

            return $this->sendResponse(200, trans('validation.success'), $q);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment show] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('ams::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'sbs_id.required'=>trans('validation.required'),
                'description.required'=>trans('validation.required'),
                'file.required'=>trans('validation.required'),
                'deadline.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'sbs_id' => trans('ams.sbs_id'),
                'description' => trans('ams.topic'),
                'file' => trans('ams.sheet'),
                'deadline' => trans('ams.deadline'),
            ]; 

            $validator = Validator::make($input, [
                'sbs_id'=> 'required', 
                'description'=> 'required', 
                'file'=> 'required', 
                'deadline'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $asgn_old = AssignmentPersonal::find($id);
            $asgn = AssignmentPersonal::find($id);

            $asgn->sbs_id = $input['sbs_id'];
            $asgn->description = $input['description'];
            $asgn->deadline = $input['deadline'];
            $asgn->is_guidance = $input['is_guidance'] ? 1 : 0;

            if($request->hasFile('file')) {
                $file_name = Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                $path = public_path()."/assets/admin/ams/personal";
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $input['file']->move($path,$file_name);

                //delete old file
                if(file_exists($path.'/'.$asgn->file))
                {
                    unlink($path.'/'.$asgn->file);
                }
                
                $asgn->file = $file_name;
            }
                
            $asgn->save();

            insert_log([
                'model' => $asgn,
                'action' => 'modified',
                'parameter' => $asgn_old
            ]);

            return $this->sendResponse(200, trans('validation.created'), $asgn);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment storePersonal] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $asgn = AssignmentPersonal::find($id);
            if(!$asgn){
                return $this->sendResponse(400, 'Data not found', (object)["name" => ["Data not found"]]);
            }
            try {
                $asgn->delete();

                //delete file
                $path = public_path()."/assets/admin/ams/personal";
                if(file_exists($path.'/'.$asgn->file))
                {
                    unlink($path.'/'.$asgn->file);
                }

                insert_log([
                  'model' => $asgn,
                  'action' => 'deleted',
                  'parameter' => $asgn
                ]);
            } catch (\Throwable $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }
            return $this->sendResponse(200,trans('validation.deleted'),$asgn);
        } catch (\Throwable $e) {
            LogSystem::error('[Assignment destroy] : ' . $e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    public function downloadAllTugas($id)
    {
        try {
            $name_file = \Carbon\Carbon::now()->timestamp.'_all_tugas.zip';
            $path = public_path().'/assets/admin/ams/'.$name_file;
            $tugas = AssignmentPersonalCollect::where('assignment_id', $id)->get();
            $zip = Zip::create($path);

            foreach($tugas as $t)
            {
                $zip->add(public_path().'/assets/admin/ams/tugas-personal/'.$t->file);
            }

            $zip->close();

            $url = config('app.url').'/assets/admin/ams/'.$name_file;

            return response()->download($path, $name_file, [
                'Content-Type: application/zip',
                'Content-Length: '.filesize($path)
            ])->deleteFileAfterSend(true);
        } catch (\Throwable $th) {
            LogSystem::error('[Assignment downloadAllTugas] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }
}
