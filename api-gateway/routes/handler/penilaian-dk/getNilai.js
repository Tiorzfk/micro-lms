const apiAdapter = require('../../apiAdapter');
const {
  PENILAIAN_DK_APPS_SERVICE_HOST,
  PENILAIAN_DK_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(PENILAIAN_DK_APPS_SERVICE_HOST, PENILAIAN_DK_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });

    const nilai = await api.get(`/penilaian/rekap-nilai`);
    return res.json(nilai.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}