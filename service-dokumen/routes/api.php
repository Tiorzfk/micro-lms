<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::get('folder', 'DmsFolderController@index')->name('folder.index');
    Route::post('folder', 'DmsFolderController@store')->name('folder.post');
    Route::post('folder/permission', 'DmsFolderController@setPermission')->name('folder.permission');
    Route::delete('folder/permission', 'DmsFolderController@deleteSetPermission')->name('folder.deletePermission');

    Route::group(['prefix' => 'folder/{id}'], function() {
        Route::get('edit', 'DmsFolderController@edit')->name('folder.edit');
        Route::get('file/{id_user?}', 'DmsFolderController@file')->name('folder.file');
        Route::get('detail', 'DmsFolderController@detail')->name('folder.detail');
        Route::get('{type}/', 'DmsFolderController@folderSaya')->name('folder.folderSaya');
        Route::patch('/', 'DmsFolderController@update')->name('folder.update');
        Route::delete('delete', 'DmsFolderController@destroy')->name('folder.delete');
    });

    /* File */
    Route::get('file', 'DmsFileController@index')->name('file.index');
    Route::post('file', 'DmsFileController@store')->name('file.post');
    Route::post('file-nota-dinas', 'DmsFileController@storeNotaDinas')->name('file.post');
    Route::post('file-ttd', 'DmsFileController@storeTtd')->name('file.post-ttd');
    Route::post('file/permission', 'DmsFileController@setPermission')->name('file.permission');
    Route::delete('file/permission', 'DmsFileController@deleteSetPermission')->name('file.deletePermission');
    Route::group(['prefix' => 'file/{id}'], function() {
        Route::get('edit', 'DmsFileController@edit')->name('file.edit');
        Route::get('detail', 'DmsFileController@detail')->name('file.detail');
        Route::patch('/', 'DmsFileController@update')->name('file.update');
        Route::delete('delete', 'DmsFileController@destroy')->name('file.delete');
    });
