<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use DB;
// use Modules\Admin\Entities\KurikulumRpp;
use App\Models\Departement;
use App\Models\Questions;
use App\Models\QuestionCategory;
use App\Models\QuestionAnswers;
use Illuminate\Support\Facades\Log as LogSystem;

class QuestionController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $searchValue =  $request->input('search');
            $mp_id =  $request->input('mp_id');
            $sbs_id =  $request->input('sbs_id');
            $filterValue =  $request->input('filterValue');

            $query = Questions::with('kategori','rightAnswer')
                        ->orderBy('updated_at','DESC');

            if($filterValue && $filterValue != 'all'){
                $query = $query->where('category_id', $filterValue);
            }

            if($mp_id)
            {
                $query = $query->where('mp_id',$mp_id);
            }

            if($sbs_id)
            {
                // $mp = KurikulumRpp::where('parent_id', $sbs_id)->get()->pluck('id')->toArray();
                // $query = $query->whereIn('mp_id',$mp);
            }
            
            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                    $query->where('name', 'like', '%' . $searchValue . '%');
                });
            }

            $query = $query->paginate(10);
            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function getQuestionSbs(Request $request, $id)
    {
        try {
            $searchValue =  $request->input('search');
            $mp_id =  $request->input('mp_id');
            $filterValue =  $request->input('filterValue');
            $sbs_id = KurikulumRpp::where('parent_id', $id)->pluck('id');
            $query = Questions::with('kategori','rightAnswer')->whereIn('mp_id',$sbs_id)->orderBy('updated_at','DESC');

            // if($filterValue != 'all'){
            //     $query = $query->where('category_id', $filterValue);
            // }
            
            // if ($searchValue) {
            //     $query = $query->where(function($query) use ($searchValue) {
            //         $query->where('name', 'like', '%' . $searchValue . '%');
            //     });
            // }

            $query = $query->get();
            // dd($query);
            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('elearning::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $cn = DB::connection('elearning');
        try {
            $cn->beginTransaction();
            $input = $request->all();

            $question = Questions::find($id);
            if(!$question){
                return $this->sendResponse(400, 'Questions not found', (object)["name" => ["Questions tidak ditemukan"]]);   
            }
            if(!$input['name']){
                return $this->sendResponse(400, 'soal wajib diisi', (object)["name" => ["soal wajib diisi"]]);
            }
            $question->name = $input['name'];
            $question->save();
                if($input['kategori']['name'] != 'Esai'){
                        $valueSuccess = 0;
                        $alfa = [];
                        $alfabet = ['A','B','C','D','E','F'];
                        $deleted_answer = QuestionAnswers::where('question_id', $id)->delete();
                        foreach ($input['right_answer'] as $keyss => $query) {
                            $answer = new QuestionAnswers;
                            $answer->question_id = $id;
                            if($query['answer']){
                                $answer->answer = $query['answer'];
                            }else{
                                array_push($alfa,$alfabet[$keyss]);
                            }
                            if($query['is_right']){
                                $answer->istrue = '1';
                                $valueSuccess = 1;
                            }else{
                                $answer->istrue = '0';
                            }
                            $answer->save();
                        }
                }
                if($input['kategori']['name'] != 'Esai'){
                    if(count($alfa) > 0){
                        $cn->rollBack();
                        return $this->sendResponse(400, trans('validation.answer').implode(',',$alfa).trans('validation.wajib_diisi'), (object)["name" => ["jawaban benar belum dipilih"]]);
                    }
                    if($valueSuccess == 0){
                        $cn->rollBack();
                        return $this->sendResponse(400, trans('validation.answer_right'), (object)["name" => ["jawaban benar belum dipilih"]]);
                    }
                 }

                $cn->commit();
                return $this->sendResponse(200, trans('validation.success'), []);
        } catch (\Throwable $th) {
            $cn->rollBack();
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try {
            $query = Questions::with('kategori','rightAnswer')->find($id);
            if(!$query){
                return $this->sendResponse(400, 'Questions not found', (object)["name" => ["Questions tidak ditemukan"]]);   
            }
            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('elearning::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function store(Request $request)
    {
        $cn = DB::connection('elearning');
        try {
            $cn->beginTransaction();
            $input = $request->all();
            $question_exist = [];
                foreach ($input as $key => $value) {
                    $no = $key+1;
                    $data = new Questions;
                    $data->category_id = QuestionCategory::where('name', $value['category'])->first()->id;
                    if($value['question']){
                        $data->name = $value['question'];
                    }else{
                        array_push($question_exist,$no);
                        DB::rollBack();
                    }
                    $data->mp_id = $value['mk_id'];
                    $data->created_by = $request->header('auth');
                    $data->save();
                    if($value['category'] != 'Esai'){
                        $valueSuccess = 0;
                        $alfa = [];
                        $alfabet = ['A','B','C','D','E','F'];
                        foreach ($value['jawaban'] as $keyss => $query) {
                            $answer = new QuestionAnswers;
                            $answer->question_id = $data->id;
                            if($query['name']){
                                $answer->answer = $query['name'];
                            }else{
                                array_push($alfa,$alfabet[$keyss]);
                            }
                            if($query['isRightAnswer']){
                                $answer->istrue = '1';
                                $valueSuccess = 1;
                            }else{
                                $answer->istrue = '0';
                            }
                            $answer->save();
                        }
                    }
                }
                if($value['category'] != 'Esai'){
                    if(count($alfa) > 0 && count($question_exist) > 0){
                        $cn->rollBack();
                        return $this->sendResponse(400, trans('validation.soal_no').implode(',',$question_exist).' '. trans('validation.and').' '.trans('validation.answer').implode(',',$alfa).trans('validation.wajib_diisi'), (object)["name" => ["jawaban benar belum dipilih"]]);
                    }
                    if(count($alfa) > 0){
                        $cn->rollBack();
                        return $this->sendResponse(400, trans('validation.answer').' '.implode(',',$alfa).' '.trans('validation.wajib_diisi'), (object)["name" => ["jawaban benar belum dipilih"]]);
                    }
                    if($valueSuccess == 0){
                        $cn->rollBack();
                        return $this->sendResponse(400, trans('validation.answer_right'), (object)["name" => ["jawaban benar belum dipilih"]]);
                    }
                 }else{
                    if(count($question_exist) > 0){
                        $cn->rollBack();
                        return $this->sendResponse(400, trans('validation.soal_no').' '.implode(',',$question_exist).' '.trans('validation.wajib_diisi'), (object)["name" => ["jawaban benar belum dipilih"]]);
                    }
                 }

                $cn->commit();
                return $this->sendResponse(200, trans('validation.success'), []);
        } catch (\Throwable $th) {
            $cn->rollBack();
            LogSystem::error('[QuestionController store] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $data = Questions::find($id);
            if(!$data){
                return $this->sendResponse(400, 'Data tidak ditemukan', (object)["name" => ["Data tidak ditemukan"]]);
            }

            try {
                $data->delete();
            } catch (\Illuminate\Database\QueryException $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }

            return $this->sendResponse(200,trans('validation.deleted'),$data);
        } catch (\Throwable $e) {
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }
}
