const getBimbingan = require('./getBimbingan');
const setPembimbing = require('./setPembimbing');
const ajukanBimbingan = require('./ajukanBimbingan');
const editStatus = require('./editStatus');
const uploadNaskah = require('./uploadNaskah');
const uploadRevisiNaskah = require('./uploadRevisiNaskah');

module.exports = {
    getBimbingan,
    setPembimbing,
    ajukanBimbingan,
    editStatus,
    uploadNaskah,
    uploadRevisiNaskah,
};