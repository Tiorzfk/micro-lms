<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use DB;
use Auth;
use App\Models\User;
use Illuminate\Support\Str;
use Modules\Admin\Entities\KurikulumRpp;
use Modules\Admin\Entities\Departement;
use App\Models\Exams;
use App\Models\ExamEnrolls;
use App\Models\QuestionAnswers;
use App\Models\ExamQuestions;
use App\Models\GamificationHistoryUser;
use App\Models\GamificationPoint;
use App\Models\ExamQuestionAnswers;
use App\Models\QuestionCategory;
use App\Models\ExamAnswer;
use Illuminate\Support\Facades\Log as LogSystem;
use App\Models\Questions;

class ExamController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $filterValue = $request->input('filterValue');
            $searchValue = $request->input('search');
            $departement_id = $request->input('departement_id');
            $generation_id = (object) ['id' => 1];
            if(!$generation_id){
                return $this->sendResponse(400, 'silahkan untuk mengaktifkan angkatan', (object)["name" => ["silahkan untuk mengaktifkan angkatan"]]);
            }
            $client = new \GuzzleHttp\Client();
            $role = auth_login()->roles_name;
            
            $query = Exams::where('generation_id', $generation_id->id)
                    ->join('seskoad_admin.kurikulum_rpp as db2','exams.sbs_id','=','db2.id')
                    ->join('seskoad_admin.departement as db3','db2.kode_departemen','=','db3.kode')
                    ->select(['exams.name','exams.id','exams.deskripsi','exams.duration','exams.status',
                             'exams.start_date','exams.end_date','db2.name as sbs_name','db3.id as departement_id', 
                             'db3.name as dep_name', 'slug'])
                    ->orderBy('id', 'DESC');
                    
            if(in_array('pasis',$role)){
                $exam_pasis = ExamEnrolls::where('user_id',$request->header('auth'))->pluck('exam_id');
                $query = $query->whereIn('exams.id',$exam_pasis);
            }

            if($filterValue != 'all'){
                if($filterValue == 'will_begin'){
                    if(in_array('pasis',$role))
                    {
                        $exam_pasis_done = ExamEnrolls::where('user_id',$request->header('auth'))->where('status','done')->pluck('exam_id');
                        $exam_pasis_ulang = ExamEnrolls::where('user_id',$request->header('auth'))->whereIn('status',['repeat', 'repeat-progress'])->pluck('exam_id');
                        
                        $query = $query->whereNotIn('exams.id', $exam_pasis_done)->where(function($q) use ($exam_pasis_ulang) {
                            $q->where('status', '!=', 'done')
                                ->orWhereIn('exams.id', $exam_pasis_ulang);
                        });
                    }else{
                        $query = $query->where('status', 'none');
                    }
                }elseif($filterValue == 'missed'){
                    $exam_pasis_missed = ExamEnrolls::where('user_id',$request->header('auth'))->where('status','none')->pluck('exam_id');
                    $query = $query->whereIn('exams.id', $exam_pasis_missed)->where('end_date' ,'<', \Carbon\Carbon::parse(\Carbon\Carbon::now()->format('d-m-Y H:i:s')));
                }else{
                    if(in_array('pasis',$role)){
                        $exam_pasis_done = ExamEnrolls::where('user_id',$request->header('auth'))->where('status', 'done')->pluck('exam_id');
                        $exam_pasis_progress = ExamEnrolls::with('exam')
                                                ->where('user_id',$request->header('auth'))
                                                ->where('status', 'progress')
                                                ->whereHas('exam', function($q){
                                                    $q->where('status', 'done');
                                                })
                                                ->pluck('exam_id');

                        $status = $exam_pasis_done->merge($exam_pasis_progress);
                        $query = $query->whereIn('exams.id',$status);
                    }else{
                        $query = $query->where('status', $filterValue);
                    }
                }
            }
            
            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                    $query->where('exams.name', 'like', '%' . $searchValue . '%');
                 });
             }
             if($departement_id && $departement_id != 'semua' ){
                 $query = $query->where('db3.id', $departement_id);
             }
            $query = $query->paginate(8);
            return $this->sendResponse(200, \Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[ExamController index] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $cn = DB::connection('elearning');
        $client = new \GuzzleHttp\Client();

        try {
            $cn->beginTransaction();
            $input = $request->all()['exam'];
            $inputQuestion = $request->all()['questions'];

            $messages = [
                'sbs_id.required'=>trans('validation.required'),
                'name.required'=>trans('validation.required'),
                'name.max'=>trans('validation.max'),
                'enroll.required'=>trans('validation.required'),
                // 'question.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'sbs_id' => 'SBS',
                'name' => 'Nama Course',
                'enroll_all_student' => 'Enroll Pasis',
                // 'question' => 'Question',
            ]; 

            $validator = Validator::make($input, [
                'sbs_id'=> 'required', 
                'name'=> 'required|max:200', 
                'enroll'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            if(count($inputQuestion) == 0)
            {
                return $this->sendResponse(400, 'Validation Error.', (object) ['question' => ['Soal belum dipilih']]);
            }
           
            $exam = new Exams;
            $exam->name = $input['name'];
            $exam->slug = Str::slug($input['name'], '-').'-'.\Carbon\Carbon::now()->format('His');
            $exam->created_by = $request->header('auth');
            $exam->sbs_id = $input['sbs_id'];
            $exam->deskripsi = $input['catatan'] ? $input['catatan'] : '-';
            $exam->generation_id = 1;
            $exam->stage_id = 1;
            $exam->enrol = $input['enroll'];
            if($input['rentang_waktu']){
                $start_time = $input['start_time'];
                $end_time = $input['end_time'];
                $exam->start_date = \Carbon\Carbon::parse($input['start_date'].' '.$start_time)->format('Y-m-d H:i:s');
                $exam->end_date = \Carbon\Carbon::parse($input['end_date'].' '.$end_time)->format('Y-m-d H:i:s');
                $exam->duration = $input['duration'];
            }else{
                $exam->start_date = \Carbon\Carbon::parse($input['start_date'])->format('Y-m-d H:i:s');
                $exam->end_date = \Carbon\Carbon::parse($input['end_date'])->format('Y-m-d H:i:s');
                $exam->duration =  null; 
            }

            $enrol = strtolower($input['enroll']);
            $exam->save();
            
            if($enrol == 'semua pasis')
            {
                $params = [
                    'query' => [
                        'generation_id' => 1,
                        'role' => 'pasis'
                    ]
                ];
                $response = $client->request(
                        'GET', 
                        env("USERS_APPS_SERVICE_HOST").':'.env("USERS_APPS_SERVICE_PORT").'/api/users', 
                        $params);
                $users = json_decode($response->getBody())->data;
            }else{
                $params = [
                    'query' => [
                        'generation_id' => 1,
                        'jabatan' => 'pasis',
                        'role' => 'pasis'
                    ]
                ];
                $response = $client->request('GET', env("USERS_APPS_SERVICE_HOST").':'.env("USERS_APPS_SERVICE_PORT").'/api/users', $params);

                $users = json_decode($response->getBody())->data;
            }

            foreach($users as $user)
            {
                $enrol = new ExamEnrolls;
                $enrol->user_id = $user;
                $enrol->exam_id = $exam->id;
                $enrol->save();
            }

            foreach ($inputQuestion as $key => $value) {
                if(isset($value['type']))
                {
                    $kategori = QuestionCategory::where('name', strtolower($value['kategori']['name']))->first();

                    $qa = new Questions;
                    $qa->created_by = $request->header('auth');
                    $qa->category_id = $kategori->id;
                    $qa->mp_id = $value['mp_id'];
                    $qa->name = $value['name'];
                    $qa->file = null;
                    $qa->save();

                    if(isset($value['right_answer']) && count($value['right_answer']) > 0){
                        foreach ($value['right_answer'] as $key => $vs) {
                            $answer = new QuestionAnswers;
                            $answer->question_id = $qa->id;
                            $answer->answer =  $vs['answer'];
                            $answer->istrue = $vs['istrue'];
                            $answer->save();
                        }
                    }

                    $question = new ExamQuestions;
                    $question->question_name = $value['name'];
                    $question->categories_id = $kategori->id;
                    $question->exam_id = $exam->id; 
                    $question->mp_id = $value['mp_id'];
                    $question->serial_number = ($key+1);
                    $question->save();

                    if(isset($value['right_answer']) && count($value['right_answer']) > 0){
                        foreach ($value['right_answer'] as $key => $vs) {
                            $answer = new ExamQuestionAnswers;
                            $answer->answer =  $vs['answer'];
                            $answer->istrue = $vs['istrue'];
                            $answer->exam_question_id = $question->id;
                            $answer->save();
                        }
                    }
                }else{
                    $question = new ExamQuestions;
                    $question->question_name = $value['name'];
                    $question->categories_id = $value['category_id'];
                    $question->exam_id = $exam->id; 
                    $question->mp_id = $value['mp_id'];
                    $question->serial_number = ($key+1);
                    $question->save();

                    if(isset($value['right_answer']) && count($value['right_answer']) > 0){
                        foreach ($value['right_answer'] as $key => $vs) {
                            $answer = new ExamQuestionAnswers;
                            $answer->answer =  $vs['answer'];
                            $answer->istrue = $vs['istrue'];
                            $answer->exam_question_id = $question->id;
                            $answer->save();
                        }
                    }
                }
            }

          
            $cn->commit();
            return $this->sendResponse(200,trans('validation.created'),$exam);
        } catch(\Throwable $e) {
            $cn->rollBack();
            LogSystem::error('[Exams store] : '.$e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
       try {
        $exams = Exams::with('questions')
                         ->join('seskoad_admin.kurikulum_rpp as db2','exams.sbs_id','=','db2.id')
                         ->join('seskoad_admin.departement as db3','db2.kode_departemen','=','db3.kode')
                         ->select(['exams.name','exams.id','exams.deskripsi','exams.duration','exams.status','exams.start_date','exams.end_date','db2.name as sbs_name','db3.name as departement_name'])
                        ->find($id);
        
        if(!$exams){
            return $this->sendResponse(404, 'Validation Error.', ['ujian tidak ditemukan']);
        }

        // $exams->questions->transform(function($q) {
        //     $q->mp = KurikulumRpp::find($q->mp_id)->name;

        //     return $q;
        // });

        $exams->questions_group = $exams->questions->unique('mp_id')->values()->all();

        $exams['monitoring'] = [
            'selesai' => ExamEnrolls::where('exam_id',$id)->where('status', 'done')->count(),
            'belum_mulai' => ExamEnrolls::where('exam_id',$id)->where('status', 'none')->count()
        ];

        $time = \Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s');
        $start_date = \Carbon\Carbon::parse($exams->start_date);
        $end_date = \Carbon\Carbon::parse($exams->end_date);

        if($start_date <= \Carbon\Carbon::parse($time) && $end_date >= \Carbon\Carbon::parse($time))
        {
            $dur = 0;
            $upd = Exams::find($id);
            $upd->status = 'onprogress';
            $upd->save();
        }else{
            $dur = $start_date->diffInSeconds(\Carbon\Carbon::parse($time));
        }

        $exams->duration_left = $dur;

        return $this->sendResponse(200, trans('validation.success'), $exams);
       } catch (\Throwable $th) {
        return $this->sendResponse(500, $this->messageError(), $th->getMessage());
       }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Request $request, $id)
    {
        try {
            $exams = Exams::with('questions:id,exam_id,question_name as name,categories_id,serial_number,mp_id', 'questions.kategori', 'questions.rightAnswer')
                         ->join('seskoad_admin.kurikulum_rpp as db2','exams.sbs_id','=','db2.id')
                         ->join('seskoad_admin.departement as db3','db2.kode_departemen','=','db3.kode')
                         ->select(['exams.name','exams.id','exams.deskripsi','exams.duration','exams.status','exams.start_date','exams.end_date',
                                'db2.name as sbs_name','db2.id as sbs_id','db3.name as departement_name','db3.id as dep_id','enrol'])
                        ->find($id);

            return $this->sendResponse(200, trans('validation.success'), $exams);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam Edit] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $cn = DB::connection('elearning');
        try {
            $cn->beginTransaction();
            $input = $request->all()['exam'];
            $inputQuestion = $request->all()['questions'];

            $messages = [
                'sbs_id.required'=>trans('validation.required'),
                'name.required'=>trans('validation.required'),
                'name.max'=>trans('validation.max'),
                'enroll.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'sbs_id' => 'SBS',
                'name' => 'Nama Course',
                'enroll_all_student' => 'Enroll Pasis',
            ]; 

            $validator = Validator::make($input, [
                'sbs_id'=> 'required', 
                'name'=> 'required|max:200', 
                'enroll'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            if(count($inputQuestion) == 0)
            {
                return $this->sendResponse(400, 'Validation Error.', (object) ['question' => ['Soal belum dipilih']]);
            }
           
            $exam = Exams::find($id);

            if(!$exam){
                return $this->sendResponse(404, 'Validation Error.', ['ujian tidak ditemukan']);
            }

            $exam_old = Exams::find($id);
            $exam->name = $input['name'];
            $exam->slug = Str::slug($input['name'], '-').'-'.\Carbon\Carbon::parse(\Carbon\Carbon::now()->format('His'));
            $exam->created_by = $request->header('auth');
            $exam->sbs_id = $input['sbs_id'];
            $exam->enrol = $input['enroll'];
            $exam->deskripsi = $input['catatan'] ? $input['catatan'] : '-';

            if($input['rentang_waktu']){
                $start_time = $input['start_time'];
                $end_time = $input['end_time'];

                $exam->start_date = \Carbon\Carbon::parse($input['start_date'].' '.$start_time)->format('Y-m-d H:i:s');
                $exam->end_date = \Carbon\Carbon::parse($input['end_date'].' '.$end_time)->format('Y-m-d H:i:s');
                $exam->duration = $input['duration'];
            }else{
                $start_date = \Carbon\Carbon::parse($input['start_date'])->format('Y-m-d H:i:s');
                $close_date = \Carbon\Carbon::parse($input['end_date'])->format('Y-m-d H:i:s');
                $exam->duration =  null; 
            }

            $enrol = strtolower($input['enroll']);
            $exam->save();

            ExamEnrolls::where('exam_id', $id)->delete();

            $client = new \GuzzleHttp\Client();

            if($enrol == 'semua pasis')
            {
                $params = [
                    'query' => [
                        'generation_id' => 1,
                        'role' => 'pasis'
                    ]
                ];
                $response = $client->request('GET', env("USERS_APPS_SERVICE_HOST").':'.env("USERS_APPS_SERVICE_PORT").'/api/users', $params);
                $users = json_decode($response->getBody())->data;
            }else{
                $params = [
                    'query' => [
                        'generation_id' => 1,
                        'jabatan' => 'pasis',
                        'role' => 'pasis'
                    ]
                ];
                $response = $client->request('GET', env("USERS_APPS_SERVICE_HOST").':'.env("USERS_APPS_SERVICE_PORT").'/api/users', $params);
            }

            foreach($users as $user)
            {
                $enrol = new ExamEnrolls;
                $enrol->user_id = $user;
                $enrol->exam_id = $exam->id;
                $enrol->save();
            }

            $question_exist = collect($inputQuestion)->filter(function($val) {
                return !isset($value['type']);
            })->all();

            ExamQuestions::where('exam_id', $exam->id)
                        ->whereNotIn('id', collect($question_exist)->pluck('id')->toArray())
                        ->delete();
                        LogSystem::warning(collect($question_exist)->pluck('id')->toArray());

            foreach ($inputQuestion as $key => $value) {
                if(isset($value['type']))
                {
                    $kategori = QuestionCategory::where('name', strtolower($value['kategori']['name']))->first();

                    $qa = new Questions;
                    $qa->created_by = $request->header('auth');
                    $qa->category_id = $kategori->id;
                    $qa->mp_id = $value['mp_id'];
                    $qa->name = $value['name'];
                    $qa->file = null;
                    $qa->save();

                    if(isset($value['right_answer']) && count($value['right_answer']) > 0){
                        foreach ($value['right_answer'] as $key => $vs) {
                            $answer = new QuestionAnswers;
                            $answer->question_id = $qa->id;
                            $answer->answer =  $vs['answer'];
                            $answer->istrue = $vs['istrue'];
                            $answer->save();
                        }
                    }

                    $question = new ExamQuestions;
                    $question->question_name = $value['name'];
                    $question->categories_id = $kategori->id;
                    $question->exam_id = $exam->id; 
                    $question->mp_id = $value['mp_id'];
                    $question->serial_number = ($key+1);
                    $question->save();

                    if(isset($value['right_answer']) && count($value['right_answer']) > 0){
                        foreach ($value['right_answer'] as $key => $vs) {
                            $answer = new ExamQuestionAnswers;
                            $answer->answer =  $vs['answer'];
                            $answer->istrue = $vs['istrue'];
                            $answer->exam_question_id = $question->id;
                            $answer->save();
                        }
                    }
                }else{
                    $cek = ExamQuestions::where('question_name', $value['name'])->where('exam_id', $exam->id)->first();
                    if(!$cek)
                    {
                        $question = new ExamQuestions;
                        $question->question_name = $value['name'];
                        $question->categories_id = $value['category_id'];
                        $question->exam_id = $exam->id; 
                        $question->mp_id = $value['mp_id'];
                        $question->serial_number = ($key+1);
                        $question->save();
    
                        if(isset($value['right_answer']) && count($value['right_answer']) > 0){
                            foreach ($value['right_answer'] as $key => $vs) {
                                $answer = new ExamQuestionAnswers;
                                $answer->answer =  $vs['answer'];
                                $answer->istrue = $vs['istrue'];
                                $answer->exam_question_id = $question->id;
                                $answer->save();
                            }
                        }
                    }else{
                        $cek->serial_number = ($key+1);
                        $cek->save();
                    }
                }
            }

            $cn->commit();
            return $this->sendResponse(200,trans('validation.created'),$exam);
        } catch(\Throwable $e) {
            $cn->rollBack();
            LogSystem::error('[Exams update] : '.$e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $exam = Exams::find($id);
            if(!$exam) return $this->sendResponse(400, 'exams id not found', (object) ['exams' => ['Exams not found']]);

            try {
                $exam->delete();
            } catch (\Illuminate\Database\QueryException $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }
            return $this->sendResponse(200, trans('validation.deleted'), $exam);
        } catch (\Throwable $th) {
            LogSystem::error('[Exams destroy] : '.$th);

            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function preview(Request $request, $id)
    {
        try {
            $exam = Exams::with('questions.kategori', 'questions.answer')->find($id);

            return $this->sendResponse(200, trans('validation.success'), $exam);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam Preview] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function mulai(Request $request, $slug)
    {
        try {
            $exam = Exams::with('questions.kategori', 'questions.answer')->where('slug', $slug)->first();
            $exam->questions->transform(function($val) use ($exam) {
                $cek = ExamAnswer::where('exam_id', $exam->id)
                            ->where('question_id', $val->id)
                            ->where('user_id', $request->header('auth'))
                            ->first();
                if($cek && $cek->answer)
                {
                    $val->isDone = $cek->answer;
                }else{
                    $val->isDone = false;
                }

                if($cek && $cek->isMarked == 1)
                {
                    $val->isMarked = true;
                }else{
                    $val->isMarked = false;
                }
                $val->isChange = false;
                return $val;
            });

            $enrol = ExamEnrolls::where('exam_id', $exam->id)
                            ->where('user_id', $request->header('auth'))
                            ->first();

            if($exam->duration)
            {
                if($enrol)
                {
                    $endDate = \Carbon\Carbon::parse($enrol->created_at)->addMinutes($exam->duration);
                    $now = \Carbon\Carbon::parse(\Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s'));

                    if($now > \Carbon\Carbon::parse($endDate))
                    {
                        $totalDuration = 0;
                    }else{
                        $totalDuration = $endDate->diffInSeconds($now);
                        $totalDuration = $totalDuration / 60;
                    }

                    $exam->duration = $totalDuration;    
                }
            }else{
                if($enrol->status == 'repeat')
                {
                    $now = \Carbon\Carbon::parse(\Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s'));

                    $startDate = \Carbon\Carbon::parse($exam->start_date);
                    $endDate = \Carbon\Carbon::parse($exam->end_date);
                    $dur_ujian = $endDate->diffIndSeconds($startDate);

                    $end_date_user = \Carbon\Carbon::parse($enrol->created_at)->addSeconds($dur_ujian);
    
                    $totalDuration = $end_date_user->diffInSeconds($now) / 60;
                }else{
                    $now = \Carbon\Carbon::parse(\Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s'));
                    $endDate = \Carbon\Carbon::parse($exam->end_date);

                    $totalDuration = $endDate->diffInSeconds($now) / 60;
                }
                
                $exam->duration = $totalDuration;    
            }

            return $this->sendResponse(200, trans('validation.success'), $exam);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam mulai] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function storeAnswer(Request $request)
    {
        $exam = Exams::where('slug', $request->exam_slug)->first();
        try {
            $answer = ExamAnswer::where('exam_id', $exam->id)
                        ->where('question_id', $request->question_id)
                        ->where('user_id', $request->header('auth'))
                        ->first();

            if(!$answer)
            {
                $answer = new ExamAnswer;
                $answer->exam_id = $exam->id;
                $answer->question_id = $request->question_id;
                $answer->user_id = $request->header('auth');
            }

            $answer->answer = $request->answer;
            $answer->save();
            
            return $this->sendResponse(200, trans('validation.updated'), $answer);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam storeAnswer] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function storeTandai(Request $request)
    {
        $exam = Exams::where('slug', $request->exam_slug)->first();
        try {
            $answer = ExamAnswer::where('exam_id', $exam->id)
                        ->where('question_id', $request->question_id)
                        ->first();

            if(!$answer)
            {
                $answer = new ExamAnswer;
                $answer->answer = '';
                $answer->exam_id = $exam->id;
                $answer->question_id = $request->question_id;
            }

            $answer->isMarked = $request->is_marked ? 1 : 0;
            $answer->save();
            
            return $this->sendResponse(200, trans('validation.updated'), $answer);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam storeTandai] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function storeSelesai(Request $request)
    {
        $exam = Exams::where('slug', $request->exam_slug)->first();
        try {
            $enrol = ExamEnrolls::where('exam_id', $exam->id)
                        ->where('user_id', $request->header('auth'))
                        ->first();

            if(!$enrol)
            {
                $enrol = new ExamEnrolls;
                $enrol->exam_id = $exam->id;
                $enrol->user_id = $request->header('auth');
            }

            $enrol->status = 'done';
            $enrol->save();
            $cn = DB::connection('elearning');
            try {
                $cn->beginTransaction();
                $kategories = QuestionCategory::where('name','Esai')->first();
                $question = ExamQuestions::with('kategori')->where('exam_id',$exam->id)->where('categories_id',$kategories->id)->pluck('id');
                $answer =  ExamAnswer::where('exam_id', $exam->id)->whereIn('question_id',$question)
                ->where('user_id', $request->header('auth'))
                ->get();
                
                $question_key = GamificationPoint::where('key','question_complete')->first();
                $evaluasi = GamificationPoint::where('key','evaluasi_complete')->first();
                $point = (intval($question_key->point)*count($answer))+intval($evaluasi->point);
                $history = new GamificationHistoryUser;
                $history->value  = $point;
                $history->type  = 'point';
                $history->user_id  = $request->header('auth');
                $history->key  = 'evaluasi_complete';
                $history->is_show  = 1;
                $history->name  = 'Menyelesaikan '.$exam->name;
                $history->save();

                foreach ($answer as $key => $value) {
                    $history = new GamificationHistoryUser;
                    $history->value  = $question_key->point;
                    $history->type  = 'point';
                    $history->user_id  = $request->header('auth');
                    $history->key  = 'question_complete';
                    $history->is_show  = 0;
                    $history->name  = 'Mengerjakan soal essay';
                    $history->save();
                }

                $user = User::find($request->header('auth'));
                $user->point = $user->point+$history->point;
                $user->save();
                $cn->commit();
            } catch (\Throwable $th) {
                $cn->rollBack();
                LogSystem::error('[Exam Gamification] : '.$th);
                return $this->sendResponse(500, $this->messageError(), $th->getMessage());
            }
            
            return $this->sendResponse(200, trans('validation.updated'), $enrol);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam storeSelesai] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function updateStatusEnrol(Request $request)
    {
        try {
            $enrol = ExamEnrolls::where('exam_id', $request->id);

            if($request->user_id)
            {
                $enrol = $enrol->where('user_id', $request->user_id);
            }else{
                $enrol = $enrol->where('user_id', $request->header('auth'));
            }

            $enrol = $enrol->first();

            if(!$enrol)
            {
                $enrol = new ExamEnrolls;
                $enrol->exam_id = $request->id;
                if($request->user_id)
                {
                    $enrol->user_id = $request->user_id;
                }else{
                    $enrol->user_id = $request->header('auth');
                }
            }else if($enrol->status == 'repeat')
            {
                $request->status = 'repeat-progress';
                $enrol->created_at = \Carbon\Carbon::now()->timezone('Asia/Jakarta')->timestamp;

                LogSystem::warning($request->created_at);
            }else if($enrol->status == 'none')
            {
                $enrol->created_at = \Carbon\Carbon::now()->timezone('Asia/Jakarta')->timestamp;
            }

            $enrol->status = $request->status;
            $enrol->save();
            
            return $this->sendResponse(200, trans('validation.updated'), $enrol);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam storeTandai] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function listEnrol(Request $request, $id)
    {
        try {
            $searchValue = $request->input('search');
            $filter = $request->input('filter');

            $query = ExamEnrolls::select('users.id as user_id', 'exam_enrolls.id as id', 'users.name', 'users.nrp', 'status')
                        ->where('exam_id', $id)
                        ->join('seskoad_admin.users as users', 'users.id', 'exam_enrolls.user_id');

            $question = ExamQuestions::where('exam_id', $id)->get();
            
            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                    $query->where('users.name', 'like', '%' . $searchValue . '%')
                        ->orWhere('users.nrp', 'like', '%' . $searchValue . '%');
                });
            }

            if($filter == 'belum mulai')
            {
                $query->where('status', 'none');
            }else if($filter == 'berlangsung')
            {
                $query->where('status', 'progress');
            }else if($filter == 'selesai')
            {
                $query->where('status', 'done');
            }

            $query = $query->paginate(10);

            $query->transform(function($q) use ($question, $id) {
                // LogSystem::warning($q->user_id);
                $enrol = ExamAnswer::where('exam_id', $id)
                            ->whereIn('question_id', $question->pluck('id')->toArray())
                            ->where('user_id', $q->user_id)
                            ->where('answer', '!=', '')
                            ->get()
                            ->count();
                $q->progress = $enrol;
                $q->count_question = $question->count();
                return $q;
            });

            return $this->sendResponse(200, trans('validation.updated'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam listEnrol] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function timeLeft($id)
    {
        $exam = Exams::find($id);
        $time = \Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s');
        $start_date = \Carbon\Carbon::parse($exam->start_date);

        if($start_date <= \Carbon\Carbon::parse($time))
        {
            $dur = 0;
        }else{
            $dur = $start_date->diffInSeconds(\Carbon\Carbon::parse($time));
        }

        return response()->json($dur);
    }

    public function review(Request $request, $id)
    {
        try {
            $user_id = $request->user_id;

            $exam = ExamQuestions::with('kategori', 'answer')->where('exam_id', $id)->orderBy('serial_number', 'ASC')->get();

            $exam->transform(function($q) use ($id, $user_id) {
                $answer = ExamAnswer::where('question_id', $q->id)
                                ->where('exam_id', $id);

                if($user_id)
                {
                    $answer = $answer->where('user_id', $user_id);
                }else{
                    $answer = $answer->where('user_id', $request->header('auth'));
                }

                $answer = $answer->first();

                $q->answer_pasis = $answer ? $answer->answer : null;
                
                return $q;
            });

            if($exam->count() > 0)
            {
                $exam[0]->exam_description = Exams::find($id)->deskripsi;
            }

            return $this->sendResponse(200, trans('validation.success'), $exam);
        } catch (\Throwable $th) {
            LogSystem::error('[Exam review] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }

        
    }
}
