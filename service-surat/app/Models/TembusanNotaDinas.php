<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TembusanNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_tembusan_nota_dinas';
    protected $fillable = ['user_id' , 'id_nota_dinas', 'status_read'];


    public function user()
    {
        return $this->hasOne('App\Models\Auth\User', 'id', 'user_id');
    }
}
