<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use Illuminate\Support\Facades\Log as LogSystem;
use Modules\Elearning\Entities\Courses;
use Modules\Elearning\Entities\CoursesContent;
use Modules\Elearning\Entities\CourseContentQuestion;
use ZanySoft\Zip\Zip;
use Modules\Elibrary\Entities\MasterEbook;
use DB;
use Modules\Elearning\Entities\ContentUserProgress;
use Modules\Elearning\Entities\GamificationPoint;
use Modules\Elearning\Entities\GamificationHistoryUser;
use App\Models\User;

class ContentsController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, $course_id)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');
            
            $query = CoursesContent::with('questions.question.rightAnswer')
                    ->where('course_id', $course_id)
                    ->orderBy('order', 'ASC');


            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                    $query->where('name', 'like', '%' . $searchValue . '%')
                            ->orWhere('type', 'like', '%' . $searchValue . '%');
                });
            }

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) {
                $cek = ContentUserProgress::where('content_id', $q->id)
                        ->where('user_id', \Auth::user()->id)
                        ->first();
                $q->status = $cek ? $cek->status : 'none';
                return $q;
            });

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('elearning::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $cn = DB::connection('elearning');

        try {
            $cn->beginTransaction();

            $input = $request->all();
            $messages = [
                'name.required'=>trans('validation.required'),
                'type.required'=>trans('validation.required'),
                'complete_by.required'=>trans('validation.required'),
                'name.max'=>trans('validation.max'),
            ];

            $niceNames = [
                'name' => 'Nama Konten',
                'complete_by' => 'Cara Penyelesaikan',
            ]; 

            $validator = Validator::make($request->all(), [
                'name'=> 'required|max:200', 
                'type'=> 'required', 
                'complete_by'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $content = new CoursesContent;
            $content->course_id = $input['course_id'];
            $content->created_by = \Auth::user()->id;
            $content->name = $input['name'];
            $content->type = $input['type'];
            $content->complete_by = explode('cbx_', $input['complete_by'])[1];

            $order = CoursesContent::where('course_id', $input['course_id'])->orderBy('order', 'DESC')->first();
            $content->order = $order ? ($order->order+1) : 1;

            if($input['type'] == 'heading')
            {
                $content->complete_by = 'checkbox';
            }else if($input['type'] == 'scorm')
            {
                if(!$input['file'] || $input['file'] == 'undefined')
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                }

                $folder_name = \Carbon\Carbon::now()->timestamp.'_'.$input['course_id'];
                $path = public_path()."/assets/admin/elearning/content/scorm/".$folder_name;

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $input['file']->move($path,'materi.zip');
                
                $zip = Zip::open($path.'/materi.zip');
                $zip->extract($path);
                $zip->close();

                $content->file = $folder_name;

                if($path.'/materi.zip')
                {
                    unlink($path.'/materi.zip');
                }
            }else if($input['type'] == 'pdf')
            {
                if(!$request->hasFile('file'))
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                }

                $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                $path = public_path()."/assets/admin/elearning/content/pdf/".$input['course_id'];

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $input['file']->move($path,$file_name);
                $content->file = $file_name;
            }else if($input['type'] == 'video')
            {
                if(!$request->hasFile('file') && !$input['url_video'])
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File atau url video harus diisi']]);
                }

                if($request->hasFile('file')) {
                    $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                    $path = public_path()."/assets/admin/elearning/content/video/".$input['course_id'];
    
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
    
                    $input['file']->move($path,$file_name);
                    $content->file = $file_name;
                }else{
                    $content->file = $input['url_video'];
                }
            }else if($input['type'] == 'audio')
            {
                if(!$request->hasFile('file'))
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                }

                $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                $path = public_path()."/assets/admin/elearning/content/audio/".$input['course_id'];

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $input['file']->move($path,$file_name);
                $content->file = $file_name;
            }else if($input['type'] == 'esai')
            {
                if(!$input['file'])
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                }
                $content->file = $input['file'];
            }else if($input['type'] == 'ebook')
            {
                if(!$input['file'])
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['Ebook harus diisi']]);
                }
                $content->file = $input['file'];
            }

            $content->save();

            if($content->complete_by == 'question')
            {
                $soals = explode(',', $input['soal']);
                foreach($soals as $soal)
                {
                    $question = new CourseContentQuestion;
                    $question->content_id = $content->id;
                    $question->question_id = $soal;
                    $question->save();
                }
            }

            insert_log([
                'model' => $content,
                'action' => 'created',
                'data' => $content->name.'('.$content->type.')'
            ]);

            $cn->commit();
            return $this->sendResponse(200,trans('validation.created'),$content);
        } catch (\Throwable $e) {
            LogSystem::error('[Contents store] : '.$e);
            $cn->rollBack();
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    public function storeProgress(Request $request)
    {
        $cn = DB::connection('elearning');

        try {
            $point = false;

            $cek = ContentUserProgress::where('content_id', $request->content_id)
                    ->where('user_id', \Auth::user()->id)
                    ->first();
            if(!$cek)
            {
                $progress = new ContentUserProgress;
                $progress->content_id = $request->content_id;
                $progress->user_id = \Auth::user()->id;
                $progress->status = $request->status;
                $progress->save();
                
                $content = CoursesContent::with('course')->find($request->content_id);
                
                insert_log([
                    'model' => $progress,
                    'action' => 'completed',
                    'data' => 'Menyelesaikan materi '.$content->name.' pada course : '.$content->course->name
                ]);

                // store poin
                try {
                    $cn->beginTransaction();

                    $cekGm = GamificationPoint::where('key', 'materi_complete')
                                ->where('is_active', 1)
                                ->first();

                    $updPoint = User::find(\Auth::user()->id);
                    $updPoint->point = $updPoint->point + $cekGm->point;
                    $updPoint->save();

                    $history = new GamificationHistoryUser;
                    $history->user_id = \Auth::user()->id;
                    $history->name = 'Menyelesaikan materi '.$content->name.' pada course '.$content->course->name;
                    $history->type = 'point';
                    $history->value = $cekGm->point;
                    $history->key = $cekGm->key;
                    $history->save();

                    $point = $cekGm->point;

                    if($request->last_content == 'true')
                    {
                        $cc = CoursesContent::where('course_id', $content->course->id)->where('type', '!=', 'heading')->count();
                        $history2 = new GamificationHistoryUser;
                        $history2->user_id = \Auth::user()->id;
                        $history2->name = 'Menyelesaikan course bernama '.$content->course->name;
                        $history2->type = 'point';
                        $history2->value = $cekGm->point * $cc;
                        $history2->key = 'mp_complete';
                        $history2->is_show = '1';
                        $history2->save();
                    }

                    $cn->commit();
                } catch (\Throwable $th) {
                    $cn->rollBack();
                    LogSystem::error('[Contents storeprogress gamification] : '.$th);
                }
            }else{
                $cek->status = $request->status;
                $cek->save();

                $content = $cek;
            }
            
            $status = $request->status;
            return $this->sendResponse(200,trans('validation.success'),compact('content', 'status', 'point'));
        } catch (\Throwable $th) {
            LogSystem::error('[Contents storeprogress] : '.$e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try {
            $query = CoursesContent::find($id);
            $query->url_video = '';
            if($query->type == 'ebook')
            {
                $query->ebook = MasterEbook::find($query->file);
            }

            if($query->complete_by == 'question')
            {
                $query->questions = CourseContentQuestion::where('content_id', $query->id)->get()->pluck('question_id');
            }else{
                $query->questions = [];
            }

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('elearning::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $cn = DB::connection('elearning');

        try {
            $cn->beginTransaction();

            $input = $request->all();
            $messages = [
                'name.required'=>trans('validation.required'),
                'type.required'=>trans('validation.required'),
                'complete_by.required'=>trans('validation.required'),
                'name.max'=>trans('validation.max'),
            ];

            $niceNames = [
                'name' => 'Nama Konten',
                'complete_by' => 'Cara Penyelesaikan',
            ]; 

            $validator = Validator::make($request->all(), [
                'name'=> 'required|max:200', 
                'type'=> 'required', 
                'complete_by'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $content_old = CoursesContent::find($id);
            $content = CoursesContent::find($id);
            $content->name = $input['name'];
            $content->type = $input['type'];
            $content->complete_by = explode('cbx_', $input['complete_by'])[1];

            if($content->complete_by == 'time')
            {
                $content->time = $input['time'];
            }else{
                $content->time = null;
            }

            if($input['type'] == 'heading')
            {
                $content->name = $input['name'];
            }else if($input['type'] == 'scorm')
            {
                if(!$input['file'] || $input['file'] == 'undefined')
                {
                    return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                }
                
                if($input['old_file'] != $input['file'])
                {
                    //delete old data
                    $this->deleteOldFile($input);

                    $folder_name = \Carbon\Carbon::now()->timestamp.'_'.$input['course_id'];
                    $path = public_path()."/assets/admin/elearning/content/scorm/".$folder_name;
    
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
    
                    $input['file']->move($path,'materi.zip');
                    
                    $zip = Zip::open($path.'/materi.zip');
                    $zip->extract($path);
                    $zip->close();
    
                    $content->file = $folder_name;
    
                    if($path.'/materi.zip')
                    {
                        unlink($path.'/materi.zip');
                    }
                }
            }else if($input['type'] == 'pdf')
            {
                if($input['old_type'] != $input['type'])
                {
                    if(!$request->hasFile('file'))
                    {
                        return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                    }
                }

                if($input['old_file'] != $input['file']) {
                    if($request->hasFile('file'))
                    {
                        //delete old data
                        $this->deleteOldFile($input);

                        $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                        $path = public_path()."/assets/admin/elearning/content/pdf/".$input['course_id'];
        
                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
        
                        $input['file']->move($path,$file_name);
                        $content->file = $file_name;
                    }
                }
            }else if($input['type'] == 'video')
            {
                if($input['old_type'] != $input['type'])
                {
                    if(!$request->hasFile('file') && !$input['url_video'])
                    {
                        return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File atau url video harus diisi']]);
                    }
                }

                $isNewFile = false;

                if($input['url_video'])
                {
                    if($input['old_file'] != $input['url_video']) {
                        $isNewFile = true;
                    }
                }else{
                    if($input['old_file'] != $input['file']) {
                        $isNewFile = true;
                    }
                }

                if($isNewFile) {
                    //delete old data
                    $this->deleteOldFile($input);

                    if($request->hasFile('file'))
                    {
                        $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                        $path = public_path()."/assets/admin/elearning/content/video/".$input['course_id'];
        
                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
        
                        $input['file']->move($path,$file_name);
                        $content->file = $file_name;
                    }else{
                        $content->file = $input['url_video'];
                    }
                }
            }else if($input['type'] == 'audio')
            {
                if($input['old_type'] != $input['type'])
                {
                    if(!$request->hasFile('file'))
                    {
                        return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                    }
                }

                if($input['old_file'] != $input['file']) {
                    //delete old data
                    $this->deleteOldFile($input);

                    $file_name = \Carbon\Carbon::now()->timestamp.'_'.str_replace('_','-', str_replace(' ', '-', $input['file']->getClientOriginalName()));
                    $path = public_path()."/assets/admin/elearning/content/audio/".$input['course_id'];
    
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
    
                    $input['file']->move($path,$file_name);
                    $content->file = $file_name;
                }
            }else if($input['type'] == 'esai')
            {
                if($input['old_type'] != $input['type'])
                {
                    if(!$input['file'])
                    {
                        return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['File harus diisi']]);
                    }

                    $this->deleteOldFile($input);
                }
                $content->file = $input['file'];
            }else if($input['type'] == 'ebook')
            {
                if($input['old_type'] != $input['type'])
                {
                    if(!$input['file'])
                    {
                        return $this->sendResponse(400, 'Validation Error.', (object) ['file' => ['Ebook harus diisi']]);
                    }

                    $this->deleteOldFile($input);
                }
                $content->file = $input['file'];
            }

            $content->save();

            if($content->complete_by == 'question')
            {
                $soals = explode(',', $input['soal']);

                CourseContentQuestion::where('content_id', $content->id)
                            ->whereNotIn('question_id', $soals)
                            ->delete();
                foreach($soals as $soal)
                {
                    $cek = CourseContentQuestion::where('content_id', $content->id)->where('question_id', $soal)->first();
                    if(!$cek)
                    {
                        $question = new CourseContentQuestion;
                        $question->content_id = $content->id;
                        $question->question_id = $soal;
                        $question->save();
                    }
                }
            }

            insert_log([
                'model' => $content,
                'action' => 'modified',
                'parameter' => $content_old
            ]);

            $cn->commit();
            return $this->sendResponse(200,trans('validation.updated'),$content);
        } catch (\Throwable $e) {
            LogSystem::error('[Contents store] : '.$e);
            $cn->rollBack();
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    public function deleteOldFile($input)
    {
        try {
            if($input['old_type'] == 'scorm')
            {
                $old_file = public_path()."/assets/admin/elearning/content/".$input['old_type']."/".$input['old_file'];

                
            }else if($input['old_type'] == 'pdf' || $input['old_type'] == 'video' || $input['old_type'] == 'audio')
            {
                $old_file = public_path()."/assets/admin/elearning/content/".$input['old_type']."/".$input['course_id']."/".$input['old_file'];
            }

            if(file_exists($old_file))
            {
                try {
                    deleteDirectory($old_file);
                } catch (\Throwable $th) {
                    LogSystem::error('[Contents deleteDirectory] : '.$th);
                }
            }
        } catch (\Throwable $th) {
            LogSystem::error('[Contents update delete oldfile] : '.$th);
        }
    }

    public function orderContent(Request $request)
    {
        try {
            foreach($request->data as $k => $val)
            {
                $content = CoursesContent::find($val['id']);
                $content->order = ($k+1);
                $content->save();
            }

            return $this->sendResponse(200,trans('validation.updated'), '');
        } catch (\Throwable $e) {
            LogSystem::error('[Contents order] : '.$e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $content = CoursesContent::find($id);
            if(!$content) return $this->sendResponse(400, 'Content not found', (object) ['content' => ['Content not found']]);

            try {
                $content->delete();

                if($content->type == 'scorm')
                {
                    $old_file = public_path()."/assets/admin/elearning/content/".$content->type."/".$content->file;

                    if(file_exists($old_file))
                    {
                        LogSystem::error($old_file);

                        try {
                            deleteDirectory($old_file);
                        } catch (\Throwable $th) {
                            LogSystem::error('[Contents deleteDirectory] : '.$th);
                        }
                    }
                }else if($content->type == 'pdf' || $content->type == 'video' || $content->type == 'audio')
                {
                    $old_file = public_path()."/assets/admin/elearning/content/".$content->type."/".$content->course_id."/".$content->file;

                    if(file_exists($old_file))
                    {
                        try {
                            unlink($old_file);
                        } catch (\Throwable $th) {
                            LogSystem::error('[Contents deleteDirectory] : '.$th);
                        }
                    }
                }

                insert_log([
                    'model' => $content,
                    'action' => 'deleted',
                    'parameter' => $content
                ]);
            } catch (\Illuminate\Database\QueryException $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }
           
            return $this->sendResponse(200,trans('validation.deleted'),$content);
        } catch (\Throwable $e) {
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }
}
