<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckUserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user_login = session('user_login');

        if(!$user_login)
        {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', env("USERS_APPS_SERVICE_HOST").':'.env("USERS_APPS_SERVICE_PORT").'/api/users/'
                                .$request->header('auth'));

            $user = json_decode($response->getBody())->data;
            session(['user_login' => json_encode($user)]);
        }
        

        return $next($request);
    }
}
