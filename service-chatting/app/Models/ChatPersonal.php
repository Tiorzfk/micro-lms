<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ChatPersonal extends Model
{
    use HasFactory;

    public $label = 'Chat Personal';

    protected $connection = 'ams';
    protected $table = 'chat_personal';
    protected $fillable = [
        'user_id_sender',
        'user_id_receiver',
        'message',
        'file',
        'status'
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\ChatPersonalFactory::new();
    }
}
