<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use Modules\Admin\Entities\KurikulumRpp;
use Modules\Admin\Entities\Departement;
use Modules\Elearning\Entities\Courses;
use Modules\Elearning\Entities\CoursesContent;
use Illuminate\Support\Facades\Log as LogSystem;
use Illuminate\Support\Str;
use Modules\Elearning\Entities\ContentUserProgress;
use Modules\Elearning\Entities\CoursesUsers;
use App\Models\User;

class CoursesController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $length = $request->input('length');
            $departemen_id = $request->input('departemen_id');
            $sbs_id = $request->input('sbs_id');
            $searchValue = $request->input('search');
            $noContent = $request->input('no_content');

            $query = Courses::select(
                            'courses.id as id',
                            'courses.name as name',
                            'mp_id',
                            'description',
                            'active',
                            'ns',
                            'by_system',
                            'enrol_all_student',
                            'users.id as user_id',
                            'users.name as created_by',
                            'kurikulum_rpp.name as mp_name',
                            'slug',
                        )
                        ->join('seskoad_admin.users', 'users.id', 'seskoad_elearning.courses.created_by')
                        ->join('seskoad_admin.kurikulum_rpp', 'kurikulum_rpp.id', 'seskoad_elearning.courses.mp_id')
                        ->orderBy('id', 'DESC');
            
            if(!$noContent || $noContent == 'false')
            {
                $query->with('contents');
            }

            if(in_array('view mulai course', getMyPermission()))
            {
                $query->has('contents')->join('courses_users as cu', 'cu.course_id', 'courses.id')
                                        ->where('cu.user_id', \Auth::user()->id);
            }
            
            if($departemen_id)
            {
                $dep = Departement::find($departemen_id);
                $sbs = KurikulumRpp::where('kode_departemen', $dep->kode)
                                        ->where('kurikulum_skep_id', get_angkatan_aktif()->kurikulum_skep_id)
                                        ->get()
                                        ->pluck('id')
                                        ->toArray();
                $mp = KurikulumRpp::whereIn('parent_id', $sbs)
                                    ->get()
                                    ->pluck('id')
                                    ->toArray();
                $query = $query->whereIn('mp_id', $mp);
            }

            if($sbs_id)
            {
                $mp = KurikulumRpp::where('parent_id', $sbs_id)
                                    ->get()
                                    ->pluck('id')
                                    ->toArray();
                $query = $query->whereIn('mp_id', $mp);
            }

            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                    $query = $query->whereHas('mp', function($query2) use ($searchValue) {
                                $query2->Where('name', 'like', '%' . $searchValue . '%');
                                $query2->orWhere('kode', 'like', '%' . $searchValue . '%');
                                $query2->orWhere('keterangan', 'like', '%' . $searchValue . '%');
                            })
                            ->orWhere('name', 'like', '%' . $searchValue . '%')
                            ->orWhere('description', 'like', '%' . $searchValue . '%');
                });
            }

            $query = $query->paginate($length ? $length : 100);

            return $this->sendResponse(200, trans('validation.success'), $query);

        } catch (\Throwable $th) {
            LogSystem::error('[Courses index] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('elearning::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'mp_id.required'=>trans('validation.required'),
                'name.required'=>trans('validation.required'),
                'name.max'=>trans('validation.max'),
                'isActive.required'=>trans('validation.required'),
                'enroll.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'mp_id' => 'Mata Pelajaran',
                'name' => 'Nama Course',
                'isActive' => 'Status',
                'enroll_all_student' => 'Enroll Pasis',
            ]; 

            $validator = Validator::make($request->all(), [
                'mp_id'=> 'required', 
                'name'=> 'required|max:200', 
                'isActive'=> 'required', 
                'enroll'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }
           
            $course = new Courses;
            $course->name = $input['name'];
            $course->slug = Str::slug($input['name'], '-');
            $course->created_by = \Auth::user()->id;
            $course->mp_id = $input['mp_id'];
            $course->description = $input['deskripsi'] ? $input['deskripsi'] : '-';
            $course->active = $input['isActive'] ? 1 : 0;
            $course->by_system = 0;
            $enrol = strtolower($input['enroll']);
            $course->enrol_all_student = $enrol == 'semua pasis' ? 1 : 0;
            $course->save();

            if($enrol == 'semua pasis')
            {
                $users = User::select('id')
                            ->role('pasis')
                            ->where('generation_id', get_angkatan_aktif()->id)
                            ->get()
                            ->pluck('id')
                            ->toArray();
            }else{
                $users = User::select('id')
                            ->role('pasis')
                            ->where('jabatan', '!=', 'pasis mancanegara')
                            ->where('generation_id', get_angkatan_aktif()->id)
                            ->get()
                            ->pluck('id')
                            ->toArray();
            }

            foreach($users as $user)
            {
                $enrol = new CoursesUsers;
                $enrol->user_id = $user;
                $enrol->course_id = $course->id;
                $enrol->save();
            }

            insert_log([
                'model' => $course,
                'action' => 'created',
                'data' => $course->name
            ]);
            return $this->sendResponse(200,trans('validation.created'),$course);
        } catch(\Throwable $e) {
            LogSystem::error('[Courses store] : '.$e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Request $request, $id)
    {
        try {
            $query = Courses::select(
                        'courses.id as id',
                        'courses.name as name',
                        'mp_id',
                        'description',
                        'active',
                        'ns',
                        'by_system',
                        'enrol_all_student',
                        'users.id as user_id',
                        'users.name as created_by',
                        'kurikulum_rpp.name as mp_name',
                    )              
                    ->join('seskoad_admin.users', 'users.id', 'seskoad_elearning.courses.created_by')
                    ->join('seskoad_admin.kurikulum_rpp', 'kurikulum_rpp.id', 'seskoad_elearning.courses.mp_id')
                    ->with('contents')
                    ->find($id);

            if($request->showProgress == 'true')
            {
                $contents_id = $query->contents->pluck('id');
                $contents_length = CoursesContent::where('type', '!=', 'heading')
                                    ->whereIn('id', $contents_id)
                                    ->get()
                                    ->pluck('id');

                $progress = ContentUserProgress::where('user_id', \Auth::user()->id)
                                ->whereIn('content_id', $contents_length->toArray())
                                ->count();

                $query->progress = $progress.'/'.$contents_length->count();
                $query->progress_precentage = round($progress / $contents_length->count() * 100, 2).' %';

                $query->contents->transform(function($q) {
                    $cek = ContentUserProgress::where('content_id', $q->id)
                            ->where('user_id', \Auth::user()->id)
                            ->first();
                    $q->status = $cek ? $cek->status : 'none';
                    return $q;
                });

                if($progress > 0)
                {
                    $query->isContinue = $progress;
                }else{
                    $query->isContinue = false;
                }
            }

            return $this->sendResponse(200, trans('validation.success'), $query);

        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('elearning::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'mp_id.required'=>trans('validation.required'),
                'name.required'=>trans('validation.required'),
                'name.max'=>trans('validation.max'),
                'active.required'=>trans('validation.required'),
                'enrol_all_student.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'mp_id' => 'Mata Pelajaran',
                'name' => 'Nama Course',
                'active' => 'Status',
                'enrol_all_student' => 'Enroll Pasis',
            ]; 

            $validator = Validator::make($request->all(), [
                'mp_id'=> 'required', 
                'name'=> 'required|max:200', 
                'active'=> 'required', 
                'enrol_all_student'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }
           
            $course_old = Courses::find($id);
            $course = Courses::find($id);

            $course->name = $input['name'];
            $course->slug = Str::slug($input['name'], '-');
            $course->created_by = \Auth::user()->id;
            $course->mp_id = $input['mp_id'];
            $course->description = $input['description'] ? $input['description'] : '-';
            $course->active = $input['active'] ? 1 : 0;
            $course->by_system = 0;
            $enrol = $input['enrol_all_student'];
            $course->enrol_all_student = $enrol ? 1 : 0;
            $course->save();

            if($enrol == 'semua pasis')
            {
                $users = User::select('id')
                            ->role('pasis')
                            ->where('generation_id', get_angkatan_aktif()->id)
                            ->get()
                            ->pluck('id')
                            ->toArray();
            }else{
                $users = User::select('id')
                            ->role('pasis')
                            ->where('jabatan', '!=', 'pasis mancanegara')
                            ->where('generation_id', get_angkatan_aktif()->id)
                            ->get()
                            ->pluck('id')
                            ->toArray();
            }

            CoursesUsers::where('course_id', $course->id)->delete();

            foreach($users as $user)
            {
                $enrol = new CoursesUsers;
                $enrol->user_id = $user;
                $enrol->course_id = $course->id;
                $enrol->save();
            }

            insert_log([
                'model' => $course,
                'action' => 'modified',
                'parameter' => $course_old
            ]);

            return $this->sendResponse(200,trans('validation.updated'),$course);
        } catch(\Throwable $e) {
            LogSystem::error('[Courses store] : '.$e);
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    public function uploadNs(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'id.required'=>trans('validation.required'),
                'file.required'=>trans('validation.required'),
                'file.max'=>trans('validation.max'),
                'file.mimes'=>trans('validation.mimes'),
            ];

            $niceNames = [
                'file' => 'File Naskah',
            ]; 

            $validator = Validator::make($request->all(), [
                'id'=> 'required', 
                'file'=> 'required|max:100000|mimes:pdf,doc,docx', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }
            
            $course = Courses::find($input['id']);

            $name_file = $input['file']->getClientOriginalName();
            $path = public_path()."/assets/admin/elearning/ns";

            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            if($course->ns)
            {
                if (file_exists($path.'/'.$course->ns)) {
                    unlink($path.'/'.$course->ns);
                }
            }

            $input['file']->move($path,$name_file);

            $course->ns = $name_file;
            $course->save();

            return $this->sendResponse(200, trans('validation.created'), $course);
        } catch (\Throwable $th) {
            LogSystem::error('[Courses uploadNs] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $course = Courses::find($id);
            if(!$course) return $this->sendResponse(400, 'Course not found', (object) ['course' => ['Course not found']]);

            try {
                $course->delete();
                insert_log([
                    'model' => $course,
                    'action' => 'deleted',
                    'parameter' => $course
                ]);
            } catch (\Illuminate\Database\QueryException $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }
           
            return $this->sendResponse(200,trans('validation.deleted'),$course);
        } catch (\Throwable $e) {
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }

    public function enrol(Request $request)
    {
        CoursesUsers::where('course_id', $request->course_id)->delete();

        if($request->isPns)
        {
            $users_pns = User::select('id')
                        ->role('pasis')
                        ->where('jabatan', 'pasis mancanegara')
                        ->where('generation_id', get_angkatan_aktif()->id)
                        ->get()
                        ->pluck('id')
                        ->toArray();

            $users = User::select('id')
                        ->role('pasis')
                        ->whereNotIn('id', $users_pns)
                        ->where('generation_id', get_angkatan_aktif()->id)
                        ->get()
                        ->pluck('id')
                        ->toArray();

        }else{
            $users = $request->users;
        }

        foreach($users as $user)
        {
            $enrol = new CoursesUsers;
            $enrol->user_id = $user;
            $enrol->course_id = $request->course_id;
            $enrol->save();
        }

        return $this->sendResponse(200,trans('validation.updated'),$users);
    }

    public function report(Request $request, $id)
    {
        try {
            $pasis_mengikuti = CoursesUsers::where('course_id', $id)
                        ->get()
                        ->pluck('user_id');

            $contents = CoursesContent::where('course_id', $id)
                            ->where('type', '!=', 'heading')
                            ->get();


            $pasis_progress = ContentUserProgress::select('user_id', \DB::raw('count(user_id) as total'))
                                    ->whereIn('content_id', $contents->pluck('id')->toArray())
                                    ->whereIn('user_id', $pasis_mengikuti->toArray())
                                    ->groupBy('user_id')
                                    ->get();

            $pasis_menyelesaikan = $pasis_progress->filter(function($val) use ($contents) {
                return $val->total == $contents->count();
            })->count();

            $pasis_menjalankan = $pasis_progress->filter(function($val) use ($contents) {
                return $val->total < $contents->count();
            })->count();

            $result = [
                'pasis_mengikuti' => $pasis_mengikuti->count(),
                'pasis_menjalankan' => $pasis_menjalankan,
                'pasis_menyelesaikan' => $pasis_menyelesaikan
            ];

            return $this->sendResponse(200, trans('validation.success'), $result);
        } catch (\Throwable $th) {
            LogSystem::error('[Courses report] : '.$th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }
}
