<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'App\Http\Controllers\LoginController@login');
Route::post('/{id}/my-roles', 'App\Http\Controllers\LoginController@myRoles');
Route::get('/users', 'App\Http\Controllers\LoginController@getUsers');
Route::get('/users/{id}', 'App\Http\Controllers\LoginController@getDetailUser');
Route::post('/refresh_tokens', 'App\Http\Controllers\LoginController@refreshTokens');
