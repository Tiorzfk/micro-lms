/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoau_penilaian

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 17/08/2021 17:48:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for penilaian_akademik
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_akademik`;
CREATE TABLE `penilaian_akademik`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `penugasan_id` int(11) NULL DEFAULT NULL COMMENT 'relasi dengan tabel penugasan, module ams',
  `siswa_data_kelompok_id` int(11) NULL DEFAULT NULL,
  `tahap` int(1) NULL DEFAULT NULL COMMENT 'tahapan',
  `sekolah_angkatan_id` int(11) NULL DEFAULT NULL,
  `bs_id` int(11) NULL DEFAULT NULL,
  `sbs_id` int(11) NULL DEFAULT NULL,
  `penilaian_form_kbk` int(11) NULL DEFAULT NULL,
  `penilaian_form_kme` int(11) NULL DEFAULT NULL,
  `penilaian_form_kbe` int(11) NULL DEFAULT NULL,
  `judul_latihan` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tanggal_latihan` date NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `departemen_kode` int(45) NULL DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `batas_penilaian` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 431 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_aspek_akademik
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_aspek_akademik`;
CREATE TABLE `penilaian_aspek_akademik`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penilaian_form_akademik_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT 'default parent 0',
  `nomor` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'bisa digit/alphabet',
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1 = Ada Input Nilai, 0 = Tidak ada',
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 599 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_aspek_akademik_kategori
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_aspek_akademik_kategori`;
CREATE TABLE `penilaian_aspek_akademik_kategori`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `penilaian_form_akademik_id` int(11) NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nilai` float NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_detail_akademik
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_detail_akademik`;
CREATE TABLE `penilaian_detail_akademik`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `penilaian_akademik_id` int(11) NULL DEFAULT NULL,
  `penilaian_aspek_akademik_id` int(11) NULL DEFAULT NULL,
  `data_patun_id` int(11) NULL DEFAULT NULL,
  `siswa_id` int(11) NULL DEFAULT NULL,
  `nilai` float NULL DEFAULT NULL,
  `keterangan` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 276167 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_form_akademik
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_form_akademik`;
CREATE TABLE `penilaian_form_akademik`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kurikulum_skep_juknis_id` int(11) NULL DEFAULT NULL,
  `form` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama_singkatan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tipe` enum('kbk','kme','kbe') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Tipe format penilaian',
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_kategori
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_kategori`;
CREATE TABLE `penilaian_kategori`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kurikulum_skep_juknis_id` int(11) NULL DEFAULT NULL,
  `nama` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nilai_min` int(3) NULL DEFAULT NULL,
  `nilai_max` int(3) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
