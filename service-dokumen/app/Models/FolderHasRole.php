<?php

namespace Modules\DMS\Entities;

use Illuminate\Database\Eloquent\Model;

class FolderHasRole extends Model
{
    protected $table = 'dms_folder_has_role';

    protected $fillable = ['id_folder', 'id_role'];

    public function role()
    {
        return $this->hasOne('\App\Models\Auth\User', 'id', 'id_role');
    }
}
