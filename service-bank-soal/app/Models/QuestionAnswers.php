<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class QuestionAnswers extends Model
{
    use HasFactory;

    protected $table = 'question_answer';
    protected $fillable = ['question_id','answer','file','istrue'];
    protected $appends = ['is_right'];
    
    public function getIsRightAttribute()
    {
        if($this->istrue == 1){
            return true;
        }else{
            return false;
        }
    }

  
}
