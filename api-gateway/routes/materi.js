
const express = require('express');
const router = express.Router();

const materiHandler = require('./handler/materi');
const verifyToken = require('../middlewares/verifyToken');

router.post('/courses', materiHandler.getCourse);
router.get('/', verifyToken, materiHandler.getMateri);

router.post('/', verifyToken, materiHandler.addMateri);
router.put('/:id', verifyToken, materiHandler.editMateri);
router.delete('/:id', verifyToken, materiHandler.deleteMateri);

module.exports = router;
