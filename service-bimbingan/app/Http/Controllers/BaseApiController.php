<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class BaseApiController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($status, $message, $result = null)
    {
    	$response = [
            'status'  => $status,
            'message' => $message,
            'result'  => $result
        ];

        if($response['message'] == 'Validation Error.')
        {
            $response['message'] = trans('validation.wrong_input');
        }

        return response()->json($response, $response['status']);
    }

    public function messageError()
    {
        return trans('validation.error_internal');
    }

    public function messageConstraintError()
    {
        return trans('validation.error_constraint');
    }

    public function messageConstraintErrorUpdate()
    {
        return trans('validation.error_constraint_update');
    }
}
