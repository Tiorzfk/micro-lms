/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoau_penilaian

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 17/08/2021 17:48:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for penilaian_akademik_dak
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_akademik_dak`;
CREATE TABLE `penilaian_akademik_dak`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sekolah_angkatan_id` int(11) NOT NULL,
  `penilaian_akademik_id` int(11) NULL DEFAULT NULL,
  `tahap` int(1) NOT NULL,
  `judul_latihan` varchar(145) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `bs_id` int(11) NULL DEFAULT NULL,
  `sbs_id` int(11) NULL DEFAULT NULL,
  `penilaian_form_kbk` int(11) NULL DEFAULT NULL,
  `penilaian_form_kme` int(11) NULL DEFAULT NULL,
  `penilaian_form_kbe` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `departemen_kode` int(11) NULL DEFAULT NULL,
  `batas_penilaian` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_akademik_dak_detail
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_akademik_dak_detail`;
CREATE TABLE `penilaian_akademik_dak_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penilaian_akademik_dak_id` int(11) NULL DEFAULT NULL,
  `siswa_data_kelompok_id` int(11) NULL DEFAULT NULL,
  `penilaian_aspek_akademik_id` int(11) NULL DEFAULT NULL,
  `data_patun_id` int(11) NULL DEFAULT NULL,
  `data_dosen_id` int(11) NULL DEFAULT NULL,
  `siswa_id` int(11) NULL DEFAULT NULL,
  `nilai` float NULL DEFAULT NULL,
  `keterangan` varchar(145) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118357 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_akademik_dak_dosen
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_akademik_dak_dosen`;
CREATE TABLE `penilaian_akademik_dak_dosen`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penilaian_akademik_dak_kelompok_id` int(11) NOT NULL,
  `data_dosen_id` int(11) NULL DEFAULT NULL,
  `data_patun_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 191 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for penilaian_akademik_dak_kelompok
-- ----------------------------
DROP TABLE IF EXISTS `penilaian_akademik_dak_kelompok`;
CREATE TABLE `penilaian_akademik_dak_kelompok`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nomor` int(2) NULL DEFAULT NULL,
  `penilaian_akademik_dak_id` int(11) NOT NULL,
  `siswa_data_kelompok_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_b` int(11) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 377 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
