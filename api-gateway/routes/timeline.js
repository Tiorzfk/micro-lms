
const express = require('express');
const router = express.Router();

const timelineHandler = require('./handler/timeline');
const verifyToken = require('../middlewares/verifyToken');

router.post('/', timelineHandler.getTimeline);
router.get('/', verifyToken, timelineHandler.getTimeline);

module.exports = router;
