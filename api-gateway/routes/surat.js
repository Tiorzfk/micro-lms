
const express = require('express');
const router = express.Router();

const suratHandler = require('./handler/surat');
const verifyToken = require('../middlewares/verifyToken');

router.post('/', suratHandler.addSurat);
router.get('/', verifyToken, suratHandler.getSurat);

module.exports = router;
