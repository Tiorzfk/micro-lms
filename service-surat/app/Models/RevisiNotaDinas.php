<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RevisiNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_revisi_nota_dinas';
    protected $fillable = ['id_nota_dinas' , 'revisi_surat' , 'catatan' , 'suer_revisi'];

    public function perbaikan()
    {
        return $this->hasOne('Modules\DMS\Entities\PerbaikanNotaDinas', 'id_revisi', 'id');
    }

    public function lampiran()
    {
        return $this->hasMany('Modules\DMS\Entities\RevisiLampiranNotaDinas', 'id_revisi', 'id');
    }
}
