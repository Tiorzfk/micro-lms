/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoad_ams

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 17/08/2021 17:16:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assignment_guidance
-- ----------------------------
DROP TABLE IF EXISTS `assignment_guidance`;
CREATE TABLE `assignment_guidance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_naskah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_lc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_revisi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` enum('pending','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `notes_for_pasis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_dosen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `submission_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_guidance_submission_id_index`(`submission_id`) USING BTREE,
  CONSTRAINT `assignment_guidance_submission_id_foreign` FOREIGN KEY (`submission_id`) REFERENCES `assignment_submission_guidance` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_personal_mentor
-- ----------------------------
DROP TABLE IF EXISTS `assignment_personal_mentor`;
CREATE TABLE `assignment_personal_mentor`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `assignment_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `mentor_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_personal_mentor_assignment_id_index`(`assignment_id`) USING BTREE,
  INDEX `assignment_personal_mentor_user_id_index`(`user_id`) USING BTREE,
  INDEX `assignment_personal_mentor_mentor_id_index`(`mentor_id`) USING BTREE,
  CONSTRAINT `assignment_personal_mentor_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignment_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_mentor_mentor_id_foreign` FOREIGN KEY (`mentor_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `assignment_personal_mentor_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for assignment_submission_guidance
-- ----------------------------
DROP TABLE IF EXISTS `assignment_submission_guidance`;
CREATE TABLE `assignment_submission_guidance`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` time(0) NOT NULL,
  `end_time` time(0) NOT NULL,
  `type` enum('online','offline') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'online',
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_dosen` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `notes_for_pasis` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `status` enum('pending','approved','canceled','rejected','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `personal_mentor_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assignment_submission_guidance_personal_mentor_id_index`(`personal_mentor_id`) USING BTREE,
  CONSTRAINT `assignment_submission_guidance_personal_mentor_id_foreign` FOREIGN KEY (`personal_mentor_id`) REFERENCES `assignment_personal_mentor` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
