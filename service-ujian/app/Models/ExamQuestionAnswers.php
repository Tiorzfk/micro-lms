<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExamQuestionAnswers extends Model
{
    use HasFactory;
   
    protected $connection = 'elearning';
    protected $table = 'exam_question_answers';
    protected $fillable = ['exam_question_id','answer','istrue'];
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\ExamQuestionAnswersFactory::new();
    }
}
