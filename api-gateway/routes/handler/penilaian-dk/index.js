const getNilai = require('./getNilai');
const simpanNilai = require('./simpanNilai');

module.exports = {
    getNilai,
    simpanNilai,
};