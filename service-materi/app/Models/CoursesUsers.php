<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CoursesUsers extends Model
{
    use HasFactory;

    public $label = 'Course User';

    protected $connection = 'elearning';
    protected $table = 'courses_users';

    protected $fillable = ['user_id', 'course_id'];
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\CoursesUsersFactory::new();
    }
}
