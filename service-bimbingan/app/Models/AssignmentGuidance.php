<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentGuidance extends Model
{
    use HasFactory;

    public $label = 'Pengajuan Bimbingan';

    protected $connection = 'ams';
    protected $table = 'assignment_guidance';
    protected $fillable = [
        'file_naskah',
        'file_lc',
        'file_revisi',
        'status',
        'notes_for_pasis',
        'notes_for_dosen',
        'submission_id',
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentSubmissionGuidanceFactory::new();
    }
}
