<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Carbon\Carbon;
use Auth;
use App\Models\ExamEnrolls;

class Exams extends Model
{
    use HasFactory;
    protected $connection = 'elearning';
    protected $table = 'exams';
    protected $fillable = ['generation_id','stage_id','created_by','sbs_id','status','name','deskripsi','duration','start_date','end_date','image','enrol'];
    protected $appends = ['date_format','detail_time','time_format','status_value','status_mulai','duration_exam'];
    
    protected static function newFactory()
    {
        return \Modules\Elearning\Database\factories\ExamsFactory::new();
    }
    public function getTimeFormatAttribute()
    {
        $start =  date('H:i',strtotime($this->start_date));
        $end = date('H:i',strtotime($this->end_date));
        return $start. ' - '. $end;
    }
    public function getStatusValueAttribute()
    {
        if(in_array('pasis',auth_login()->roles_name)){
            $auth = ExamEnrolls::where('user_id',auth_login()->id)->where('exam_id', $this->id)->where('status','none')->first();
            $auth_progress = ExamEnrolls::where('user_id',auth_login()->id)->where('exam_id', $this->id)->where('status','progress')->first();
            $auth_done = ExamEnrolls::where('user_id',auth_login()->id)->where('exam_id', $this->id)->where('status','done')->first();
            $auth_ulang = ExamEnrolls::where('user_id',auth_login()->id)->where('exam_id', $this->id)->whereIn('status',['repeat', 'repeat-progress'])->first();

            if($auth && $this->status == 'done'){
                return 'missed';
            }else if($auth_progress && $this->status != 'done'){
                return true;
            }else if($auth_ulang) {
                return true;
            }else if($auth_done) {
                return 'review';
            }else if($auth) {
                return true;
            }else{
                return $this->status;
            }
        }else{
            return $this->status;
        }
        
    }
    public function getDetailTimeAttribute()
    {
        return get_date($this->start_date, true).' s/d '.get_date($this->end_date, true);
    }
    public function getDateFormatAttribute()
    {   
        $date = date('l',strtotime($this->start_date));
        $day = getDay($date).', '.get_date($this->start_date, false);
        return $day;
    }

    public function getStatusMulaiAttribute()
    {
        $auth_ulang = ExamEnrolls::where('user_id',auth_login()->id)->where('exam_id', $this->id)->whereIn('status',['repeat', 'repeat-progress'])->first();

        if($auth_ulang)
        {
            return true;
        }else if(\Carbon\Carbon::parse($this->start_date) <= \Carbon\Carbon::parse(\Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s')) && 
            \Carbon\Carbon::parse($this->end_date) >= \Carbon\Carbon::parse(\Carbon\Carbon::now()->timezone('Asia/Jakarta')->format('Y-m-d H:i:s'))){
            return true;
        }else{
            return false;
        }
    }

    public function getDurationExamAttribute()
    {
        if($this->duration)
        {
            $duration = $this->duration;
        }else{
            $start_date = \Carbon\Carbon::parse($this->start_date);
            $end_date = \Carbon\Carbon::parse($this->end_date);

            $duration = $end_date->diffInMinutes($start_date);
        }

        return $duration;
    }

    public function enrolls()
    {
        return $this->hasMany('\Modules\Elearning\Entities\ExamEnrolls','exam_id', 'id');
    }

    public function questions()
    {
        return $this->hasMany('\App\Models\ExamQuestions','exam_id', 'id')->orderBy('serial_number', 'ASC');
    }
}
