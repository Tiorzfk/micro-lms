
const express = require('express');
const router = express.Router();

const penugasanHandler = require('./handler/penugasan');
const verifyToken = require('../middlewares/verifyToken');

router.get('/personal', verifyToken, penugasanHandler.getPersonal);
router.get('/group', verifyToken, penugasanHandler.getGroup);

router.post('/personal', verifyToken, penugasanHandler.addPersonal);
router.post('/group', verifyToken, penugasanHandler.addGroup);

router.post('/group/upload-tugas', verifyToken, penugasanHandler.uploadTugasPersonal);
router.post('/personal/upload-tugas', verifyToken, penugasanHandler.uploadTugasGroup);

module.exports = router;
