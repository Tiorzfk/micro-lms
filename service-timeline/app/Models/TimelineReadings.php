<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimelineReadings extends Model
{
    use HasFactory;
    protected $connection = 'ams';
    protected $table = 'timeline_readings';
    protected $fillable = ['user_id','timeline_id','is_reading'];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\TimelineReadingsFactory::new();
    }
}
