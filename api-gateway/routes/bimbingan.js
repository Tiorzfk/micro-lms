
const express = require('express');
const router = express.Router();

const handler = require('./handler/bimbingan');
const verifyToken = require('../middlewares/verifyToken');

router.get('/', verifyToken, handler.getBimbingan);

router.post('/:id/set', verifyToken, handler.setPembimbing);
router.post('/:id/ajukan', verifyToken, handler.ajukanBimbingan);
router.put('/:id/status', verifyToken, handler.editStatus);
router.post('/:id/upload-naskah', verifyToken, handler.uploadNaskah);
router.post('/:id/upload-revisi-naskah/:bimbingan_id', verifyToken, handler.uploadRevisiNaskah);

module.exports = router;
