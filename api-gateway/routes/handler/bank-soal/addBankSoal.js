const apiAdapter = require('../../apiAdapter');
const {
  BANK_SOAL_APPS_SERVICE_HOST,
  BANK_SOAL_APPS_SERVICE_PORT,
} = process.env;

const api = apiAdapter(BANK_SOAL_APPS_SERVICE_HOST, BANK_SOAL_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });

    const question = await api.post('/questions', req.body);
    const data = question.data.data;

    return res.json({
      status: 'success',
      data: data
    });

  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}