<?php

namespace Modules\Ams\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use DB;
use Illuminate\Support\Facades\Log as LogSystem;
use Modules\Ams\Entities\ChatPeople;
use Modules\Ams\Entities\ChatPersonal;
use Modules\Ams\Entities\ChatGroup;
use Modules\Ams\Entities\ChatGroupRead;
use Modules\Admin\Entities\UserGroup;

// use ElephantIO\Client as Elephant;
// use ElephantIO\Engine\SocketIO\Version2X;
// use Workerman\Worker;
// use PHPSocketIO\SocketIO;

class ChatController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');
            $user_id_sender = $request->input('user_id_sender');
            $user_id_receiver = $request->input('user_id_receiver');

            $query = ChatPersonal::select('chat_personal.id as id', 'users_receiver.name as name_receiver', 
                        'users_receiver.nrp as nrp_receiver', 'users_sender.nrp as nrp_sender',
                        'users_sender.name as name_sender', 'users_sender.avatar as avatar_sender', 
                        'users_receiver.avatar as avatar_receiver', 'users_receiver.id as user_id_receiver',
                        'users_sender.id as user_id_sender', 'chat_personal.message', 'chat_personal.created_at')
                        ->join('seskoad_admin.users as users_receiver', 'users_receiver.id', 'chat_personal.user_id_receiver')
                        ->join('seskoad_admin.users as users_sender', 'users_sender.id', 'chat_personal.user_id_sender')
                        ->where(function($q) use ($user_id_receiver, $user_id_sender) {
                            $q->where('chat_personal.user_id_sender', $user_id_sender)
                                ->where('chat_personal.user_id_receiver', $user_id_receiver);
                        })
                        ->orWhere(function($q) use ($user_id_receiver, $user_id_sender) {
                            $q->where('chat_personal.user_id_sender', $user_id_receiver)
                                ->where('chat_personal.user_id_receiver', $user_id_sender);
                        })
                        ->orderBy('chat_personal.id', 'DESC');

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) {
                $q->status = 'sent';
                $created_at = \Carbon\Carbon::parse($q->created_at);
                $now = \Carbon\Carbon::now()->format('Y-m-d');

                if($now == $created_at->format('Y-m-d'))
                {
                    $q->time = \Carbon\Carbon::parse($q->created_at)->format('H:i');
                }else{
                    $q->time = \Carbon\Carbon::parse($q->created_at)->format('d M y, H:i');
                }
                return $q;
            });

            $upd = ChatPersonal::whereIn('id', $query->pluck('id')->toArray())
                        ->where('user_id_receiver', \Auth::user()->id)
                        ->update(['status' => 'read']);

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat index] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function indexGroup(Request $request)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');
            $group_id = $request->input('group_id');

            $query = ChatGroup::select('chat_group.id as id', 'users_sender.nrp as nrp_sender',
                        'users_sender.name as name_sender', 'users_sender.avatar as avatar_sender', 
                        'users_sender.id as user_id_sender', 'chat_group.message', 'chat_group.created_at')
                        ->join('seskoad_admin.users as users_sender', 'users_sender.id', 'chat_group.user_id_sender')
                        ->where('group_id', $group_id)
                        ->orderBy('chat_group.id', 'DESC');

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) {
                $q->status = 'sent';
                $created_at = \Carbon\Carbon::parse($q->created_at);
                $now = \Carbon\Carbon::now()->format('Y-m-d');

                if($now == $created_at->format('Y-m-d'))
                {
                    $q->time = \Carbon\Carbon::parse($q->created_at)->format('H:i');
                }else{
                    $q->time = \Carbon\Carbon::parse($q->created_at)->format('d M y, H:i');
                }
                return $q;
            });

            $upd = ChatGroupRead::where('group_id', $group_id)
                        ->where('user_id', '!=', \Auth::user()->id)
                        ->update(['status' => 'read']);

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat indexGroup] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'user_id_sender.required'=>trans('validation.required'),
                'user_id_receiver.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'user_id_sender' => 'User Pengirim',
                'user_id_receiver' => 'User Penerima',
            ]; 

            $validator = Validator::make($input, [
                'user_id_sender'=> 'required', 
                'user_id_receiver'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $chat = new ChatPersonal;
            $chat->user_id_sender = $input['user_id_sender'];
            $chat->user_id_receiver = $input['user_id_receiver'];
            $chat->message = $input['message'];
            $chat->save();

            $response = ChatPersonal::select('chat_personal.id as id', 'users_receiver.name as name_receiver', 
                        'users_receiver.nrp as nrp_receiver', 'users_sender.nrp as nrp_sender',
                        'users_sender.name as name_sender', 'users_sender.avatar as avatar_sender', 
                        'users_receiver.avatar as avatar_receiver', 'users_receiver.id as user_id_receiver',
                        'users_sender.id as user_id_sender', 'chat_personal.message', DB::raw('TIME_FORMAT(chat_personal.created_at, "%H:%i") as time'))
                        ->join('seskoad_admin.users as users_receiver', 'users_receiver.id', 'chat_personal.user_id_receiver')
                        ->join('seskoad_admin.users as users_sender', 'users_sender.id', 'chat_personal.user_id_sender')
                        ->find($chat->id);

            return $this->sendResponse(200, trans('validation.created'), $response);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat store] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function storeChatGroup(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'user_id_sender.required'=>trans('validation.required'),
                'group_id.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'user_id_sender' => 'User Pengirim',
                'group_id' => 'Group',
            ]; 

            $validator = Validator::make($input, [
                'user_id_sender'=> 'required', 
                'group_id'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $chat = new ChatGroup;
            $chat->user_id_sender = $input['user_id_sender'];
            $chat->group_id = $input['group_id'];
            $chat->message = $input['message'];
            $chat->save();

            $read = ChatGroupRead::where('group_id', $input['group_id'])
                                    ->where('user_id', $input['user_id_sender'])
                                    ->first();
            if(!$read)
            {
                $read = new ChatGroupRead;
                $read->group_id = $input['group_id'];
                $read->user_id = $input['user_id_sender'];
                $read->status = 'new';
            }else{
                $read->status = 'new';
            }
            $read->save();

            $response = ChatGroup::select('chat_group.id as id', 'users_sender.nrp as nrp_sender',
                        'users_sender.name as name_sender', 'users_sender.avatar as avatar_sender', 
                        'users_sender.id as user_id_sender', 'chat_group.message', DB::raw('TIME_FORMAT(chat_group.created_at, "%H:%i") as time'))
                        ->join('seskoad_admin.users as users_sender', 'users_sender.id', 'chat_group.user_id_sender')
                        ->find($chat->id);

            return $this->sendResponse(200, trans('validation.created'), $response);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat store] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function listFriend(Request $request)
    {
        try {
            $length = $request->input('length');
            $searchValue = $request->input('search');

            $query = ChatPeople::select('users.id as id', 'users.name as name', 'users.islogin', 'users.nrp as nrp', 'chat_people.user_id as user_id', 'users.avatar',
                        'chat_people.user_id_friend as user_id_friend')
                        ->join('seskoad_admin.users as users', 'users.id', 'chat_people.user_id_friend')
                        ->where('user_id', \Auth::user()->id)
                        ->orderBy('chat_people.id', 'DESC');

            if ($searchValue) {
                $query = $query->where(function($query) use ($searchValue) {
                      $query->where('users.name', 'like', '%' . $searchValue . '%');
                    //   $query->orwhere('email', 'like', '%' . $searchValue . '%');
                });
            }

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) {
                $chat = ChatPersonal::where(function($w) use ($q) {
                                        $w->where('chat_personal.user_id_sender', \Auth::user()->id)
                                            ->where('chat_personal.user_id_receiver', $q->user_id_friend);
                                    })
                                    ->orWhere(function($w) use ($q) {
                                        $w->where('chat_personal.user_id_sender', $q->user_id_friend)
                                            ->where('chat_personal.user_id_receiver', \Auth::user()->id);
                                    })
                                    ->orderBy('id', 'DESC')
                                    ->first();
                
                $q->read = true;
                if($chat) {
                    $q->message = $chat->message;
                    $q->status = $chat->status; //sent,pending,null;

                    $created_at = \Carbon\Carbon::parse($chat->created_at);
                    $now = \Carbon\Carbon::now()->format('Y-m-d');

                    if($now == $created_at->format('Y-m-d'))
                    {
                        $q->time = \Carbon\Carbon::parse($chat->created_at)->format('H:i');
                    }else{
                        $q->time = \Carbon\Carbon::parse($chat->created_at)->format('d M y, H:i');
                    }
                }else{
                    $q->time = '';
                    $q->message = '';
                    $q->status = null; //sent,pending,null;
                }
                $chat_notif = ChatPersonal::where('user_id_receiver', \Auth::user()->id)
                                ->where('user_id_sender', $q->user_id_friend)
                                ->where('status', 'sent')
                                ->get()
                                ->count();

                $q->isOnline = $q->islogin == 1 ? true : false;
                $q->notif = $chat_notif;
                return $q;
            });

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat listFriend] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function storeFriend(Request $request)
    {
        try {
            $input = $request->all();
            $messages = [
                'user_id_friend.required'=>trans('validation.required'),
            ];

            $niceNames = [
                'user_id_friend' => 'User',
            ]; 

            $validator = Validator::make($input, [
                'user_id_friend'=> 'required', 
            ] , $messages, $niceNames);

            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $cek = ChatPeople::where('user_id', \Auth::user()->id)
                                ->where('user_id_friend', $input['user_id_friend'])
                                ->first();
            if(!$cek) {
                $friend = new ChatPeople;
                $friend->user_id = \Auth::user()->id;
                $friend->user_id_friend = $input['user_id_friend'];
                $friend->save();
            }

            $cek2 = ChatPeople::where('user_id', $input['user_id_friend'])
                                ->where('user_id_friend', \Auth::user()->id)
                                ->first();
            if(!$cek2) {
                $friend = new ChatPeople;
                $friend->user_id = $input['user_id_friend'];
                $friend->user_id_friend = \Auth::user()->id;
                $friend->save();
            }

            return $this->sendResponse(200,trans('validation.created'),$cek);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat storeFriend] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function listGroup(Request $request)
    {
        try {
            $length = $request->input('length');

            $query = UserGroup::select('user_groups.id', 'groups.name', 'groups.id as group_id', 'stages.name as tahap')
                    ->join('seskoad_admin.groups as groups', 'groups.id', 'seskoad_admin.user_groups.group_id')
                    ->join('seskoad_admin.stages as stages', 'stages.id', 'seskoad_admin.user_groups.stage_id')
                    ->where('user_id', \Auth::user()->id);

            $query = $query->paginate($length ? $length : 100);

            $query->transform(function($q) {
                $chat = ChatGroup::where('group_id', $q->group_id)
                                        ->orderBy('id', 'DESC')
                                        ->first();

                $jml_user = UserGroup::where('group_id', $q->group_id)->get()->count();
                $read = ChatGroupRead::where('group_id', $q->group_id)->get()->count();

                $q->read = $jml_user == $read ? true : false;

                if($chat) {
                    $q->message = $chat->message;
                    $q->status = 'sent'; //sent,pending,null;

                    $created_at = \Carbon\Carbon::parse($chat->created_at);
                    $now = \Carbon\Carbon::now()->format('Y-m-d');

                    if($now == $created_at->format('Y-m-d'))
                    {
                        $q->time = \Carbon\Carbon::parse($chat->created_at)->format('H:i');
                    }else{
                        $q->time = \Carbon\Carbon::parse($chat->created_at)->format('d M y, H:i');
                    }
                }else{
                    $q->time = '';
                    $q->message = '';
                    $q->status = null;
                }

                $q->jml_user_online = UserGroup::with('user:id,name,isLogin')
                                        ->where('group_id', $q->group_id)
                                        ->whereHas('user', function($q) {
                                            $q->where('islogin', 1);
                                        })
                                        ->get()
                                        ->count();

                return $q;
            });

            return $this->sendResponse(200, trans('validation.success'), $query);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat listGroup] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function notif(Request $request)
    {
        try {
            $chat_personal = ChatPersonal::where('user_id_receiver', \Auth::user()->id)
                                ->where('status', 'sent')
                                ->get()
                                ->count();

            $chat_personal_detail = ChatPersonal::select(DB::raw('COUNT(user_id_sender) as total_notif'), 'user_id_sender')
                                ->where('user_id_receiver', \Auth::user()->id)
                                ->where('status', 'sent')
                                ->groupBy('user_id_sender')
                                ->get();

            $chat_group = ChatGroupRead::where('user_id', '!=', \Auth::user()->id)
                                    ->where('status', 'new')
                                    ->get()
                                    ->count();

            return $this->sendResponse(200, trans('validation.success'), compact('chat_personal', 'chat_group', 'chat_personal_detail'));
        } catch (\Throwable $th) {
            LogSystem::error('[Chat notif] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function read(Request $request)
    {
        try {
            $read = ChatPersonal::where('user_id_receiver', $request->user_id_sender)
                            ->where('user_id_sender', $request->user_id_receiver)
                            ->orderBy('id', 'DESC')
                            ->first();
            $read->status = 'read';
            $read->save();
            return $this->sendResponse(200, trans('validation.success'), $read);
        } catch (\Throwable $th) {
            LogSystem::error('[Chat read] : ' . $th);
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('ams::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('ams::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Request $request)
    {
        try {
            $chat = ChatPeople::where('user_id', $request->user_id)
                            ->where('user_id_friend', $request->user_id_friend)
                            ->first();
            if(!$chat) return $this->sendResponse(400, 'Chat not found', (object) ['chat' => ['chat not found']]);

            try {
                $chat->delete();

                // ChatPersonal::where(function($q) use ($chat) {
                //     $q->where('user_id_sender', $chat->user_id)
                //         ->where('user_id_receiver', $chat->user_id_friend);
                // })
                // ->orWhere(function($q) use ($chat) {
                //     $q->where('user_id_receiver', $chat->user_id)
                //         ->where('user_id_sender', $chat->user_id_friend);
                // })
                // ->delete();
            } catch (\Illuminate\Database\QueryException $e) {
                return $this->sendResponse(
                    400, 
                    $this->messageConstraintError(), 
                    (object)["name" => [$this->messageConstraintError()]]
                );
            }
           
            return $this->sendResponse(200,trans('validation.deleted'),$chat);
        } catch (\Throwable $e) {
            return $this->sendResponse(500, $this->messageError(), $e->getMessage());
        }
    }
}
