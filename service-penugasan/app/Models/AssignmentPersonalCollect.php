<?php

namespace Modules\Ams\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Log as LogSystem;

class AssignmentPersonalCollect extends Model
{
    use HasFactory;

    public $label = 'Pengumpulan Tugas Personal';

    protected $connection = 'ams';
    protected $table = 'assignment_personal_collect';
    protected $fillable = [
        'assignment_id',
        'user_id',
        'file',
    ];
    protected $appends = ['file_url'];

    public function getFileUrlAttribute() {
        if($this->file){
            $url = config('app.url').'/assets/admin/ams/tugas-personal/'.$this->file;

            if(!file_exists(public_path().'/assets/admin/ams/tugas-personal/'.$this->file))
            {
                return null;
            }
            
            return $url;
        }else{
            return null;
        }
    }
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentPersonalCollectFactory::new();
    }
}
