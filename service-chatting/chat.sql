/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoad_ams

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 07/08/2021 07:54:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat_group
-- ----------------------------
DROP TABLE IF EXISTS `chat_group`;
CREATE TABLE `chat_group`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id_sender` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_group_user_id_sender_index`(`user_id_sender`) USING BTREE,
  INDEX `chat_group_group_id_index`(`group_id`) USING BTREE,
  CONSTRAINT `chat_group_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_group_user_id_sender_foreign` FOREIGN KEY (`user_id_sender`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_group_read
-- ----------------------------
DROP TABLE IF EXISTS `chat_group_read`;
CREATE TABLE `chat_group_read`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('new','read') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_group_read_group_id_index`(`group_id`) USING BTREE,
  INDEX `chat_group_read_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `chat_group_read_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `seskoad_admin`.`groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_group_read_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_people
-- ----------------------------
DROP TABLE IF EXISTS `chat_people`;
CREATE TABLE `chat_people`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_id_friend` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_people_user_id_index`(`user_id`) USING BTREE,
  INDEX `chat_people_user_id_friend_index`(`user_id_friend`) USING BTREE,
  CONSTRAINT `chat_people_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_people_user_id_friend_foreign` FOREIGN KEY (`user_id_friend`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chat_personal
-- ----------------------------
DROP TABLE IF EXISTS `chat_personal`;
CREATE TABLE `chat_personal`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id_sender` bigint(20) UNSIGNED NOT NULL,
  `user_id_receiver` bigint(20) UNSIGNED NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` enum('sent','read') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'sent',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status_read` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chat_personal_user_id_sender_index`(`user_id_sender`) USING BTREE,
  INDEX `chat_personal_user_id_receiver_index`(`user_id_receiver`) USING BTREE,
  CONSTRAINT `chat_personal_user_id_receiver_foreign` FOREIGN KEY (`user_id_receiver`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_personal_user_id_sender_foreign` FOREIGN KEY (`user_id_sender`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
