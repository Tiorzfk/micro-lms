/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : alins_sesko

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 01/08/2021 08:39:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dms_lampiran_nota_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dms_lampiran_nota_dinas`;
CREATE TABLE `dms_lampiran_nota_dinas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_nota_dinas` int(11) NOT NULL,
  `lampiran` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dms_nota_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dms_nota_dinas`;
CREATE TABLE `dms_nota_dinas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `perihal` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `surat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('dicek','revisi','perbaikan','terkirim','legalisir') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `dicek_oleh` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 190 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dms_nota_dinas_revisi_lampiran
-- ----------------------------
DROP TABLE IF EXISTS `dms_nota_dinas_revisi_lampiran`;
CREATE TABLE `dms_nota_dinas_revisi_lampiran`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_revisi` int(11) NOT NULL,
  `id_lampiran` int(11) NOT NULL,
  `lampiran_revisi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dms_perbaikan_nota_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dms_perbaikan_nota_dinas`;
CREATE TABLE `dms_perbaikan_nota_dinas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_nota_dinas` int(11) NOT NULL,
  `perbaikan_surat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `id_revisi` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dms_revisi_nota_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dms_revisi_nota_dinas`;
CREATE TABLE `dms_revisi_nota_dinas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_nota_dinas` int(11) NOT NULL,
  `surat_revisi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `catatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `user_revisi` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dms_tembusan_nota_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dms_tembusan_nota_dinas`;
CREATE TABLE `dms_tembusan_nota_dinas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `id_nota_dinas` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dms_tujuan_nota_dinas
-- ----------------------------
DROP TABLE IF EXISTS `dms_tujuan_nota_dinas`;
CREATE TABLE `dms_tujuan_nota_dinas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `jenis` enum('Tujuan','Tembusan') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_nota_dinas` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 648 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
