<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TujuanNotaDinas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'dms_tujuan_nota_dinas';
    protected $fillable = ['user_id' , 'id_nota_dinas' , 'tipe', 'status_read', 'is_sistem'];

    public function user()
    {
        return $this->hasOne('App\Models\Auth\UserLite', 'id', 'user_id');
    }

    public function notadinas()
    {
        return $this->hasOne('Modules\DMS\Entities\NotaDinas', 'id', 'id_nota_dinas');
    }

}
