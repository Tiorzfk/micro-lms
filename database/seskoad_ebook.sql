/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoad_ebook

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 23/08/2021 18:52:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ebook_last_seen
-- ----------------------------
DROP TABLE IF EXISTS `ebook_last_seen`;
CREATE TABLE `ebook_last_seen`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `ebook_id` bigint(20) UNSIGNED NOT NULL,
  `last_page` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ebook_last_seen_user_id_index`(`user_id`) USING BTREE,
  INDEX `ebook_last_seen_ebook_id_index`(`ebook_id`) USING BTREE,
  CONSTRAINT `ebook_last_seen_ebook_id_foreign` FOREIGN KEY (`ebook_id`) REFERENCES `master_ebook` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ebook_last_seen_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ebook_last_seen
-- ----------------------------
INSERT INTO `ebook_last_seen` VALUES (1, 1, 7, 1, '2021-07-05 08:51:53', '2021-07-09 03:56:50');
INSERT INTO `ebook_last_seen` VALUES (2, 1, 10, 9, '2021-07-05 09:17:50', '2021-07-05 10:56:00');
INSERT INTO `ebook_last_seen` VALUES (3, 1, 2, 3, '2021-07-05 09:33:19', '2021-07-09 04:46:53');
INSERT INTO `ebook_last_seen` VALUES (4, 1, 8, 7, '2021-07-05 09:48:35', '2021-07-18 17:21:07');
INSERT INTO `ebook_last_seen` VALUES (5, 1, 9, 1, '2021-07-05 10:50:42', '2021-07-22 09:16:13');
INSERT INTO `ebook_last_seen` VALUES (6, 1, 11, 1, '2021-07-05 12:25:07', '2021-07-06 09:30:01');
INSERT INTO `ebook_last_seen` VALUES (7, 1, 3, 1, '2021-07-06 04:24:34', '2021-08-04 10:15:10');
INSERT INTO `ebook_last_seen` VALUES (8, 1, 5, 9, '2021-07-09 04:47:51', '2021-07-09 04:48:31');
INSERT INTO `ebook_last_seen` VALUES (9, 1, 4, 1, '2021-07-09 11:31:04', '2021-07-18 17:21:48');
INSERT INTO `ebook_last_seen` VALUES (10, 1, 12, 3, '2021-07-21 09:24:56', '2021-08-09 18:05:51');
INSERT INTO `ebook_last_seen` VALUES (11, 18, 9, 1, '2021-07-22 06:17:06', '2021-07-23 04:32:43');
INSERT INTO `ebook_last_seen` VALUES (12, 19, 9, 1, '2021-07-25 14:04:02', '2021-07-25 14:04:02');
INSERT INTO `ebook_last_seen` VALUES (13, 1, 11, 1, '2021-07-05 12:25:07', '2021-07-06 09:30:01');
INSERT INTO `ebook_last_seen` VALUES (14, 1, 3, 1, '2021-07-06 04:24:34', '2021-07-06 09:28:33');
INSERT INTO `ebook_last_seen` VALUES (15, 1, 5, 9, '2021-07-09 04:47:51', '2021-07-09 04:48:31');
INSERT INTO `ebook_last_seen` VALUES (16, 1, 4, 1, '2021-07-09 11:31:04', '2021-07-18 17:21:48');
INSERT INTO `ebook_last_seen` VALUES (17, 1, 12, 1, '2021-07-21 09:24:56', '2021-07-21 09:24:56');
INSERT INTO `ebook_last_seen` VALUES (18, 1, 8, 7, '2021-07-05 09:48:35', '2021-07-18 17:21:07');
INSERT INTO `ebook_last_seen` VALUES (19, 1, 9, 1, '2021-07-05 10:50:42', '2021-07-22 09:16:13');
INSERT INTO `ebook_last_seen` VALUES (20, 1, 22, 1, '2021-08-09 15:52:57', '2021-08-09 17:37:26');
INSERT INTO `ebook_last_seen` VALUES (21, 1, 1, 9, '2021-08-09 18:09:37', '2021-08-16 09:47:10');

-- ----------------------------
-- Table structure for ebook_master_tag
-- ----------------------------
DROP TABLE IF EXISTS `ebook_master_tag`;
CREATE TABLE `ebook_master_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ebook_id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ebook_master_tag_ebook_id_index`(`ebook_id`) USING BTREE,
  INDEX `ebook_master_tag_tag_id_index`(`tag_id`) USING BTREE,
  CONSTRAINT `ebook_master_tag_ebook_id_foreign` FOREIGN KEY (`ebook_id`) REFERENCES `master_ebook` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ebook_master_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `master_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ebook_master_tag
-- ----------------------------
INSERT INTO `ebook_master_tag` VALUES (59, 21, 3, '2021-07-12 08:29:53', '2021-07-12 08:29:53');
INSERT INTO `ebook_master_tag` VALUES (60, 22, 1, '2021-07-12 08:35:20', '2021-07-12 08:35:20');
INSERT INTO `ebook_master_tag` VALUES (61, 23, 3, '2021-07-12 08:40:01', '2021-07-12 08:40:01');
INSERT INTO `ebook_master_tag` VALUES (76, 24, 3, '2021-07-13 09:24:41', '2021-07-13 09:24:41');
INSERT INTO `ebook_master_tag` VALUES (77, 17, 1, '2021-07-14 02:32:24', '2021-07-14 02:32:24');
INSERT INTO `ebook_master_tag` VALUES (126, 25, 3, '2021-07-18 11:33:16', '2021-07-18 11:33:16');
INSERT INTO `ebook_master_tag` VALUES (127, 25, 2, '2021-07-18 11:33:16', '2021-07-18 11:33:16');
INSERT INTO `ebook_master_tag` VALUES (136, 9, 2, '2021-07-18 16:44:37', '2021-07-18 16:44:37');

-- ----------------------------
-- Table structure for ebook_rating
-- ----------------------------
DROP TABLE IF EXISTS `ebook_rating`;
CREATE TABLE `ebook_rating`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `master_ebook_id` bigint(20) UNSIGNED NOT NULL,
  `rating` double(8, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ebook_rating_user_id_index`(`user_id`) USING BTREE,
  INDEX `ebook_rating_master_ebook_id_index`(`master_ebook_id`) USING BTREE,
  CONSTRAINT `ebook_rating_master_ebook_id_foreign` FOREIGN KEY (`master_ebook_id`) REFERENCES `master_ebook` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ebook_rating_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ebook_rating
-- ----------------------------
INSERT INTO `ebook_rating` VALUES (1, 1, 7, 5.00, '2021-07-07 05:58:40', '2021-07-07 05:59:52');
INSERT INTO `ebook_rating` VALUES (2, 1, 1, 1.00, '2021-07-07 06:08:13', '2021-07-07 06:08:48');
INSERT INTO `ebook_rating` VALUES (3, 2, 7, 2.00, '2021-07-08 03:46:46', '2021-07-08 03:53:48');
INSERT INTO `ebook_rating` VALUES (4, 2, 4, 5.00, '2021-07-08 04:16:29', '2021-07-08 04:16:29');

-- ----------------------------
-- Table structure for master_ebook
-- ----------------------------
DROP TABLE IF EXISTS `master_ebook`;
CREATE TABLE `master_ebook`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `isbn_issn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `master_penerbit_id` bigint(20) UNSIGNED NOT NULL,
  `master_kategori_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `tahun_terbit` year NULL DEFAULT NULL,
  `img_cover` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `file_pdf` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `dibaca` bigint(20) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `start_page` int(11) NOT NULL DEFAULT 0,
  `number_of_page` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `master_ebook_master_penerbit_id_index`(`master_penerbit_id`) USING BTREE,
  INDEX `master_ebook_created_by_index`(`created_by`) USING BTREE,
  INDEX `master_ebook_master_kategori_id_foreign`(`master_kategori_id`) USING BTREE,
  CONSTRAINT `master_ebook_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `master_ebook_master_kategori_id_foreign` FOREIGN KEY (`master_kategori_id`) REFERENCES `master_kategori` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `master_ebook_master_penerbit_id_foreign` FOREIGN KEY (`master_penerbit_id`) REFERENCES `master_penerbit` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_ebook
-- ----------------------------
INSERT INTO `master_ebook` VALUES (1, 'TATACARA PERNIKAHAN PERCERAIAN DAN RUJUK BAGI PRAJURIT', '1642851305', 1, 1, 2009, NULL, NULL, NULL, 264, NULL, '2021-08-10 10:01:08', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (2, 'TECHNOLOGICAL CHANGE AND THE FUTURE OF WARFARE', '1641470019', 1, 1, 2016, NULL, NULL, NULL, 142, NULL, '2021-07-13 04:14:37', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (3, 'TEKNIK ANALISA KEPENDUDUKAN', '1859977657', 1, 1, 2017, NULL, NULL, NULL, 4, NULL, '2021-08-04 10:14:03', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (4, 'TEKNIK PENGUKURAN DAN EVALUASI PENDIDIKAN', '90336713', 1, 1, 2017, NULL, NULL, 'asdas das', 3, NULL, '2021-07-18 17:21:47', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (5, 'TEKNIK PENULISAN ILMIAH POPULER', '117602077', 1, 1, 2006, NULL, NULL, NULL, 1, NULL, '2021-07-09 04:47:47', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (6, 'TEKNIK PERSUASI YANG EFEKTIF', '247905297', 1, 1, 2010, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, 0);
INSERT INTO `master_ebook` VALUES (7, 'TEKNIK RAPAT DAN DISKUSI KELOMPOK', '862123504', 1, 1, 2009, NULL, NULL, NULL, 10, NULL, '2021-07-09 04:17:02', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (8, 'Teknologi Chassis Kendaraan Modern', '1291685809', 1, 1, 2000, NULL, NULL, NULL, 5, NULL, '2021-07-18 17:20:52', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (9, 'TEKNOLOGI NUKLIR', '2019460617', 1, 1, 2016, NULL, '1.pdf', 'TEKNOLOGI NUKLIR TEKNOLOGI NUKLIR TEKNOLOGI NUKLIR TEKNOLOGI NUKLIR TEKNOLOGI NUKLIR TEKNOLOGI NUKLIR', 790, NULL, '2021-07-25 14:04:01', 1, 32, 0);
INSERT INTO `master_ebook` VALUES (10, 'TELADAN DARI TIMUR', '169714117', 1, 1, 2007, NULL, NULL, NULL, 8, NULL, '2021-07-09 05:53:28', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (11, 'TEN MINUTES FROM NORMAL', '921325845', 1, 1, 2019, NULL, NULL, NULL, 3, NULL, '2021-07-06 09:29:50', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (12, 'TEN PROPOSITIONS REGARDING SPACEPOWER', '2079675543', 1, 1, 2008, NULL, NULL, NULL, 83, NULL, '2021-08-10 10:05:34', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (13, 'TENTANG AGAMA', '571519512', 1, 1, 2006, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, 0);
INSERT INTO `master_ebook` VALUES (14, 'TENTANG LEMBAGA LEMBAGA NEGARA MENURUT UUD 1945', '477410053', 1, 1, 2001, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, 0);
INSERT INTO `master_ebook` VALUES (15, 'TENTARA BAYARAN AS DI IRAK', '1123670172', 1, 1, 2017, NULL, NULL, NULL, 0, NULL, NULL, 1, 0, 0);
INSERT INTO `master_ebook` VALUES (16, 'TENTARA DAN KAUM BERSENJATA', '1347096020', 1, 1, 2013, NULL, NULL, NULL, 0, '2021-07-05 19:15:42', NULL, 1, 0, 0);
INSERT INTO `master_ebook` VALUES (17, 'Teknik rapat dan diskusi kelompok', '9014021712', 7, 14, 1982, 'cover.png', 'BAB I PENDAHULUAN.pdf', '', 1, '2021-07-09 10:33:34', '2021-07-14 02:32:24', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (21, 'Ahmadinejad on Palestine :', '9789793371870', 12, 13, 2008, 'cover.png', 'Silabus 4 In Campus.pdf', 'Ahmadinejad on Palestine Ahmadinejad on Palestine Ahmadinejad on Palestine Ahmadinejad on Palestine Ahmadinejad on Palestine Ahmadinejad on Palestine', 2, '2021-07-12 08:29:53', '2021-07-19 09:31:04', 1, 18, 0);
INSERT INTO `master_ebook` VALUES (22, '54 cara hancurkan Israel', '97915081', 13, 11, 2006, 'cover.png', 'Tugas- Dep. Operasi.pdf', 'Across the wall Across the wall Across the wall Across the wall Across the wall Across the wall Across the wall Across the wall Across the wall', 27, '2021-07-12 08:35:20', '2021-08-09 17:37:25', 1, 0, 0);
INSERT INTO `master_ebook` VALUES (23, 'A Deeper level', '9786028537841', 14, 12, 2014, 'cover.jpg', 'Silabus 1 Out Campus (1).pdf', 'A Deeper level A Deeper level A Deeper level A Deeper level A Deeper level A Deeper level A Deeper level', 1, '2021-07-12 08:40:01', '2021-07-19 06:13:34', 1, 12, 0);
INSERT INTO `master_ebook` VALUES (24, '10 shahabat yang dijamin masuk surga', '9786028406710', 15, 8, 2011, 'cover.jpg', 'contoh-ebook.pdf', '10 shahabat yang dijamin masuk surga 10 shahabat yang dijamin masuk surga 10 shahabat yang dijamin masuk surga 10 shahabat yang dijamin masuk surga', 1, '2021-07-12 08:41:16', '2021-07-21 09:04:52', 1, 17, 0);
INSERT INTO `master_ebook` VALUES (25, 'Bahasa Arab dan perkembangan bahasa Indonesia', '11231231212', 16, 14, 1970, 'cover.jpg', 'Unified Modeling Language.pdf', 'Bahasa Arab dan perkembangan bahasa Indonesia Bahasa Arab dan perkembangan bahasa Indonesia Bahasa Arab dan perkembangan bahasa Indonesia', 4, '2021-07-18 10:11:38', '2021-07-18 10:38:36', 1, 0, 0);

-- ----------------------------
-- Table structure for master_ebook_pengarang
-- ----------------------------
DROP TABLE IF EXISTS `master_ebook_pengarang`;
CREATE TABLE `master_ebook_pengarang`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ebook_id` bigint(20) UNSIGNED NOT NULL,
  `pengarang_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `master_ebook_pengarang_ebook_id_index`(`ebook_id`) USING BTREE,
  INDEX `master_ebook_pengarang_pengarang_id_index`(`pengarang_id`) USING BTREE,
  CONSTRAINT `master_ebook_pengarang_ebook_id_foreign` FOREIGN KEY (`ebook_id`) REFERENCES `master_ebook` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `master_ebook_pengarang_pengarang_id_foreign` FOREIGN KEY (`pengarang_id`) REFERENCES `master_pengarang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 91 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_ebook_pengarang
-- ----------------------------
INSERT INTO `master_ebook_pengarang` VALUES (38, 21, 13, '2021-07-12 08:29:53', '2021-07-12 08:29:53');
INSERT INTO `master_ebook_pengarang` VALUES (39, 22, 14, '2021-07-12 08:35:20', '2021-07-12 08:35:20');
INSERT INTO `master_ebook_pengarang` VALUES (40, 23, 15, '2021-07-12 08:40:01', '2021-07-12 08:40:01');
INSERT INTO `master_ebook_pengarang` VALUES (55, 24, 16, '2021-07-13 09:24:41', '2021-07-13 09:24:41');
INSERT INTO `master_ebook_pengarang` VALUES (56, 17, 7, '2021-07-14 02:32:24', '2021-07-14 02:32:24');
INSERT INTO `master_ebook_pengarang` VALUES (81, 25, 17, '2021-07-18 11:33:16', '2021-07-18 11:33:16');
INSERT INTO `master_ebook_pengarang` VALUES (90, 9, 17, '2021-07-18 16:44:37', '2021-07-18 16:44:37');

-- ----------------------------
-- Table structure for master_kategori
-- ----------------------------
DROP TABLE IF EXISTS `master_kategori`;
CREATE TABLE `master_kategori`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_kategori
-- ----------------------------
INSERT INTO `master_kategori` VALUES (1, 'umum', NULL, NULL);
INSERT INTO `master_kategori` VALUES (2, 'militer', NULL, NULL);
INSERT INTO `master_kategori` VALUES (3, 'sejarah', NULL, NULL);
INSERT INTO `master_kategori` VALUES (4, 'politik', NULL, NULL);
INSERT INTO `master_kategori` VALUES (5, 'teknologi', NULL, NULL);
INSERT INTO `master_kategori` VALUES (6, 'ekonomi', NULL, NULL);
INSERT INTO `master_kategori` VALUES (7, 'otomotif', NULL, NULL);
INSERT INTO `master_kategori` VALUES (8, 'budaya', NULL, NULL);
INSERT INTO `master_kategori` VALUES (9, 'majalah', NULL, NULL);
INSERT INTO `master_kategori` VALUES (10, 'hukum', NULL, NULL);
INSERT INTO `master_kategori` VALUES (11, 'religi', NULL, NULL);
INSERT INTO `master_kategori` VALUES (12, 'pendidikan', NULL, NULL);
INSERT INTO `master_kategori` VALUES (13, 'pemerintahan', NULL, NULL);
INSERT INTO `master_kategori` VALUES (14, 'kesehatan', NULL, NULL);

-- ----------------------------
-- Table structure for master_like_ebook
-- ----------------------------
DROP TABLE IF EXISTS `master_like_ebook`;
CREATE TABLE `master_like_ebook`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `master_ebook_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `master_like_ebook_user_id_index`(`user_id`) USING BTREE,
  INDEX `master_like_ebook_master_ebook_id_index`(`master_ebook_id`) USING BTREE,
  CONSTRAINT `master_like_ebook_master_ebook_id_foreign` FOREIGN KEY (`master_ebook_id`) REFERENCES `master_ebook` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_like_ebook
-- ----------------------------
INSERT INTO `master_like_ebook` VALUES (7, 1, 10, '2021-07-06 05:45:40', '2021-07-06 05:45:40');
INSERT INTO `master_like_ebook` VALUES (8, 1, 17, '2021-07-09 11:17:16', '2021-07-09 11:17:16');

-- ----------------------------
-- Table structure for master_penerbit
-- ----------------------------
DROP TABLE IF EXISTS `master_penerbit`;
CREATE TABLE `master_penerbit`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_penerbit
-- ----------------------------
INSERT INTO `master_penerbit` VALUES (1, 'Abadi Bandungasdasd', '', '', NULL, NULL);
INSERT INTO `master_penerbit` VALUES (2, 'sks', 'lsa', 'iadjfda', '2021-07-08 08:13:26', '2021-07-08 08:13:26');
INSERT INTO `master_penerbit` VALUES (7, 'Balai Aksara', NULL, NULL, '2021-07-09 10:33:33', '2021-07-09 10:33:33');
INSERT INTO `master_penerbit` VALUES (8, 'Universitas Negeri Padang Press', NULL, NULL, '2021-07-12 03:17:52', '2021-07-12 03:17:52');
INSERT INTO `master_penerbit` VALUES (9, 'Jurusan Pedalangan. Institut Seni Indonesia (ISI) Surakarta', NULL, NULL, '2021-07-12 08:02:13', '2021-07-12 08:02:13');
INSERT INTO `master_penerbit` VALUES (10, 'Titian ilmu Bandung', NULL, NULL, '2021-07-12 08:24:25', '2021-07-12 08:24:25');
INSERT INTO `master_penerbit` VALUES (11, 'Titian Ilmu', NULL, NULL, '2021-07-12 08:27:51', '2021-07-12 08:27:51');
INSERT INTO `master_penerbit` VALUES (12, 'Pustaka IIMaN', NULL, NULL, '2021-07-12 08:29:53', '2021-07-12 08:29:53');
INSERT INTO `master_penerbit` VALUES (13, 'Hujjah Pressr', NULL, NULL, '2021-07-12 08:35:20', '2021-07-12 08:35:20');
INSERT INTO `master_penerbit` VALUES (14, 'Immanuel Publishing House', NULL, NULL, '2021-07-12 08:40:01', '2021-07-12 08:40:01');
INSERT INTO `master_penerbit` VALUES (15, 'Darus Sunnah', NULL, NULL, '2021-07-12 08:41:16', '2021-07-12 08:41:16');
INSERT INTO `master_penerbit` VALUES (16, 'Universitas Gadjah Mada ', NULL, NULL, '2021-07-18 10:11:38', '2021-07-18 10:11:38');

-- ----------------------------
-- Table structure for master_pengarang
-- ----------------------------
DROP TABLE IF EXISTS `master_pengarang`;
CREATE TABLE `master_pengarang`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_pengarang
-- ----------------------------
INSERT INTO `master_pengarang` VALUES (1, 'A.M Hendropryiono', NULL, NULL);
INSERT INTO `master_pengarang` VALUES (7, 'Stemerding, A.H.S', '2021-07-09 10:33:33', '2021-07-09 10:33:33');
INSERT INTO `master_pengarang` VALUES (8, 'Nasbahry Couto', '2021-07-12 03:17:52', '2021-07-12 03:17:52');
INSERT INTO `master_pengarang` VALUES (9, 'Bambang sudarsono', '2021-07-12 07:09:25', '2021-07-12 07:09:25');
INSERT INTO `master_pengarang` VALUES (10, 'SUNARDI', '2021-07-12 08:02:13', '2021-07-12 08:02:13');
INSERT INTO `master_pengarang` VALUES (11, 'Fajar Dinar', '2021-07-12 08:24:25', '2021-07-12 08:24:25');
INSERT INTO `master_pengarang` VALUES (12, 'Astry Islam', '2021-07-12 08:27:51', '2021-07-12 08:27:51');
INSERT INTO `master_pengarang` VALUES (13, 'Dina Y. Sulaeman', '2021-07-12 08:29:53', '2021-07-12 08:29:53');
INSERT INTO `master_pengarang` VALUES (14, 'Rizki Ridyasmara,1971-', '2021-07-12 08:35:20', '2021-07-12 08:35:20');
INSERT INTO `master_pengarang` VALUES (15, 'Houghton, Israel', '2021-07-12 08:40:01', '2021-07-12 08:40:01');
INSERT INTO `master_pengarang` VALUES (16, 'Asy-syaikh, Abdus Sattar', '2021-07-12 08:41:16', '2021-07-12 08:41:16');
INSERT INTO `master_pengarang` VALUES (17, 'Baried, Siti Barorah', '2021-07-18 10:11:38', '2021-07-18 10:11:38');

-- ----------------------------
-- Table structure for master_tag
-- ----------------------------
DROP TABLE IF EXISTS `master_tag`;
CREATE TABLE `master_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_tag
-- ----------------------------
INSERT INTO `master_tag` VALUES (1, 'SDSAD', '2021-07-08 08:07:50', '2021-07-08 08:07:50');
INSERT INTO `master_tag` VALUES (2, 'PAKSDSA', '2021-07-08 08:08:05', '2021-07-08 08:08:05');
INSERT INTO `master_tag` VALUES (3, 'Tag Satu', '2021-07-12 03:17:52', '2021-07-12 03:17:52');

SET FOREIGN_KEY_CHECKS = 1;
