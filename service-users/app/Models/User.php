<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use \Carbon\Carbon;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    protected $fillable = [
        'name',
        'email',
        'password',
        'korps_id',
        'generation_id',
        'username',
        'last_login',
        'nrp',
        'ranks_id'.
        'korps_id',
        'active_status',
        'lang',
        'point',
        'level',
        'date_format',
        'islogin',
        'avatar',
        'jabatan',
        'departemen_id'
    ];

    protected $appends = ['last_login_view','rolename','roles_name','departemen_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getLastLoginViewAttribute()
    {
        if($this->active_status == 1){
            if(isset($this->last_login)){
                $data = (new \Carbon\Carbon($this->last_login))->diffForHumans();
                return $data;
            }
            return '-';
        }else{
            return 'TIDAK AKTIF';
        }
       
    }
    
    public function getRoleNameAttribute(){
        $data = count($this->getRoleNames());
        if($data > 0){
            $role =$this->getRoleNames()->toArray();
            $name = implode(",",$role);
        }else{
            $name = '-';
        }
        return $name;
    }

    public function getRolesNameAttribute()
    {
        return $this->roles->pluck('name');
    }

    public function getDepartemenNameAttribute()
    {
        // $dep = Departement::find($this->departemen_id);
        // if($dep)
        // {
        //     return $dep->name;
        // }

        return '1000';
    }

    public function generation()
    {
        return $this->hasOne('\Modules\Admin\Entities\Generation','id', 'generation_id');
    }

    public function departemen()
    {
        return $this->hasOne('\Modules\Admin\Entities\Departement','id', 'departemen_id');
    }
}
