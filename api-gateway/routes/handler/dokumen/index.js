const addFile = require('./addFile');
const addFolder = require('./addFolder');
const getFile = require('./getFile');
const getFolder = require('./getFolder');

module.exports = {
    addFile,
    addFolder,
    getFile,
    getFolder,
};