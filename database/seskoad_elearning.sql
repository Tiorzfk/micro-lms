/*
 Navicat Premium Data Transfer

 Source Server         : Mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : seskoad_elearning

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 23/08/2021 18:51:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for badges
-- ----------------------------
DROP TABLE IF EXISTS `badges`;
CREATE TABLE `badges`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of badges
-- ----------------------------

-- ----------------------------
-- Table structure for content_user_progress
-- ----------------------------
DROP TABLE IF EXISTS `content_user_progress`;
CREATE TABLE `content_user_progress`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('onprogress','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `content_user_progress_content_id_index`(`content_id`) USING BTREE,
  INDEX `content_user_progress_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `content_user_progress_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `courses_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_user_progress_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of content_user_progress
-- ----------------------------
INSERT INTO `content_user_progress` VALUES (30, 33, 19, 'done', '2021-07-25 13:57:22', '2021-07-25 13:57:22');
INSERT INTO `content_user_progress` VALUES (33, 35, 19, 'done', '2021-07-25 14:01:18', '2021-07-25 14:01:18');
INSERT INTO `content_user_progress` VALUES (34, 36, 19, 'done', '2021-07-25 14:03:43', '2021-07-25 14:03:43');
INSERT INTO `content_user_progress` VALUES (35, 37, 19, 'done', '2021-07-25 14:03:51', '2021-07-25 14:03:51');
INSERT INTO `content_user_progress` VALUES (36, 39, 19, 'done', '2021-07-25 14:04:02', '2021-07-25 14:04:02');
INSERT INTO `content_user_progress` VALUES (38, 38, 19, 'done', '2021-07-25 14:05:36', '2021-07-25 14:05:36');

-- ----------------------------
-- Table structure for course_content_question
-- ----------------------------
DROP TABLE IF EXISTS `course_content_question`;
CREATE TABLE `course_content_question`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `course_content_question_content_id_index`(`content_id`) USING BTREE,
  INDEX `course_content_question_question_id_index`(`question_id`) USING BTREE,
  CONSTRAINT `course_content_question_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `courses_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `course_content_question_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_content_question
-- ----------------------------

-- ----------------------------
-- Table structure for courses
-- ----------------------------
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `mp_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ns` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `by_system` tinyint(1) NOT NULL DEFAULT 0,
  `enrol_all_student` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `courses_created_by_index`(`created_by`) USING BTREE,
  INDEX `courses_mp_id_index`(`mp_id`) USING BTREE,
  CONSTRAINT `courses_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `courses_mp_id_foreign` FOREIGN KEY (`mp_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of courses
-- ----------------------------
INSERT INTO `courses` VALUES (2, 'MP Pesawat Terbang', 1, 36, NULL, 0, NULL, NULL, 1, 1, '2021-07-14 15:49:02', '2021-07-14 15:49:02', '');
INSERT INTO `courses` VALUES (4, 'MP Radar 2', 1, 38, NULL, 0, NULL, NULL, 1, 1, '2021-07-14 15:50:05', '2021-07-14 15:50:17', '');
INSERT INTO `courses` VALUES (5, 'MP 2', 1, 10, NULL, 0, NULL, NULL, 1, 1, '2021-07-14 16:03:33', '2021-07-14 16:03:33', '');
INSERT INTO `courses` VALUES (6, 'MP 3', 1, 27, NULL, 0, NULL, NULL, 1, 1, '2021-07-14 16:03:42', '2021-07-14 16:03:42', '');
INSERT INTO `courses` VALUES (7, 'MP 5', 1, 29, NULL, 0, NULL, NULL, 1, 1, '2021-07-14 16:03:51', '2021-07-14 16:03:51', '');
INSERT INTO `courses` VALUES (10, 'MP. Teknik', 1, 23, 'mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik mp teknik', 0, NULL, 'asd.pdf', 0, 0, '2021-07-15 04:43:41', '2021-07-15 09:37:31', '');
INSERT INTO `courses` VALUES (11, 'MP. Ops', 1, 23, 'mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops mp ops', 1, NULL, NULL, 0, 1, '2021-07-15 04:48:57', '2021-07-15 04:48:57', '');
INSERT INTO `courses` VALUES (12, 'MP. Ops 2', 1, 23, 'mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2 mp ops 2', 0, NULL, 'Unified Modeling Language.pdf', 0, 1, '2021-07-15 04:52:19', '2021-07-22 03:49:17', 'mp-ops-2');
INSERT INTO `courses` VALUES (13, 'MP Mantap', 1, 9, 'mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap mp mantap', 1, NULL, NULL, 0, 0, '2021-07-15 04:53:12', '2021-07-15 04:53:12', '');

-- ----------------------------
-- Table structure for courses_content
-- ----------------------------
DROP TABLE IF EXISTS `courses_content`;
CREATE TABLE `courses_content`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `time` int(11) NULL DEFAULT NULL,
  `complete_by` enum('checkbox','question','time') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `type` enum('scorm','pdf','video','audio','esai','ebook','heading') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `courses_content_created_by_index`(`created_by`) USING BTREE,
  INDEX `courses_content_course_id_index`(`course_id`) USING BTREE,
  CONSTRAINT `courses_content_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `courses_content_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of courses_content
-- ----------------------------
INSERT INTO `courses_content` VALUES (33, 1, 12, 'Scorm materi', '1626855860_12', NULL, 'question', 2, '2021-07-21 08:24:20', '2021-07-23 03:52:41', 'scorm');
INSERT INTO `courses_content` VALUES (34, 1, 12, 'Heading 1', NULL, NULL, 'checkbox', 1, '2021-07-21 08:24:30', '2021-07-23 03:52:41', 'heading');
INSERT INTO `courses_content` VALUES (35, 1, 12, 'PDF Materi', '1626855887_Unified-Modeling-Language.pdf', NULL, 'checkbox', 3, '2021-07-21 08:24:47', '2021-07-21 08:24:47', 'pdf');
INSERT INTO `courses_content` VALUES (36, 1, 12, 'Video Materi', '1626855937_ScreenCast-00000.mp4', 1, 'time', 4, '2021-07-21 08:25:37', '2021-07-21 08:25:37', 'video');
INSERT INTO `courses_content` VALUES (37, 1, 12, 'Audio Materi', '1626856032_asd.mp3', NULL, 'checkbox', 5, '2021-07-21 08:27:12', '2021-07-22 07:52:54', 'audio');
INSERT INTO `courses_content` VALUES (38, 1, 12, 'Esai', '<p>esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a esai esai a </p>', NULL, 'checkbox', 7, '2021-07-21 08:27:30', '2021-07-23 04:15:41', 'esai');
INSERT INTO `courses_content` VALUES (39, 1, 12, 'Ebook', '9', NULL, 'checkbox', 6, '2021-07-21 08:27:46', '2021-07-23 04:10:23', 'ebook');
INSERT INTO `courses_content` VALUES (40, 1, 12, 'Heading penutup', NULL, NULL, 'checkbox', 8, '2021-07-23 03:56:29', '2021-07-23 04:15:41', 'heading');
INSERT INTO `courses_content` VALUES (41, 1, 11, 'test', '1627293506_Unified-Modeling-Language.pdf', NULL, 'checkbox', 1, '2021-07-26 09:58:26', '2021-07-26 09:58:26', 'pdf');

-- ----------------------------
-- Table structure for courses_users
-- ----------------------------
DROP TABLE IF EXISTS `courses_users`;
CREATE TABLE `courses_users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `courses_users_user_id_index`(`user_id`) USING BTREE,
  INDEX `courses_users_course_id_index`(`course_id`) USING BTREE,
  CONSTRAINT `courses_users_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `courses_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of courses_users
-- ----------------------------
INSERT INTO `courses_users` VALUES (17, 18, 12, '2021-07-26 09:56:36', '2021-07-26 09:56:36');

-- ----------------------------
-- Table structure for exam_answer
-- ----------------------------
DROP TABLE IF EXISTS `exam_answer`;
CREATE TABLE `exam_answer`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `isMarked` tinyint(4) NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_answer_exam_id_index`(`exam_id`) USING BTREE,
  INDEX `exam_answer_question_id_index`(`question_id`) USING BTREE,
  INDEX `exam_answer_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `exam_answer_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exam_answer_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `exam_questions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exam_answer_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_answer
-- ----------------------------
INSERT INTO `exam_answer` VALUES (20, 1, 1, 'dasdasda', '2021-07-28 15:54:23', NULL, 0, 18);
INSERT INTO `exam_answer` VALUES (21, 1, 3, '1', NULL, NULL, 0, 18);

-- ----------------------------
-- Table structure for exam_enrolls
-- ----------------------------
DROP TABLE IF EXISTS `exam_enrolls`;
CREATE TABLE `exam_enrolls`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status` enum('none','progress','done','repeat','repeat-progress') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_enrolls_exam_id_index`(`exam_id`) USING BTREE,
  INDEX `exam_enrolls_user_id_index`(`user_id`) USING BTREE,
  CONSTRAINT `exam_enrolls_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_enrolls_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 178 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_enrolls
-- ----------------------------
INSERT INTO `exam_enrolls` VALUES (1, 1, 18, '2021-07-29 14:15:27', '2021-07-29 14:47:09', 'done');
INSERT INTO `exam_enrolls` VALUES (2, 1, 26, '2021-07-28 13:00:23', NULL, 'none');
INSERT INTO `exam_enrolls` VALUES (3, 1, 25, '2021-07-28 16:06:48', NULL, 'none');
INSERT INTO `exam_enrolls` VALUES (154, 26, 18, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 'none');
INSERT INTO `exam_enrolls` VALUES (155, 26, 19, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 'none');
INSERT INTO `exam_enrolls` VALUES (156, 26, 20, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 'none');
INSERT INTO `exam_enrolls` VALUES (157, 26, 22, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 'none');
INSERT INTO `exam_enrolls` VALUES (158, 26, 25, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 'none');
INSERT INTO `exam_enrolls` VALUES (159, 26, 26, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 'none');
INSERT INTO `exam_enrolls` VALUES (172, 27, 18, '2021-08-21 18:07:03', '2021-08-21 18:07:03', 'none');
INSERT INTO `exam_enrolls` VALUES (173, 27, 19, '2021-08-21 18:07:03', '2021-08-21 18:07:03', 'none');
INSERT INTO `exam_enrolls` VALUES (174, 27, 20, '2021-08-21 18:07:03', '2021-08-21 18:07:03', 'none');
INSERT INTO `exam_enrolls` VALUES (175, 27, 22, '2021-08-21 18:07:03', '2021-08-21 18:07:03', 'none');
INSERT INTO `exam_enrolls` VALUES (176, 27, 25, '2021-08-21 18:07:03', '2021-08-21 18:07:03', 'none');
INSERT INTO `exam_enrolls` VALUES (177, 27, 26, '2021-08-21 18:07:03', '2021-08-21 18:07:03', 'none');

-- ----------------------------
-- Table structure for exam_question_answers
-- ----------------------------
DROP TABLE IF EXISTS `exam_question_answers`;
CREATE TABLE `exam_question_answers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_question_id` bigint(20) UNSIGNED NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `istrue` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_question_answers_exam_question_id_index`(`exam_question_id`) USING BTREE,
  CONSTRAINT `exam_question_answers_exam_question_id_foreign` FOREIGN KEY (`exam_question_id`) REFERENCES `exam_questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_question_answers
-- ----------------------------
INSERT INTO `exam_question_answers` VALUES (1, 3, 'jawaban 1', '0', NULL, NULL);
INSERT INTO `exam_question_answers` VALUES (2, 3, 'jawaban 2', '1', NULL, NULL);
INSERT INTO `exam_question_answers` VALUES (3, 4, 'jawaban 3', '0', NULL, NULL);
INSERT INTO `exam_question_answers` VALUES (4, 4, 'jawaban 4', '1', NULL, NULL);
INSERT INTO `exam_question_answers` VALUES (8, 9, '<p>fgsdfsdf</p>', '1', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `exam_question_answers` VALUES (9, 9, '<p>sdfsdfsdfsdfsd</p>', '0', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `exam_question_answers` VALUES (10, 10, 'jawaban 1 salah', '0', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `exam_question_answers` VALUES (11, 10, 'jawaban 2 salah', '0', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `exam_question_answers` VALUES (12, 10, 'jawaban 3 benar', '1', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `exam_question_answers` VALUES (13, 13, 'jawaban 1 salah', '0', '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `exam_question_answers` VALUES (14, 13, 'jawaban 2 salah', '0', '2021-08-21 04:02:15', '2021-08-21 04:02:15');
INSERT INTO `exam_question_answers` VALUES (15, 13, 'jawaban 3 benar', '1', '2021-08-21 04:02:15', '2021-08-21 04:02:15');

-- ----------------------------
-- Table structure for exam_questions
-- ----------------------------
DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE `exam_questions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `question_name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `serial_number` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `mp_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exam_questions_exam_id_index`(`exam_id`) USING BTREE,
  INDEX `exam_questions_mp_id_index`(`mp_id`) USING BTREE,
  CONSTRAINT `exam_questions_exam_id_foreign` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_questions_mp_id_foreign` FOREIGN KEY (`mp_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_questions
-- ----------------------------
INSERT INTO `exam_questions` VALUES (1, 1, '<p>Jelaskan sejarang SESKOAD ?</p>', 2, 1, NULL, NULL, 9);
INSERT INTO `exam_questions` VALUES (2, 1, '<p>Apa yang dimaksud ?</p>', 2, 3, NULL, NULL, 9);
INSERT INTO `exam_questions` VALUES (3, 1, '<p>Pilihlah jawaban yang benar !</p>', 1, 2, NULL, NULL, 9);
INSERT INTO `exam_questions` VALUES (4, 1, '<p>Yang mana pernyataan yang benar ?</p>', 1, 4, NULL, NULL, 9);
INSERT INTO `exam_questions` VALUES (9, 26, '<p>sdfsdf sdfsdfsdsdf</p>', 1, 2, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 23);
INSERT INTO `exam_questions` VALUES (10, 26, '<p>Pilihlah jawaban yang benar !</p>', 1, 2, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 23);
INSERT INTO `exam_questions` VALUES (11, 27, '<p>ini esai baru dibuat ya</p>', 2, 1, '2021-08-21 04:02:15', '2021-08-21 04:02:15', 23);
INSERT INTO `exam_questions` VALUES (12, 27, '<p>Jelaskan sejarang SESKOAD ?</p>', 2, 2, '2021-08-21 04:02:15', '2021-08-21 04:02:15', 23);
INSERT INTO `exam_questions` VALUES (13, 27, '<p>Pilihlah jawaban yang benar !</p>', 1, 3, '2021-08-21 04:02:15', '2021-08-21 04:02:15', 23);

-- ----------------------------
-- Table structure for exams
-- ----------------------------
DROP TABLE IF EXISTS `exams`;
CREATE TABLE `exams`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `generation_id` bigint(20) UNSIGNED NOT NULL,
  `stage_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `sbs_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NULL DEFAULT NULL,
  `start_date` datetime(0) NOT NULL,
  `end_date` datetime(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `slug` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('none','onprogress','done') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `enrol` varchar(21) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'semua pasis',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exams_generation_id_index`(`generation_id`) USING BTREE,
  INDEX `exams_stage_id_index`(`stage_id`) USING BTREE,
  INDEX `exams_created_by_index`(`created_by`) USING BTREE,
  INDEX `exams_mp_id_index`(`sbs_id`) USING BTREE,
  CONSTRAINT `exams_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exams_generation_id_foreign` FOREIGN KEY (`generation_id`) REFERENCES `seskoad_admin`.`generations` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exams_mp_id_foreign` FOREIGN KEY (`sbs_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `exams_stage_id_foreign` FOREIGN KEY (`stage_id`) REFERENCES `seskoad_admin`.`stages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exams
-- ----------------------------
INSERT INTO `exams` VALUES (1, 1, 1, 1, 22, 'Test', 'test', 60, '2021-07-29 07:00:31', '2021-07-29 12:37:36', NULL, '2021-08-21 18:06:52', 'test-2392383', 'done', 'semua pasis');
INSERT INTO `exams` VALUES (26, 1, 1, 1, 22, 'asdasd asd asdas', '<p>asdasda asd asd asdasdasdasd</p>', NULL, '2021-07-30 00:00:00', '2021-07-30 17:14:23', '2021-07-30 17:14:23', '2021-08-21 18:06:52', 'asdasd-asd-asdas-2021-07-30 17:14:23', 'done', 'semua pasis');
INSERT INTO `exams` VALUES (27, 1, 1, 1, 22, 'UTS Operasi (ubah) 123', '<p>ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi ini deskripsi</p>', NULL, '2021-08-21 07:00:00', '2021-08-28 09:00:00', '2021-08-21 04:02:15', '2021-08-21 18:07:06', 'uts-operasi-ubah-123-2021-08-21 18:07:03', 'onprogress', 'semua pasis');

-- ----------------------------
-- Table structure for gamification_history_user
-- ----------------------------
DROP TABLE IF EXISTS `gamification_history_user`;
CREATE TABLE `gamification_history_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `lencana_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` bigint(20) NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_show` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gamification_history_user_user_id_index`(`user_id`) USING BTREE,
  INDEX `gamification_history_user_lencana_id_index`(`lencana_id`) USING BTREE,
  CONSTRAINT `gamification_history_user_lencana_id_foreign` FOREIGN KEY (`lencana_id`) REFERENCES `gamification_lencana` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `gamification_history_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gamification_history_user
-- ----------------------------
INSERT INTO `gamification_history_user` VALUES (7, 19, NULL, 'Menyelesaikan materi PDF Materi pada course MP. Ops 2', 25, 'point', '2021-07-25 14:01:18', '2021-07-25 14:01:18', 'materi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (8, 19, NULL, 'Menyelesaikan materi Video Materi pada course MP. Ops 2', 25, 'point', '2021-07-25 14:03:43', '2021-07-25 14:03:43', 'materi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (9, 19, NULL, 'Menyelesaikan materi Audio Materi pada course MP. Ops 2', 25, 'point', '2021-07-25 14:03:51', '2021-07-25 14:03:51', 'materi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (10, 19, NULL, 'Menyelesaikan materi Ebook pada course MP. Ops 2', 25, 'point', '2021-07-25 14:04:02', '2021-07-25 14:04:02', 'materi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (11, 19, NULL, 'Menyelesaikan materi Esai pada course MP. Ops 2', 25, 'point', '2021-07-25 14:04:09', '2021-07-25 14:04:09', 'materi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (12, 19, NULL, 'Menyelesaikan materi Esai pada course MP. Ops 2', 25, 'point', '2021-07-25 14:05:36', '2021-07-25 14:05:36', 'materi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (13, 1, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-25 14:30:06', '2021-07-25 14:30:06', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (14, 1, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-26 02:16:56', '2021-07-26 02:16:56', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (15, 23, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-26 04:58:54', '2021-07-26 04:58:54', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (16, 18, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-26 09:39:10', '2021-07-26 09:39:10', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (17, 18, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-27 02:21:11', '2021-07-27 02:21:11', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (18, 18, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-28 03:22:51', '2021-07-28 03:22:51', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (19, 1, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-28 14:29:59', '2021-07-28 14:29:59', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (20, 1, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-29 09:20:29', '2021-07-29 09:20:29', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (21, 18, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-29 09:28:47', '2021-07-29 09:28:47', 'login_application', 0);
INSERT INTO `gamification_history_user` VALUES (22, 18, NULL, 'MenyelesaikanTest', 50, 'point', '2021-07-29 14:39:24', '2021-07-29 14:39:37', 'evaluasi_complete', 0);
INSERT INTO `gamification_history_user` VALUES (23, 1, NULL, 'Masuk ke aplikasi elearning dalam 1 hari', 25, 'point', '2021-07-30 09:24:56', '2021-07-30 09:24:56', 'login_application', 0);

-- ----------------------------
-- Table structure for gamification_lencana
-- ----------------------------
DROP TABLE IF EXISTS `gamification_lencana`;
CREATE TABLE `gamification_lencana`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `point` int(11) NULL DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gamification_lencana
-- ----------------------------
INSERT INTO `gamification_lencana` VALUES (1, 'Lencana Aktifitas', 'Login', 0, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 0, '');
INSERT INTO `gamification_lencana` VALUES (2, 'Aktifitas Newbie', 'Login', 20, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 1, 'Aktifitas=pemula.svg');
INSERT INTO `gamification_lencana` VALUES (3, 'Aktifitas Intermediate', 'Login', 50, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 1, 'Aktifitas=intermediate.svg');
INSERT INTO `gamification_lencana` VALUES (4, 'Aktifitas Advance', 'Login', 80, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 1, 'Aktifitas=advance.svg');
INSERT INTO `gamification_lencana` VALUES (5, 'Aktifitas Master', 'Login', 100, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 1, 'Aktifitas=advance.svg');
INSERT INTO `gamification_lencana` VALUES (6, 'Lencana Belajar', 'Menyelesaikan Mata Pelajaran', 0, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 0, '');
INSERT INTO `gamification_lencana` VALUES (7, 'Belajar Newbie', 'Menyelesaikan Mata Pelajaran', 10, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 6, 'Belajar=newbie.svg');
INSERT INTO `gamification_lencana` VALUES (8, 'Belajar Intermediate', 'Menyelesaikan Mata Pelajaran', 40, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 6, 'Belajar=intermediate.svg');
INSERT INTO `gamification_lencana` VALUES (9, 'Belajar Advance', 'Menyelesaikan Mata Pelajaran', 80, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 6, 'Belajar=advance.svg');
INSERT INTO `gamification_lencana` VALUES (10, 'Belajar Master', 'Menyelesaikan Mata Pelajaran', 120, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 6, 'Belajar=master.svg');
INSERT INTO `gamification_lencana` VALUES (11, 'Lencana Ujian', 'Menyelesaikan Evaluasi', 0, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 0, '');
INSERT INTO `gamification_lencana` VALUES (12, 'Ujian Newbie', 'Menyelesaikan Evaluasi', 8, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 11, 'Ujian=newbie.svg');
INSERT INTO `gamification_lencana` VALUES (13, 'Ujian Intermediate', 'Menyelesaikan Evaluasi', 16, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 11, 'Ujian=intermediate.svg');
INSERT INTO `gamification_lencana` VALUES (14, 'Ujian Advance', 'Menyelesaikan Evaluasi', 32, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 11, 'Ujian=advance.svg');
INSERT INTO `gamification_lencana` VALUES (15, 'Ujian Master', 'Menyelesaikan Evaluasi', 64, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 11, 'Ujian=master.svg');
INSERT INTO `gamification_lencana` VALUES (16, 'Lencana Kehormatan', 'Mencapai Level Hingga Lv', 0, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 0, '');
INSERT INTO `gamification_lencana` VALUES (17, 'Kehormatan Newbie', 'Mencapai Level Hingga Lv', 10, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 16, 'kehormatan=newbie.svg');
INSERT INTO `gamification_lencana` VALUES (18, 'Kehormatan Intermediate', 'Mencapai Level Hingga Lv', 20, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 16, 'kehormatan=intermediate.svg');
INSERT INTO `gamification_lencana` VALUES (19, 'Kehormatan Advance', 'Mencapai Level Hingga Lv', 30, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 16, 'kehormatan=advance.svg');
INSERT INTO `gamification_lencana` VALUES (20, 'Kehormatan Master', 'Mencapai Level Hingga Lv', 50, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 16, 'kehormatan=master.svg');
INSERT INTO `gamification_lencana` VALUES (21, 'Lencana Pencapaian', 'Mengisi Soal Evaluasi', 0, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 0, '');
INSERT INTO `gamification_lencana` VALUES (22, 'Pencapaian Newbie', 'Mengisi Soal Evaluasi', 200, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 21, 'pencapaian=newbie.svg');
INSERT INTO `gamification_lencana` VALUES (23, 'Pencapaian Intermediate', 'Mengisi Soal Evaluasi', 250, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 21, 'pencapaian=intermediate.svg');
INSERT INTO `gamification_lencana` VALUES (24, 'Pencapaian Advance', 'Mengisi Soal Evaluasi', 300, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 21, 'pencapaian=advance.svg');
INSERT INTO `gamification_lencana` VALUES (25, 'Pencapaian Master', 'Menyelesaikan Evaluasi', 400, 1, '2021-07-22 04:36:14', '2021-07-22 04:36:14', 21, 'pencapaian=master.svg');

-- ----------------------------
-- Table structure for gamification_level
-- ----------------------------
DROP TABLE IF EXISTS `gamification_level`;
CREATE TABLE `gamification_level`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `point` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gamification_level
-- ----------------------------
INSERT INTO `gamification_level` VALUES (1, 'Tingkatkan Level setiap', 'level_point', 2500, 1, NULL, NULL);
INSERT INTO `gamification_level` VALUES (2, 'Tingkatkan Level setiap', 'level_mp', 25, 1, NULL, NULL);

-- ----------------------------
-- Table structure for gamification_point
-- ----------------------------
DROP TABLE IF EXISTS `gamification_point`;
CREATE TABLE `gamification_point`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `point` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gamification_point
-- ----------------------------
INSERT INTO `gamification_point` VALUES (1, 'Setiap Masuk Aplikasi E-Learning Memberikan', 'login_application', 25, 1, NULL, NULL);
INSERT INTO `gamification_point` VALUES (2, 'Setiap Menyelesaikan Materi Pelajaran Memberikan', 'materi_complete', 25, 1, NULL, NULL);
INSERT INTO `gamification_point` VALUES (3, 'Setiap Pengisian Soal Essay Memberikan', 'question_complete', 25, 1, NULL, NULL);
INSERT INTO `gamification_point` VALUES (4, 'Setiap Menyelesaikan Evaluasi Memberikan', 'evaluasi_complete', 25, 1, NULL, NULL);
INSERT INTO `gamification_point` VALUES (5, 'Setiap Menyelesaikan Post Tes dan Pre Tes', 'test_complete', 25, 1, NULL, NULL);

-- ----------------------------
-- Table structure for question_answer
-- ----------------------------
DROP TABLE IF EXISTS `question_answer`;
CREATE TABLE `question_answer`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `istrue` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `question_answer_question_id_index`(`question_id`) USING BTREE,
  CONSTRAINT `question_answer_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of question_answer
-- ----------------------------
INSERT INTO `question_answer` VALUES (8, 3, 'jawaban 1 salah', NULL, '0', NULL, NULL);
INSERT INTO `question_answer` VALUES (9, 3, 'jawaban 2 salah', NULL, '0', NULL, NULL);
INSERT INTO `question_answer` VALUES (10, 3, 'jawaban 3 benar', NULL, '1', NULL, NULL);
INSERT INTO `question_answer` VALUES (11, 4, 'jawaban salah', NULL, '0', NULL, NULL);
INSERT INTO `question_answer` VALUES (12, 4, 'jawaban benar', NULL, '1', NULL, NULL);
INSERT INTO `question_answer` VALUES (13, 4, 'jawaban benar', NULL, '1', NULL, NULL);
INSERT INTO `question_answer` VALUES (24, 10, '<p>fgsdfsdf</p>', NULL, '1', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `question_answer` VALUES (25, 10, '<p>sdfsdfsdfsdfsd</p>', NULL, '0', '2021-07-30 17:14:23', '2021-07-30 17:14:23');
INSERT INTO `question_answer` VALUES (26, 12, '<p>A</p>', NULL, '0', '2021-08-02 16:39:35', '2021-08-02 16:39:35');
INSERT INTO `question_answer` VALUES (27, 12, 'B', NULL, '1', '2021-08-02 16:39:35', '2021-08-02 16:39:35');
INSERT INTO `question_answer` VALUES (28, 12, 'C', NULL, '1', '2021-08-02 16:39:35', '2021-08-02 16:39:35');
INSERT INTO `question_answer` VALUES (44, 19, '<p>sfgdfgdfgdf</p>', NULL, '1', '2021-08-02 16:55:35', '2021-08-02 16:55:35');
INSERT INTO `question_answer` VALUES (45, 19, '<p>asdasdasd</p>', NULL, '0', '2021-08-02 16:55:35', '2021-08-02 16:55:35');
INSERT INTO `question_answer` VALUES (46, 19, '<p>hgjghjghjhgj</p>', NULL, '1', '2021-08-02 16:55:35', '2021-08-02 16:55:35');

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `mp_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `questions_created_by_index`(`created_by`) USING BTREE,
  INDEX `questions_category_id_index`(`category_id`) USING BTREE,
  INDEX `questions_mp_id_index`(`mp_id`) USING BTREE,
  CONSTRAINT `questions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `questions_category` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `questions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `seskoad_admin`.`users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `questions_mp_id_foreign` FOREIGN KEY (`mp_id`) REFERENCES `seskoad_admin`.`kurikulum_rpp` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, 1, 2, '<p>Jelaskan sejarang SESKOAD ?</p>', NULL, '2021-07-22 07:55:23', '2021-07-22 07:55:23', 23);
INSERT INTO `questions` VALUES (2, 1, 2, '<p>Apa yang dimaksud ?</p>', NULL, '2021-07-22 07:55:23', '2021-07-22 07:55:23', 23);
INSERT INTO `questions` VALUES (3, 1, 1, '<p>Pilihlah jawaban yang benar !</p>', NULL, '2021-07-22 07:55:23', '2021-07-22 07:55:23', 23);
INSERT INTO `questions` VALUES (4, 1, 1, '<p>Yang mana pernyataan yang benar ?</p>', NULL, '2021-07-22 08:19:28', '2021-07-22 08:19:28', 23);
INSERT INTO `questions` VALUES (10, 1, 1, '<p>sdfsdf sdfsdfsdsdf</p>', NULL, '2021-07-30 17:14:23', '2021-07-30 17:14:23', 23);
INSERT INTO `questions` VALUES (12, 1, 1, '<p>ini soal barnunya</p>', NULL, '2021-08-02 16:39:35', '2021-08-02 16:39:35', 23);
INSERT INTO `questions` VALUES (13, 1, 2, '<p>ini esai baru</p>', NULL, '2021-08-02 16:42:39', '2021-08-02 16:42:39', 25);
INSERT INTO `questions` VALUES (19, 1, 1, '<p>ini soal baru donggggg</p>', NULL, '2021-08-02 16:55:35', '2021-08-02 16:55:35', 23);
INSERT INTO `questions` VALUES (20, 1, 2, '<p>ini esai baru dibuat ya</p>', NULL, '2021-08-02 21:16:17', '2021-08-02 21:16:17', 23);

-- ----------------------------
-- Table structure for questions_category
-- ----------------------------
DROP TABLE IF EXISTS `questions_category`;
CREATE TABLE `questions_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of questions_category
-- ----------------------------
INSERT INTO `questions_category` VALUES (1, 'Pilihan Ganda', NULL, '', NULL, NULL);
INSERT INTO `questions_category` VALUES (2, 'Esai', NULL, '', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
