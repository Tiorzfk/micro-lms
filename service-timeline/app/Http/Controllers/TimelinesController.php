<?php

namespace Modules\Ams\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Ams\Entities\Timelines;
use Modules\Ams\Entities\TimelineLikes;
use Modules\Ams\Entities\TimelineReadings;
use App\Models\User;
use App\Http\Controllers\BaseApiController as BaseApiController;
use Validator;
use DB;
use Auth;

class TimelinesController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $filterValue = $request->input('filterValue');
            $searchValue = $request->input('search');
            $query = Timelines::join('seskoad_admin.users as db2','timelines.user_id','=','db2.id')
                    ->select(['timelines.id','timelines.user_id','timelines.size','timelines.file','timelines.type','timelines.post','timelines.parent_id','timelines.created_at',
                    'db2.nrp','db2.name','db2.generation_id'])->where('parent_id', null)->where('db2.active_status',1);
            
            if($filterValue != 'semua'){
                if($filterValue == 'me'){
                    $query = $query->where('user_id', Auth::user()->id);
                }elseif ($filterValue == 'dosen') {
                    $user = User::role('dosen')->pluck('id');
                    $query = $query->whereIn('user_id', $user);
                }elseif ($filterValue == 'patun') {
                    $user = User::role('patun')->pluck('id');
                    $query = $query->whereIn('user_id', $user);
                }elseif ($filterValue == 'pasis') {
                    $user = User::role('pasis')->pluck('id');
                    $query = $query->whereIn('user_id', $user);
                }elseif ($filterValue == 'search'){
                    if ($searchValue) {
                        $user = User::orderBy('id');
                        $user = $user->where(function($user) use ($searchValue) {
                            $user->Where('name', 'like', '%' . $searchValue . '%');
                            $user->orwhere('nrp', 'like', '%' . $searchValue . '%');
                            $user->orWhere('username', 'like', '%' . $searchValue . '%');
                            $user->orwhere('email', 'like', '%' . $searchValue . '%');
                        });
                        $user = $user->pluck('id');
    
                        $query = $query->where(function($query) use ($searchValue,$user) {
                            $query->Where('timelines.post', 'like', '%' . $searchValue . '%');
                            $query->orwhereIn('timelines.user_id',$user);
                        });
                    }
                }elseif($filterValue == 'gambar'){
                    $query = $query->where('timelines.type', 'image');
                }elseif ($filterValue == 'dokumen') {
                    $query = $query->where('timelines.type', 'document');
                }
            }

            $query = $query->orderBy('timelines.created_at','DESC')->paginate(10);
            foreach ($query as $key => $value) {
                $value['comments'] = $this->comments($value->id, []);

                $cek_reading = TimelineReadings::where('timeline_id', $value->id)->where('user_id', Auth::user()->id)->first();
                if(!$cek_reading){
                    $read = new TimelineReadings;
                    $read->timeline_id = $value->id;
                    $read->user_id = Auth::user()->id;
                    $read->is_reading = 1;
                    $read->save();
                 }
            }
            return $this->sendResponse(200,trans('validation.created'),$query);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function comments($id_timelines, $childs){
            $comments = Timelines::where('parent_id', $id_timelines)->orderBy('id', 'ASC')->limit(2)
              ->get();
                   
              if(count($comments) > 0){
                $comments->transform(function ($item, $key) {
                  $item['comments'] = $this->comments($item->id, []);
                  return $item;
                });
              }     
              return $comments;  
    }

    public function countNotReadings(){
        try {
            $getTimelines = Timelines::where('parent_id', null)->count();
            $getReadings = TimelineReadings::where('user_id', Auth::user()->id)->count();

            $data = $getTimelines-$getReadings;
            return $this->sendResponse(200,trans('validation.created'),$data);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function showComments(Request $request, $id){
        $comments = Timelines::where('parent_id', $id)->whereNotIn('id',$request->data)->orderBy('created_at', 'DESC')->limit(2)->get();
        foreach ($comments as $key => $value) {
            $value['comments'] = $this->comments($value->id, []);
        }
        return $this->sendResponse(200,trans('validation.success'),$comments);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */

    public function likeorunlike(Request $request, $id){
        try {
           $like = $request->input('like');
           $timeline = Timelines::find($id);
           if(!$timeline) return $this->sendResponse(400, 'mungkin postingan/comment sudah terhapus', (object) ['timeline' => ['mungkin postingan/comment sudah terhapus']]);
            if($like){
                $addLike = new TimelineLikes;
                $addLike->user_id = Auth::user()->id;
                $addLike->timeline_id = $id;
                $addLike->save();
                return $this->sendResponse(200,trans('validation.like'),$addLike);
            }else{
                $likes = TimelineLikes::where('timeline_id', $id)->where('user_id', Auth::user()->id)->first();
                if(!$likes) return $this->sendResponse(400, 'mungkin postingan/comment sudah terhapus', (object) ['timeline' => ['mungkin postingan/comment sudah terhapus']]);
                $likes->delete();
                return $this->sendResponse(200,trans('validation.like'),$likes);
            }
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function addComent(Request $request ,$id){
        try {
            $newComment = new Timelines;
            $newComment->post = $request->comment;
            if($request->reply){
                $newComment->reply_id = $request->reply;
            }
            $newComment->parent_id = $id;
            $newComment->user_id = Auth::user()->id;
            $newComment->save();

            $data = Timelines::with('comments')->find($newComment->id);
            return $this->sendResponse(200,trans('validation.created'),$data);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }
    public function editComent(Request $request ,$id){
        try {
            $editComment = Timelines::find($id);
            if(!$editComment) return $this->sendResponse(400, 'comment not found', (object) ['timeline' => ['comment not found']]);
            $editComment->post = $request->post;
            if($request->reply){
                $editComment->reply_id = $request->reply;
            }
            $editComment->user_id = Auth::user()->id;
            $editComment->save();

            $data = Timelines::with('comments')->find($editComment->id);
            $data['isEdit'] = false;
            return $this->sendResponse(200,trans('validation.updated'),$data);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }

    public function create()
    {
        return view('ams::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'post.required'=> trans('validation.required'),
            'file.max'=>trans('validation.max'),
          ];

          $validator = Validator::make($request->all(), [
            // 'post' => 'required'
        ] , $messages);
          
        if($validator->fails()){
            return $this->sendResponse(400, 'Validation Error.', $validator->errors());
        }

        $newPost = new Timelines;
        $newPost->user_id = Auth::user()->id;
        $newPost->post = $request->post;
        $explode = explode(',',$input['file']);
        if(isset($explode[1]) && base64_decode($explode[1], true)){
                if(isset($input['file']) && $input['file'])
                {
                    $name = $input['info_file']['name'];
                    $explode_name = explode('.',$name);
                    $real_name = $explode_name[0].'@'.\Carbon\Carbon::now()->format('his').'@.'.$explode_name[1];
                    $nama_file= uploadbase64ImagewithName($input['file'],'/assets/admin/ams/timeline/',$real_name);
                    if($input['type'] !='image'){
                        $newPost->size = $this->getBase64ImageSize($input['file']);
                    }
                    $newPost->type = $input['type'];
                    $newPost->file = $nama_file;
                }
         }

        $newPost->save();

        $read = new TimelineReadings;
        $read->timeline_id = $newPost->id;
        $read->user_id = Auth::user()->id;
        $read->is_reading = 1;
        $read->save();

        $data = Timelines::with('comments')->find($newPost->id);
        return $this->sendResponse(200,trans('validation.created'),$data);
    }

    public function getBase64ImageSize($base64Image){ //return memory size in B, KB, MB
        try{
            $size_in_bytes = (int) (strlen(rtrim($base64Image, '=')) * 3 / 4);
            $size_in_kb    = $size_in_bytes / 1024;
            $size_in_mb    = $size_in_kb / 1024;
    
            return $size_in_bytes;
        }
        catch(Exception $e){
            return $e;
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('ams::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('ams::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        try {
            $input = $request->all();
            $messages = [
                'post.required'=> trans('validation.required'),
                'file.max'=>trans('validation.max'),
            ];

            $validator = Validator::make($request->all(), [
                // 'file' => 'max:50000',
            ] , $messages);
            
            if($validator->fails()){
                return $this->sendResponse(400, 'Validation Error.', $validator->errors());
            }

            $newPost = Timelines::find($id);
            $newPost->user_id = Auth::user()->id;
            $newPost->post = $request->post;
            if(isset($input['file'])){
                $explode = explode(',',$input['file']);
                if(isset($explode[1]) && base64_decode($explode[1], true)){
                        if(isset($input['file']) && $input['file'])
                        {
                            $name = $input['info_file']['name'];
                            $explode_name = explode('.',$name);
                            $cekExist = public_path().'/assets/admin/ams/timeline/'.$newPost->file;
                            if(file_exists($cekExist) && $newPost->file) {
                                unlink($cekExist);
                            }
                            $name = $input['info_file']['name'];
                            $explode_name = explode('.',$name);
                            $real_name = $explode_name[0].'@'.\Carbon\Carbon::now()->format('his').'@.'.$explode_name[1];
                            $nama_file= uploadbase64ImagewithName($input['file'],'/assets/admin/ams/timeline/',$real_name);
                            if($input['type'] !='image'){
                                $newPost->size = $this->getBase64ImageSize($input['file']);
                            }
                            $newPost->type = $input['type'];
                            $newPost->file = $nama_file;
                        }
                }
            }elseif(!isset($input['file']) && !$input['LastFile']){
                $cekExist = public_path().'/assets/admin/ams/timeline/'.$newPost->file;
                if(file_exists($cekExist) && $newPost->file) {
                    unlink($cekExist);
                }
                $newPost->file ='';
                $newPost->type = null;
            }

            $newPost->save();
            $data = Timelines::find($newPost->id);
            $data['comments'] = $this->comments($data->id, []);
            return $this->sendResponse(200,trans('validation.deleted'),$data);
        } catch (\Throwable $th) {
            return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
             $data = Timelines::find($id);
             if(!$data) return $this->sendResponse(400, 'timelines id not found', (object) ['timeline' => ['timelines not found']]);
             $cekExist = public_path().'/assets/admin/ams/timeline/'.$data->file;
             if(file_exists($cekExist) && $data->file) {
                 unlink($cekExist);
             }
             $data->delete();

             return $this->sendResponse(200,trans('validation.deleted_comment'),$data);
        } catch (\Throwable $th) {
             return $this->sendResponse(500, $this->messageError(), $th->getMessage());
        }
    }
}
