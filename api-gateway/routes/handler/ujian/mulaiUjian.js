const apiAdapter = require('../../apiAdapter');
const {
  UJIAN_APPS_SERVICE_HOST,
  UJIAN_APPS_SERVICE_PORT,
} = process.env;

const api = apiAdapter(UJIAN_APPS_SERVICE_HOST, UJIAN_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });
    const slug = req.params.slug;

    const question = await api.get(`/exams/mulai/${slug}`);
    return res.json(question.data);
  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}