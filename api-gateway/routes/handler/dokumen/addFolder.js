const apiAdapter = require('../../apiAdapter');
const {
  DOKUMEN_APPS_SERVICE_HOST,
  DOKUMEN_APPS_SERVICE_PORT
} = process.env;

const api = apiAdapter(DOKUMEN_APPS_SERVICE_HOST, DOKUMEN_APPS_SERVICE_PORT);

module.exports = async (req, res) => {
  try {
    api.interceptors.request.use(function (config) {
      config.headers.auth =  req.headers.auth;
      return config;
    });

    const folder = await api.post('/folder', req.body);
    const data = folder.data.data;

    return res.json({
      status: 'success',
      data: data
    });

  } catch (error) {

    if (error.code === 'ECONNREFUSED') {
      return res.status(500).json({ status: 'error', message: 'service unavailable' });
    }

    const { status, data } = error.response;
    return res.status(status).json(data);
  }
}