<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('timelines')->group(function() {
    Route::get('/', 'TimelinesController@index');
    Route::get('/count-timelines', 'TimelinesController@countNotReadings');
    Route::get('/show-comments/{id}', 'TimelinesController@showComments');
    Route::post('/', 'TimelinesController@store');
    Route::post('/like-or-unlike/{id}', 'TimelinesController@likeorunlike');
    Route::post('/add-comment/{id}', 'TimelinesController@addComent');
    Route::put('/edit-comment/{id}', 'TimelinesController@editComent');
    Route::put('/{id}', 'TimelinesController@update');
    Route::delete('/{id}', 'TimelinesController@destroy');
});