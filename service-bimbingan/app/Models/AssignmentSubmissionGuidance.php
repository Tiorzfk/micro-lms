<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentSubmissionGuidance extends Model
{
    use HasFactory;

    public $label = 'Pengajuan Bimbingan';

    protected $connection = 'ams';
    protected $table = 'assignment_submission_guidance';
    protected $fillable = [
        'user_id',
        'guidance_id',
        'assignment_id',
        'subject',
        'date',
        'start_time',
        'end_time',
        'type',
        'link',
        'notes_for_dosen',
        'notes_for_pasis',
        'status',
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentSubmissionGuidanceFactory::new();
    }
}
