<?php

namespace App\Models\Penilaian;

use Illuminate\Database\Eloquent\Model;
use App\Models\Akademik\KurikulumRpp;
use App\Models\Penilaian\PenilaianAspekAkademik;
use App\Models\Penilaian\PenilaianAkademik;

class PenilaianDetailAkademik extends Model {
    protected $connection = 'penilaian';
    protected $table = 'penilaian_detail_akademik';

    public function AspekPenilaian(){
        return $this->belongsTo(PenilaianAspekAkademik::class,'penilaian_aspek_akademik_id');
    }

    public function PenilaianAkademik(){
        return $this->hasOne(PenilaianAkademik::class,'penilaian_akademik_id');
    }
}
