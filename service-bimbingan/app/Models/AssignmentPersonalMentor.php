<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignmentPersonalMentor extends Model
{
    use HasFactory;

    public $label = 'Penugasan Pembimbing';

    protected $connection = 'ams';
    protected $table = 'assignment_personal_mentor';
    protected $fillable = [
        'assignment_id',
        'user_id',
        'mentor_id',
    ];
    
    protected static function newFactory()
    {
        return \Modules\Ams\Database\factories\AssignmentPersonalMentorFactory::new();
    }
}
